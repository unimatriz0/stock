
/*
 * Nombre: inventario.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock para el inventario
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control del inventario
 */
class Inventario {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initInventario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initInventario(){

        // inicializamos las variables
        this.IdItem = 0;                   // clave del item
        this.Item = "";                    // descripción del item
        this.Cantidad = 0;                 // cantidad en existencia
        this.Critico = 0;                  // número crítico en existencia
        this.Fecha = "";                   // fecha de alta del registro
        this.IdUsuario = 0;                // clave del usuario
        this.Usuario = "";                 // nombre del usuario

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla con el resumen de inventario
     */
    Inventario(){

        // cargamos en el contenido la nómina de clientes
        $('#contenido').load('inventario/inventario.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del elemento
     * Método que verifica el formulario de inventario antes de
     * enviarlo al servidor
     */
    EditaInventario(id){

        // declaración de variables
        var mensaje;

        // asignamos el valor de la clave
        this.IdItem = id;

        // verifica que se halla ingresado una cantidad en existentes
        this.Cantidad = document.getElementById("cantidad_" + id).value;
        if (this.Cantidad === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar una cantidad de repuestos ";
            mensaje += "en existencia.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidad_" + id).focus();
            return false;

        // verifica que sea un número (firefox no valida correctamente)
        } else if (isNaN(this.Cantidad)){

            // presenta el mensaje y retorna
            mensaje = "La cantidad de repuestos en existencia ";
            mensaje += "debe ser un número";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidad_" + id).focus();
            return false;

        }

        // grabamos el registro
        this.grabaInventario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía al servidor los datos del registro
     */
    grabaInventario(){

        // crea el objeto formulario de html5 y le asigna las propiedades
        var formulario = new FormData();
        formulario.append("id", this.IdItem);
        formulario.append("cantidad", this.Cantidad);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "inventario/graba_inventario.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            dataType: 'json',
            processData: false,
            success: function(data) {

                // si hubo un error
                if (data.Error === false){

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió todo bien
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                    // recarga el div de datos que es menos
                    // complicado que agregar por script a la tabla
                    inventario.Inventario();

                }
            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica si hay unidades en cantidad crítica y
     * en todo caso presenta el informe
     */
    HayCritico(){

        // declaración de variables
        var mensaje;

        // llama la rutina php
        $.get('inventario/haycritico.php',
            function(data){

                // si hay elementos
                if (data.length !== 0){

                    // inicializa la cadena
                    mensaje = "<b>Atención, hay inventario crítico</b>";
                    mensaje += "<br><br>";
                    mensaje += "<table>";
                    mensaje += "<tr>";
                    mensaje += "<td>Marca</td>";
                    mensaje += "<td>Modelo</td>";
                    mensaje += "<td>Existente</td>";
                    mensaje += "<td>Crítico</td>";
                    mensaje += "</tr>";

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        mensaje += "<tr>";
                        mensaje += "<td>" + data[i].marca + "</td>";
                        mensaje += "<td>" + data[i].modelo + "</td>";
                        mensaje += "<td>" + data[i].cantidad + "</td>";
                        mensaje += "<td>" + data[i].critico + "</td>";
                        mensaje += "</tr>";

                    }

                    // termina de armar
                    mensaje += "</table>";

                    // presentamos el mensaje
                    inventario.muestraCritico(mensaje);

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} mensaje - contenido a mostrar
     * Método que presenta el cuadro emergente con la información
     * del inventario crítico
     */
    muestraCritico(mensaje){

        var Critico = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: true,
                        closeOnMouseleave: false,
                        closeButton: true,
                        title: 'Atención',
                        draggable: 'title',
                        repositionOnContent: true,
                        overlay: false,
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        theme: 'TooltipBorder',
                        content: mensaje
                });
        Critico.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que genera el xls con el inventario existente
     */
    Exportar(){

        // redirigimos el navegador
        window.location = "inventario/xls_inventario.php";

    }

}