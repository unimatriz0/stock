<?php

/**
 *
 * inventario/hay_critico.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que verifica si existe algún elemento en cantidad
 * crítica, retorna el array de repuestos por debajo
 *
*/

// incluimos e instanciamos la clase
require_once("inventario.class.php");
$inventario = new Inventario();

// obtenemos el array de elementos
$criticos = $inventario->hayCritico();

// inicializa las variables
$jsondata = array();

// si hay registros
if (count($criticos) != 0){

    // inicia un bucle recorriendo el vector
    foreach($criticos AS $registro){

        // obtiene el registro
        extract($registro);

        // lo agrega a la matriz
        $jsondata[] = array("marca" => $marca,
                            "modelo" => $modelo,
                            "cantidad" => $cantidad,
                            "critico" => $critico);

    }

}

// devuelve la cadena
echo json_encode($jsondata);

?>
