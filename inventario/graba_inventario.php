<?php

/**
 *
 * inventario/graba_inventario.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del formulario y ejecuta la
 * consulta
 *
*/

// incluye e instancia las clases
require_once ("inventario.class.php");
$inventario = new Inventario();

// setea los valores
$inventario->setId($_POST["id"]);
$inventario->setCantidad($_POST["cantidad"]);

// ejecuta la consulta
$resultado = $inventario->grabaInventario();

// retorna el estado
echo json_encode(array("Error" => $resultado));

?>
