<?php

/**
 *
 * inventario/xls_inventario.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (03/10/2018)
 * @copyright   Copyright (c) 2018, KiwiDigital
 *
 * Procedimiento que genera un xls con la nómina de items en depósito
 *
*/

// incluimos e instanciamos las clases
require_once ("inventario.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
$inventario = new Inventario();
$hoja = new PHPExcel();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Existencia en Inventario")
					  ->setSubject("Modelos en inventario")
					  ->setDescription("Resumen de la las existencias de inventario")
					  ->setKeywords("Stock")
					  ->setCategory("Reportes");

// obtenemos la nómina de modelos
$nomina = $inventario->listaInventario();

// si hubo registros
if (count($nomina) != 0){

    // leemos la plantilla
    $hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

    // establecemos el ancho de las columnas
    $hoja->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $hoja->getActiveSheet()->getColumnDimension('B')->setWidth(60);
    $hoja->getActiveSheet()->getColumnDimension('C')->setWidth(13);
    $hoja->getActiveSheet()->getColumnDimension('D')->setWidth(13);
    $hoja->getActiveSheet()->getColumnDimension('E')->setWidth(13);
    $hoja->getActiveSheet()->getColumnDimension('F')->setWidth(13);
    $hoja->getActiveSheet()->getColumnDimension('G')->setWidth(13);
    $hoja->getActiveSheet()->getColumnDimension('H')->setWidth(13);

    // contador de filas
    $fila = 13;

    // inicializamos el total
    $importe_total = 0;
    
    // fijamos el estilo del título
    $estilo = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 12,
            'name'  => 'Verdana'
        ));

    // fijamos el estilo de los encabezados
    $encabezado = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 10,
            'name'  => 'Verdana'
        ));

    // centramos las columnas
    $hoja->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // presenta el título
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('C3', 'Sistema de Control de Stock');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('C5', 'Inventario de Existencias en Depósito');

    // centramos las celdas de los títulos
    $hoja->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('C3')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('C5')->applyFromArray($estilo);

    // establecemos la fuente
    $hoja->getDefaultStyle()->getFont()->setName('Arial')
         ->setSize(10);

    // presenta los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Marca')
          ->setCellValue('B' . $fila, 'Modelo')
          ->setCellValue('C' . $fila, 'Costo')
          ->setCellValue('D' . $fila, 'Mayorista')
          ->setCellValue('E' . $fila, 'Minorista')
          ->setCellValue('F' . $fila, 'Crítico')
          ->setCellValue('G' . $fila, 'Exist.')
          ->setCellValue('H' . $fila, 'Fecha');

    // establecemos la fuente de los encabezados
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('H' . $fila)->applyFromArray($encabezado);

    // volvemos a incrementar la fila
    $fila++;

    // iniciamos un bucle recorriendo el vector
    foreach ($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // presenta el registro
        $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila , $marca)
            ->setCellValue('B' . $fila , $modelo)
            ->setCellValue('C' . $fila , $costo)
            ->setCellValue('D' . $fila , $mayorista)
            ->setCellValue('E' . $fila , $minorista)
            ->setCellValue('F' . $fila , $critico)
            ->setCellValue('G' . $fila , $cantidad)
            ->setCellValue('H' . $fila , $fecha);

        // fijamos el formato de las celdas numéricas
        $hoja->getActiveSheet()->getStyle('C' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('D' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('E' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('F' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('G' . $fila)->getNumberFormat()->setFormatCode("#,###.##");

        // sumamos el importe almacenado
        $importe_total += $costo;
        
        // incrementamos el contador
        $fila++;

    }

    // incrementamos el contador y agregamos el total
    $fila += 2;
        // presenta el registro
        $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila , "Total")
            ->setCellValue('C' . $fila , $importe_total);

    // fijamos la fuente
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
        
    // formateamos la celda
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        
    // renombramos la hoja
    $hoja->getActiveSheet()->setTitle('Inventario');

    // fijamos la primer hoja como activa para abrirla predeterminada
    $hoja->setActiveSheetIndex(0);

    // creamos el writer y lo dirigimos al navegador en formato 2007
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="inventario.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
    $Writer->save('php://output');

}

?>