<?php

/**
 *
 * inventario/inventario.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con los datos del inventario
 *
*/

// incluimos e instanciamos la clase
require_once ("inventario.class.php");
$inventario = new Inventario();

// obtenemos la nómina de inventario
$lista = $inventario->listaInventario();

// verificamos si es administrador (solo el administrador
// puede modificar el inventario)
$esadmin = $_COOKIE["Administrador"];

// definimos la tabla
echo "<table width='90%' align='center' id='inventario'>";

// los encabezados de la tabla
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Marca</th>";
echo "<th align='left'>Modelo</th>";
echo "<th align='left'>Existentes</th>";

// si es administrador
if ($esadmin == "Si"){
    echo "<th align='left'>Costo</th>";
}

// sigue armando
echo "<th align='left'>Mayorista</th>";
echo "<th align='left'>Minorista</th>";
echo "<th align='left'>Crítico</th>";
echo "<th align='left'>Fecha</th>";
echo "<th align='left'></th>";
echo "</tr>";
echo "</thead>";

// el cuerpo de la tabla
echo "<tbody>";

// recorremos el array
foreach($lista AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>$marca</td>";
    echo "<td>$modelo</td>";

    // la cantidad la podemos editar
    echo "<td>
          <input type='number'
           name='cantidad_$id'
           class='numerogrande'
           id='cantidad_$id'
           onClick='$(" . chr(34) . "cantidad_" . $id . chr(34) . ").select();'
           onfocus='$(" . chr(34) . "cantidad_" . $id . chr(34) . ").select();'
           step='0.01'
           value = $cantidad
           title='Cantidad en existencia'>";
    echo "</td>";


    // si es administrador
    if ($esadmin == "Si"){
        echo "<td>" . number_format($costo, 2, ',', '.') . "</td>";
    }

    // continuamos presentando
    echo "<td>" . number_format($mayorista, 3, ',', '.') . "</td>";
    echo "<td>" . number_format($minorista, 3, ',', '.') . "</td>";
    echo "<td>$critico</td>";

    // presentamos la fecha
    echo "<td>$fecha</td>";

    // presentamos el botón editar
    echo "<td>";

    // si es administrador
    if ($esadmin == "Si"){

        echo "<input type='button' name='BtnEditar'
               title='Graba los datos del repuesto'
               onClick='inventario.EditaInventario($id)'
               class='botoneditar'>";

    }

    // cerramos la columna
    echo "</td>";

    // cierra la fila
    echo "</tr>";

}

// cerramos el cuerpo y la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>

<script>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Inventario en Existencia</h1>");

</script>

<?php

// si es administrador
if ($esadmin == "Si"){

    ?>
    <script>

        // aquí fijamos las propiedades del objeto tabla
        $('#inventario').datatable({
            pageSize: 15,
            sort:    [true,     true,     true,  true,  true,  true,  true,  false, false],
            filters: ['select', 'select', false, false, false, false, false, false, false],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

// arma la tabla
} else {

    ?>
    <script>

        // arma la tabla con una columna menos
        $('#inventario').datatable({
            pageSize: 15,
            sort:    [true,     true,     true,  true,  true,  true,  false, false],
            filters: ['select', 'select', false, false, false, false, false, false],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

}
?>
