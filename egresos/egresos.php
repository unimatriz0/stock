<?php

/**
 *
 * egresos/egresos.class.php
 *
 * @package     Stock
 * @subpackage  Egresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones de la tabla de egresos del
 * depósito
 *
*/

// incluimos e instanciamos la clase
require_once ("egresos.class.php");
$egresos = new Egresos();

// obtenemos la nómina de egresos
$materiales = $egresos->listaEgreso();

// definimos la tabla
echo "<table width='90%' align='center' id='egresos'>";

// los encabezados de la tabla
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Fecha</th>";
echo "<th align='left'>Remito</th>";
echo "<th align='left'>Cliente</th>";
echo "<th align='left'>Impreso</th>";
echo "<th align='left'>Pago</th>";
echo "<th align='left'>Venta</th>";
echo "<th align='left'>Importe</th>";
echo "</tr>";
echo "</thead>";

// el cuerpo de la tabla
echo "<tbody>";

// recorremos la matriz
foreach($materiales AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos los datos
    echo "<td>$fecha_egreso</td>";
    echo "<td>$remito</td>";
    echo "<td>$cliente</td>";
    echo "<td>$impreso</td>";
    echo "<td>$pago</td>";
    echo "<td>$tipo_venta</td>";
    echo "<td>" . number_format($importe, 2, ",", ".") . "</td>";

    // cerramos la fila
    echo "</tr>";

}

// cerramos el cuerpo y la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>

<SCRIPT>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Ventas</h1>");

    // aquí fijamos las propiedades del objeto tabla
    // definimos las propiedades de la tabla
    $('#egresos').datatable({
        pageSize: 20,
        sort:    [true, true,  true, true,     true,     true,     true],
        filters: [true, false, true, 'select', 'select', 'select', false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
