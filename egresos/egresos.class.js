/*
 * Nombre: egresos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 18/09/2018
 * Licencia: GPL
 * Comentarios: Funciones que controlan las operaciones con la tabla de
 *              egresos
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los usuarios
 */
class Egresos {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos el layer (no lo podemos en el init
        // porque sino reiniciaríamos el layer al grabar el
        // egreso)
        this.layerEgresos = "";

        // inicializamos las variables de clase
        this.initEgresos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initEgresos(){

        // inicializamos las variables
        this.IdEgreso = 0;             // clave del egreso
        this.IdItem = 0;               // clave del item
        this.Cantidad = 0;             // cantidad egresada
        this.Importe = 0;              // importe parcial
        this.Total = 0;                // importe total del remito
        this.TipoVenta = "";           // si la venta fue mayorista o minorista
        this.Cliente = 0;              // clave del cliente
        this.Remito = 0;               // clave del remito
        this.Fecha = "";               // fecha de alta
        this.Usuario = "";             // usuario que egresò

    }

    // métodos de asignación de valores
    setIdEgreso(idegreso){
        this.IdEgreso = idegreso;
    }
    setIdItem(iditem){
        this.IdItem = iditem;
    }
    setCantidad(cantidad){
        this.Cantidad = cantidad;
    }
    setImporte(importe){
        this.Importe = importe;
    }
    setTotal(total){
        this.Total = total;
    }
    setCliente(cliente){
        this.Cliente = cliente;
    }
    setRemito(remito){
        this.Remito = remito;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de remitos luego de grabar
     * el registro
     */
    limpiaFormulario(){

        // inicializamos algunas de las varibles de clase
        this.IdItem = 0;
        this.Cantidad = 0;
        this.Importe = 0;
        this.TipoVenta = "";

        // inicializamos el formulario
        document.getElementById("item_egreso").value = "";
        document.getElementById("cantidad_egreso").value = 0;
        document.getElementById("mayorista").checked = false;
        document.getElementById("minorista").checked = false;
        document.getElementById("importe_egreso").value = 0;

        // fijamos el foco
        document.getElementById("item_egreso").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito de egreso
     * Método que presenta el formulario para edición o
     * inserción de egresos
     */
    muestraEgresos(idremito){

        // primero verificamos que halla grabado
        if (document.getElementById("id").value == 0){

            // presenta el mensaje
            var mensaje = "Debe grabar el registro primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asignamos en la variable de clase el número de remito
        // y la clave del cliente, verifica si es un nuevo remito
        if (typeof(idremito) == "undefined"){
            this.setRemito(0);
        } else {
            this.setRemito(idremito);
        }
        this.setCliente(document.getElementById("id").value);

        // cargamos el formulario
        this.layerEgresos = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        title: 'Ventas de Mercadería',
                        draggable: 'title',
                        repositionOnContent: true,
                        overlay: false,
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        theme: 'TooltipBorder',
                        width: 700,
                        zIndex: 15000,
                        ajax: {
                            url: 'egresos/form_egresos.html',
                            reload: 'strict'
                        }
                });
        this.layerEgresos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el formulario al editar un remito
     * que carga el importe total del remito
     */
    totalRemito(){

        // llamamos la rutina php pasándole el remito
        $.get('remitos/getimporte.php','remito='+this.Remito,
            function(data){

                // actualizamos el importe total
                document.getElementById("importe_total").value = data.Importe;

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} tipo - el tipo de venta
     * Método llamado al perder el foco la cantidad del formulario
     * de egresos, calcula el importe del item y el importe total
     * del remito (el importe del item puede luego ser modificado
     * por el usuario)
     */
    calculaImporte(tipo){

        // declaración de variables
        var mensaje;

        // verificamos que halla ingresado un item
        if (this.IdItem == 0){

            // presenta el mensaje
            mensaje = "Debe seleccionar un item primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos la cantidad y verificamos
        var cantidad = document.getElementById("cantidad_egreso").value;
        if (cantidad == 0){

            // presenta el mensaje
            mensaje = "La cantidad debe ser distinta de 0";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos el importe unitario del item
        // llama la rutina php
        $.get('modelos/getmodelo.php','modelo='+this.IdItem,
            function(data){

                // definimos las variables
                var importe;
                var total;

                // si la venta es mayorista
                if (tipo == "Mayorista"){

                    // calculamos el importe y asignamos en la variable de clase
                    importe = data.Mayorista * cantidad;
                    egresos.TipoVenta = "Mayorista";

                // si la venta es minorista
                } else {

                    // calculamos el importe minorista y asignamos en la variable
                    importe = data.Minorista * cantidad;
                    egresos.TipoVenta = "Minorista";

                }

                // asignamos los valores
                document.getElementById("importe_egreso").value = importe.toFixed(2);

                // almacenamos en las variables de clase
                egresos.setCantidad(cantidad);
                egresos.setImporte(importe);

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de egresos antes
     * de enviarlo al servidor
     */
    verificaEgreso(){

        // declaración de variables
        var mensaje;

        // por las dudas llamamos al calculo del importe
        this.calculaImporte();

        // si no seleccionò un item
        if (this.IdItem == 0 || this.IdItem == ""){

            // presenta el mensaje
            mensaje = "Debe indicar un item a entregar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no ingresò la cantidad
        if (this.Cantidad == 0 || this.Cantidad == ""){

            // presenta el mensaje
            mensaje = "Debe indicar una cantidad a entregar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no calculó el importe
        if (this.Importe == 0 || this.Importe == ""){

            // presenta el mensaje
            mensaje = "No tengo información del importe";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // grabamos el registro
        this.grabaEgreso();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que instancia el formulario y lo envía al
     * servidor
     */
    grabaEgreso(){

        // declaración de variables
        var datosEgreso = new FormData();

        // tomamos el importe del texto porque puede
        // haberlo editado
        this.Importe = document.getElementById("importe_egreso").value;

        // si ya generò un remito
        if (this.Remito != 0){
            datosEgreso.append("Remito", this.Remito);
        }

        // agregamos el resto de los elementos
        datosEgreso.append("Cantidad", this.Cantidad);
        datosEgreso.append("Item", this.IdItem);
        datosEgreso.append("Importe", this.Importe);
        datosEgreso.append("Cliente", this.Cliente);
        datosEgreso.append("Tipo", this.TipoVenta);

        // serializa el formulario y lo envía, recibe como respuesta
        // la clave del remito
        $.ajax({
            type: "POST",
            url: "egresos/graba_egreso.php",
            data: datosEgreso,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió bien
                } else {

                    // almacenamos la id en el formulario y en la
                    // variable de clase
                    document.getElementById("remito_egreso").value = data.Id;
                    document.getElementById("importe_total").value = data.Importe;
                    egresos.setRemito(data.Id);

                    // carga en la grilla la nómina de egresos
                    egresos.nominaEgresos();

                    // recargamos los remitos del cliente
                    remitos.nominaRemitos();

                    // limpiamos el formulario
                    egresos.limpiaFormulario();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la nómina de egresos correspondiente
     * al remito actual
     */
    nominaEgresos(){

        // cargamos en el div la tabla de egresos
        $('#nomina_egresos').load('egresos/nomina_egresos.php?remito='+this.Remito);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla con los egresos
     */
    Egresos(){

        // cargamos en el contenido la nómina de clientes
        $('#contenido').load('egresos/egresos.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina php que genera el excel de egresos
     */
    Exportar(){

        // llamamos la rutina
        window.location = "egresos/xls_egresos.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar, reinicia
     * el formulario
     */
    cancelaEgreso(){

        // reiniciamos y retornamos
        document.getElementById("egresos_cliente").reset();
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idegreso - clave del registro
     * Método que recibe como parámetro la clave de un egreso y
     * pide confirmación antes de ejecutar la consulta
     */
    borraEgreso(idegreso){

        // declaración de variables
        var mensaje;

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Eliminar Egreso',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            zIndex: 20000,
                            content: 'Está seguro que desea eliminar el registro?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // llama la rutina php
                                $.get('egresos/borra_egreso.php?id='+idegreso,
                                    function(data){

                                        // recargamos la grilla para reflejar los cambios
                                        egresos.nominaEgresos();

                                        // actualizamos el importe total del remito
                                        document.getElementById("importe_total").value = data.Importe;

                                        // presenta el mensaje
                                        new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                    }, "json");

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}