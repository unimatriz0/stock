<?php

/*

    Nombre: remitos.class.php
    Autor: Lic. Claudio Invernizzi
    Fecha: 16/05/2016
    É-Mail: cinvernizzi@gmail.com
    Comentarios: que genera el pdf con el remito de una
                 unidad y lo almacena en la base de datos
                 hereda la clase tpdf para poder genera
                 en utf8

*/

/*

    Atención, esta clase extiende la clase tfpdf que a su vez extiende
    la clase fpdf, la diferencia es que permite definir y generar documentos
    pdf con página de códigos UTF8, el funcionamiento es el mismo que en
    la clase original.

    Sin embargo, la clase para ahorrar espacio regenera las fuentes que
    son utilizadas en el documento, si migramos el sistema y cambia el
    path de la aplicación arroja un error señalando que no encuentra las
    fuentes.

    Para obligar a que regenere las fuentes basta con eliminar todos los
    archivos dat y aquellos php que tengan nombres de fuentes del directorio
    /font/unifont/ dejando solamente los archivos ttf y el archivo ttfonts.php
    que es el que se encarga de generar las fuentes

    Si se desean incluir otras fuentes en el documento, bastaría con
    copiar los archivos ttf en este directorio y luego al incluirlos en
    el documento el sistema se encarga automáticamente de generar los dat

*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// define las fuentes pdf la inclusión de las clases
// funciona correctamente pero el php informa que
// está en el directorio 'unidades' que es desde donde
// fue llamada la clase
define('FPDF_FONTPATH', '../clases/font');

// incluye la clase tpdf
require_once ("../clases/tfpdf.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definición de la clase
class Remitos extends tFPDF {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $AltoLinea;         // constante de alto de la línea
    protected $IdRemito;          // clave del remito
    protected $Remito;            // cadena con el documento
    protected $Nomina;            // vector con los datos de la consulta
    protected $Columnas;          // array con el ancho de las columnas

    /**
     * Constructor de la clase, establece la conexiòn con la base
     * de datos y recibe como paràmetro la clave del remito a
     * generar
     * @param int $idremito
     */
    function __construct ($idremito){

        // llamamos al constructor de la clase padre
        parent::__construct();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la variable de clase
        $this->IdRemito = $idremito;

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // establecemos las propiedades
        $this->SetAuthor("Claudio Invernizzi");
        $this->SetCreator("Sistema de Stock");
        $this->SetSubject("Remito de Entrega", true);
        $this->SetTitle("Remito de Entrega", true);
        $this->SetAutoPageBreak(true, 10);

        // setea los márgenes
        $this->SetMargins(0.1,0.3,0.3);

        // agrega una fuente unicode
        $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // inicializamos el alto de línea de impresión
        $this->AltoLinea = 5;

        // inicializamos el array de columnas
        $this->Columnas = array("Marca" => 60,
                                "Modelo" => 80,
                                "Cantidad" => 30,
                                "Importe" => 25);

        // obtenemos los datos del remito y verificamos si ya
        // fue generado
        $this->getDatosRemito();

    }

    // destructor de la clase
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Mètodo protegido que obtiene los datos del remito
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatosRemito(){

        // verificamos si ya tiene el documento generado
        $consulta = "SELECT remitos.remito AS remito
                     FROM remitos
                     WHERE remitos.id = '$this->IdRemito';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si ya fue generado el remito
        if (!empty($remito)){

            // grabamos el documento en un archivo
            $this->Remito = $remito;
            $this->grabarArchivo();
            return;

        // si no fue generado
        } else {

            // generamos el remito
            $this->generarRemito();

        }

    }

    /**
     * Mètodo protegido que genera del remito
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function generarRemito(){

        // obtenemos los datos del remito
        $this->getRemito();

        // imprimimos el encabezado
        $this->encabezadoRemito();

        // imprime los datos del cliente
        $this->datosCliente();

        // imprimimos la descripción
        $this->imprimirRemito();

        // imprimimos el cierre
        $this->pieRemito();

        // grabamos el remito
        $this->grabarRemito();

        // grabamos en el disco
        $this->grabarArchivo();

        // retornamos
        return;

    }

    /**
     * Mètodo pùblico llamado cuando no està generado el remito
     * que obtiene los datos del mismo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getRemito(){

        // componemos la consulta sobre la unidad
        $consulta = "SELECT vw_remitos.cliente AS cliente,
                            vw_remitos.localidad AS localidad,
                            vw_remitos.domicilio AS domicilio,
                            vw_remitos.pago AS pago,
                            vw_remitos.marca AS marca,
                            vw_remitos.descripcion AS descripcion,
                            vw_remitos.cantidad AS cantidad,
                            vw_remitos.tipo_venta AS tipo_venta,
                            vw_remitos.importe AS importe,
                            vw_remitos.total AS total,
                            vw_remitos.observaciones AS observaciones
                     FROM vw_remitos
                     WHERE vw_remitos.idremito = '$this->IdRemito'
                     ORDER BY vw_remitos.descripcion,
                              vw_remitos.marca;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $this->Nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos
        return;

    }

    // método protegido que imprime el encabezado del remito
    protected function encabezadoRemito(){

        // agregamos la página
        $this->AddPage();

        // setea los márgenes
        $this->SetLeftMargin(5);
        $this->SetTopMargin(5);
        $this->SetRightMargin(5);

        // seteamos la fuente
        $this->SetFont('DejaVu','B',11);

        // setea la posición del cursor de impresión
        $this->SetXY(100,5);

        // presenta los datos de la empresa
        $this->Cell(40, $this->AltoLinea, "BK",0,0,'C');
        $this->Cell(40, $this->AltoLinea, "Remito Nro: " . $this->IdRemito,0,1,'C');

        // ahora reducimos la fuente
        $this->SetFont('DejaVu','',10);

        // seteamos el puntero de impresión
        $this->SetXY(80,15);

        // seteamos la fuente
        $this->SetFont('DejaVu','B',12);

        // presentamos el texto
        $this->Cell(0,$this->AltoLinea,"Entrega de Materiales",0,1,'C');

        // seteamos la fuente
        $this->SetFont('DejaVu','',12);

        // seteamos el puntero de impresión
        $this->SetXY(5,40);

    }

    /**
     * Mètodo protegido que imprime los datos del cliente en la
     * primer pàgina del remito
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function datosCliente(){

        // obtenemos el primer registro del vector y
        // lo pasamos a variables
        $fila = $this->Nomina[0];
        extract($fila);

        // presentamos el nombre y apellido
        $this->SetFont('DejaVu','',12);
        $texto = "Cliente: " . $cliente;
        $this->Cell(0,$this->AltoLinea, $texto, 0, 1, 'L');

        // presentamos el teléfono
        $texto = "Dirección: " . $domicilio;
        $this->Cell(0,$this->AltoLinea, $texto, 0, 1, 'L');

        // presentamos el mail
        $texto = "Localidad: " . $localidad;
        $this->Cell(0,$this->AltoLinea, $texto, 0, 1, 'L');

        // presentamos el medio de pago
        $texto = "Medio de Pago: " . $pago;
        $this->Cell(0,$this->AltoLinea, $texto, 0, 1, 'L');

        // insertamos una línea como separador
        // obtenemos las coordenadas actuales
        $horizontal = $this->GetX();
        $vertical = $this->GetY();
        $this->Line($horizontal, $vertical, $horizontal + 200, $vertical);

        // seteamos a fuente
        $this->SetFont('DejaVu', 'B', 12);

        // insertamos un separador
        $this->Ln($this->AltoLinea - 1);

        // presentamos el título de servicio
        $this->Cell(0,$this->AltoLinea, "Descripción", 0, 1, 'L');

        // definimos los encabezados de tabla
        $this->Cell($this->Columnas["Marca"], $this->AltoLinea, "Marca", 0, 0,'L');
        $this->Cell($this->Columnas["Modelo"], $this->AltoLinea, "Modelo", 0, 0,'L');
        $this->Cell($this->Columnas["Cantidad"], $this->AltoLinea, "Cantidad", 0, 0,'C');
        $this->Cell($this->Columnas["Importe"], $this->AltoLinea, "Importe", 0, 1,'R');

        // retornamos
        return;

    }

    // método protegido que imprime el cuerpo del remito
    protected function imprimirRemito(){

        // seteamos la fuente
        $this->SetFont('DejaVu', '', 10);

        // recorremos el vector
        foreach($this->Nomina AS $registro){

            // obtenemos los datos
            extract($registro);

            // presentamos el registro
            $this->Cell($this->Columnas["Marca"], $this->AltoLinea, $marca, 0, 0,'L');
            $this->Cell($this->Columnas["Modelo"], $this->AltoLinea, $descripcion, 0, 0,'L');
            $this->Cell($this->Columnas["Cantidad"], $this->AltoLinea, $cantidad, 0, 0,'L');
            $this->Cell($this->Columnas["Importe"], $this->AltoLinea, $importe, 0, 1,'L');

        }

        // ahora imprimimos el total
        $columna = $this->Columnas["Marca"] + $this->Columnas["Modelo"] + $this->Columnas["Cantidad"];
        $this->Cell($columna, $this->AltoLinea, "Total", 0, 0,'L');
        $this->Cell($this->Columnas["Importe"], $this->AltoLinea, $total, 0, 1,'L');

    }

    // método protegido que imprime el pié de página
    protected function pieRemito(){

        // verificamos si tiene observaciones
        $fila = $this->Nomina[0];
        extract($fila);

        // si tiene comentarios
        if (!empty($observaciones)){

            // insertamos un separador
            $this->Ln($this->AltoLinea);

            // remplazamos los saltos de lìnea y el espacio
            $observaciones = str_replace("&nbsp;", " ", $observaciones);
            $observaciones = str_replace("<br>", "/n", $observaciones);

            // lo imprimimos
            $observaciones = strip_tags($observaciones);
            $this->MultiCell(0, $this->AltoLinea, $observaciones);

        }

        // retornamos
        return;

    }

    /**
     * Método protegido que graba el remito en la base de
     * datos y luego lo envía para grabar en el disco
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function grabarRemito(){

        // atención, usamos la clase tfpdf que extiende la clase
        // original para poder definir la página de códigos del
        // documento, el manual dice que funciona exactamente igual
        // salvo que hay que definir las fuentes y la página de
        // códigos a utilizar, sin embargo, el método output
        // espera los parámetros a la inversa que la clase original
        // primero el nombre del archivo y luego el tipo de salida

        // obtenemos la cadena del pdf y la escapamos
        $this->Remito = $this->Output("","S");

        // usamos sentencias preparadas con parámetros para
        // no tener que preocuparnos por los simbolos de 8 bits
        $consulta = "UPDATE remitos SET
                            remito = :remito
                     WHERE remitos.id = :idremito;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":remito", $this->Remito);
        $psInsertar->bindParam(":idremito", $this->IdRemito);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // retornamos
        return;

    }

    /**
     * Método protegido que graba el contenido del documento
     * en un archivo del disco
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function grabarArchivo(){

        // abrimos el archivo para escritura
        $puntero = fopen("remito.pdf", 'w');
        fwrite($puntero, $this->Remito);
        fclose($puntero);

    }

}
