<?php

/**
 *
 * egresos/nomina_egresos.php
 *
 * @package     Stock
 * @subpackage  Egresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro la clave de un remito y
 * presenta el detalle de los egresos correspondientes a
 * ese remito
 *
*/

// incluimos e instanciamos la clase
require_once ("egresos.class.php");
$egreso = new Egresos();

// obtenemos la nómina
$nomina = $egreso->nominaEgresos($_GET["remito"]);

// definimos la tabla
echo "<table width='95%'
             align='center'
             border='0'
             class='display'
             id='remitosegresos'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Marca</th>";
echo "<th>Modelo</th>";
echo "<th>Cantidad</th>";
echo "<th>Importe</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // lo presentamos
    echo "<td>$marca</td>";
    echo "<td>$modelo</td>";
    echo "<td>$cantidad</td>";
    echo "<td>$importe</td>";

    // presenta el botón eliminar
    echo "<td>";
    echo "<input type='button'
                 name='btnBorraEgreso'
                 id='btnBorraEgreso'
                 class='botonborrar'
                 title='Pulse para borrar el registro'
                 onClick='egresos.borraEgreso($idegreso)'>";

    // cerramos la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>

<script>

    // definimos las propiedades de todas las tablas
    // con la clase display
    $('table.display').DataTable({
        "language":{
            "url": "script/dataTables.spanish.lang"
        }});
        
</script>