<?php

/**
 *
 * egresos/borra_egreso.php
 *
 * @package     Stock
 * @subpackage  Egresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro la id de un egreso y ejecuta
 * la consulta de eliminación, retorna el resultado de la
 * operación
 *
*/

// incluimos e instanciamos la clase
require_once("egresos.class.php");
$egreso = new Egresos();

// ejecutamos la consulta
$importe = $egreso->borraEgreso($_GET["id"]);

// retorna siempre verdadero
echo json_encode(array("Importe" => $importe));

?>
