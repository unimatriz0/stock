/*
 * Nombre: clientes.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 15/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock para los clientes
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los clientes
 */
class Clientes {

    /**
     * Constructor de la clase
     * @constructor
     */
    constructor(){

        // el layer emergente lo instanciamos acá porque si lo hacemos
        // en el init al llamarlo perdemos control del layer
        this.layerClientes = "";

        // inicializamos las variables de clase
        this.initClientes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que inicializa las variables de clase
     */
    initClientes(){

        // inicializamos las variables
        this.IdCliente = 0;                // clave del registro
        this.Cliente = "";                 // nombre del cliente
        this.Localidad = "";               // localidad del cliente
        this.Domicilio = "";               // dirección postal
        this.Telefono = "";                // teléfono del cliente
        this.Mail = "";                    // mail del cliente
        this.Contacto = "";                // de donde vino el cliente
        this.Observaciones = "";           // comentarios y observaciones
        this.Correcto = true;              // switch de cliente repetido

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {boolean} clienteCorrecto 
     * Método llamado por callback que verifica el cliente no se 
     * encuentre repetido
     */
    validaCliente(clienteCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // lo llamamos asincrónico
        $.ajax({
            url: "clientes/validacliente.php?cliente=" + this.Cliente + '&mail=' + this.Mail,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si está repetido
                if (data.Registros != 0) {

                    // asignamos en la variable
                    clientes.Correcto = false;

                // si no hay registros
                } else {

                    // asignamos en la variable
                    clientes.Correcto = true;

                }

            }
        });
        
        // retornamos
        clienteCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de clientes y
     * asigna los valores en las variables de clase
     */
    GrabaCliente(){

        // declaración de variables
        var mensaje;

        // inicializamos las variables para estar seguros de no
        // arrastrar basura
        this.initClientes();

        // asignamos la clave
        this.IdCliente = document.form_clientes.id.value;

        // verifica se halla ingresado el nombre
        this.Cliente = document.form_clientes.cliente.value;
        if (this.Cliente === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el nombre completo del cliente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_clientes.nombre.focus();
            return false;

        }

        // domicilio y localidad son optativos
        this.Localidad = document.form_clientes.localidad.value;
        this.Domicilio = document.form_clientes.domicilio.value;

        // da un alerta si no hay domicilio
        if (this.Domicilio == ""){

            // presenta el alerta y continúa
            mensaje = "No hay datos del domicilio del cliente";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // verifica se halla ingresado el teléfono
        this.Telefono = document.form_clientes.telefono.value;
        if (this.Telefono === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el teléfono o celular del cliente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_clientes.telefono.focus();
            return false;

        }

        // verifica se halla ingresado el mail
        this.Mail = document.form_clientes.mail.value;
        if (this.Mail === ""){

            // presenta el mensaje y valida igualmente
            mensaje = "Recuerde que no hay mail de contacto";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // verifica que esté correctamente formateado
        } else if (!echeck(this.Mail)){

            // presenta el mensaje y retorna
            mensaje = "La dirección de mail parece incorrecta";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_clientes.mail.focus();
            return false;

        }

        // verifica que halla seleccionado una forma de contacto
        this.Contacto = document.form_clientes.contacto_cliente.value;
        if (this.Contacto === ""){

            // presenta el mensaje y retorna
            mensaje = "No hay información de la forma en<br>";
            mensaje += "que el cliente se puso en contacto.";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // lee el valor del editor y lo asigna al formulario
        this.Observaciones = CKEDITOR.instances['observaciones'].getData();
        document.form_clientes.observaciones.value = this.Observaciones;

        // si está dando un alta verificamos por callback que el cliente no esté repetido
        if (this.IdCliente == 0){

            // llamamos por callbak para verificar el usuario
            this.validaCliente(function (clienteCorrecto) {

                // si está repetido
                if (!clienteCorrecto) {

                    // presentamos el mensaje
                    mensaje = "Ese cliente ya se encuentra declarado";
                    new jBox('Notice', { content: mensaje, color: 'red' });

                }

            });

        }

        // graba el registro
        this.actualizaRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase envía la
     * consulta al servidor
     */
    actualizaRegistro(){

        // si está repetido
        if (!this.Correcto){
            return false;
        }
        
        // declaramos el formulario
        var datosCliente = new FormData();

        // si està editando
        if (this.IdCliente != 0){
            datosCliente.append("id", this.IdCliente);
        }

        // asignamos el resto de los valores
        datosCliente.append("domicilio", this.Domicilio);
        datosCliente.append("cliente", this.Cliente);
        datosCliente.append("localidad", this.Localidad);
        datosCliente.append("telefono", this.Telefono);
        datosCliente.append("mail", this.Mail);
        datosCliente.append("contacto", this.Contacto);
        datosCliente.append("observaciones", this.Observaciones);

        // serializa el formulario y lo envía, recibe como respuesta
        // el estado de la operación
        $.ajax({
            type: "POST",
            url: "clientes/graba_cliente.php",
            data: datosCliente,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Error == false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió bien
                } else {

                    // almacenamos la id en el formulario para poder
                    // agregar unidades inmediatamente
                    document.form_clientes.id.value = data.id;

                    // carga en el layer en segundo plano los cambios
                    clientes.Clientes();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} cliente - clave del registro a mostrar o nulo
     * Método que presenta el layer con el formulario de clientes
     */
    NuevoCliente(cliente){

        // si no recibió la id
        if (typeof(cliente) == "undefined"){

            // lo carga para un alta
            cliente = "";

        }

        // mostramos el tab de clientes
        this.Clientes();

        // cargamos el formulario
        this.layerClientes = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                title: 'Edición de Clientes',
                                draggable: 'title',
                                repositionOnContent: true,
                                overlay: false,
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                theme: 'TooltipBorder',
                                ajax: {
                                    url: 'clientes/form_clientes.php?id='+cliente,
                                    reload: 'strict'
                                }
                        });
        this.layerClientes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla con los clientes
     */
    Clientes(){

        // cargamos en el contenido la nómina de clientes
        $('#contenido').load('clientes/clientes.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina php para generar el xls de clientes
     */
    Exportar(){

        // llamamos la rutina
        window.location = "clientes/xls_clientes.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que pide confirmación antes de llamar la consulta
     * de eliminación
     */
    BorraCliente(id){

        // declaración de variables
        var mensaje;

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Eliminar Cliente',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: 'Está seguro que desea eliminar el registro?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // llama la rutina php
                                $.get('clientes/borra_cliente.php?id='+id,
                                    function(data){

                                        // si retornó correcto
                                        if (data.Error != 0){

                                            // recargamos la grilla para reflejar los cambios
                                            clientes.Clientes();

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                        // si no pudo eliminar
                                        } else {

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                        }

                                    }, "json");

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}