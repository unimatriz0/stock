<?php

/**
 *
 * clientes/form_cliente.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que arma el formulario para la carga de un
 * nuevo cliente o su edición
 *
*/

// inclusión de las variables de clase
require_once ("clientes.class.php");

// obtenemos el nivel de acceso
$clientes = $_COOKIE["Clientes"];
$usuario = $_COOKIE["Usuario"];

// si recibió la id del registro
if (!empty($_GET["id"])){

    // instancia la clase y obtiene el registro
    $datos_cliente = new Clientes();
    $datos_cliente->obtenerCliente($_GET["id"]);

    // ahora asignamos las variables
    $id = $_GET["id"];
    $cliente = strtoupper($datos_cliente->getCliente());
    $localidad = $datos_cliente->getLocalidad();
    $domicilio = $datos_cliente->getDomicilio();
    $telefono = $datos_cliente->getTelefono();
    $mail = $datos_cliente->getMail();
    $contacto = $datos_cliente->getContacto();
    $observaciones = $datos_cliente->getObservaciones();
    $usuario = $datos_cliente->getUsuario();
    $fecha_alta = $datos_cliente->getFechaAlta();

// si está dando un alta
} else {

    // declaración de variables (para evitar los warning)
    $id = 0;
    $cliente = "";
    $localidad = "";
    $domicilio = "";
    $telefono = "";
    $mail = "";
    $contacto = "";
    $observaciones = "";
    $fecha_alta = date('d/m/Y');

}

// insertamos un separador
echo "<br>";

// definimos el formulario
echo "<form name='form_clientes' id='form_clientes'>";

// definimos la tabla
echo "<table width='90%' align='center' border='1'>";

// presentamos la id oculta
echo "<input type='hidden'
       name='id'
       id='id'
       value='$id'>";

// presentamos el nombre del cliente y fijamos que para cuando
// pierda el foco lo convierta a mayúsculas
echo "<tr>";
echo "<td colspan='3'><b>Nombre y Apellido: </b>";
echo "<input type='text'
       name='cliente'
       id='cliente'
       size='35'
       required
       value='$cliente'
       onblur='document.form_clientes.cliente.value = document.form_clientes.cliente.value.toUpperCase()'
       title='Nombre completo del cliente'>";
echo "</td>";

// presentamos el domicilio
echo "<td colspan='3'>";
echo "<b>Domicilio: </b>";
echo "<input type='text'
       name='domicilio'
       id='domicilio'
       size='40'
       value='$domicilio'
       required
       title='Domicilio del cliente'>";
echo "</td>";
echo "</tr>";

// presentamos la localidad
echo "<tr>";
echo "<td colspan='2'>";
echo "<b>Localidad: </b>";
echo "<input type='text'
       name='localidad'
       id='localidad'
       size='30'
       required
       value='$localidad'
       title='Localidad de residencia'>";
echo "</td>";

// presentamos el telefono
echo "<td colspan='4'>";
echo "<b>Teléfono: </b>";
echo "<input type='text'
       name='telefono'
       id='telefono'
       size='12'
       required
       value='$telefono'
       title='Teléfono o Móvil del cliente'>";

echo "&nbsp;&nbsp;";

// presentamos el mail
echo "<b>Mail: </b>";
echo "<input type='text'
       name='mail'
       id='mail'
       size='30'
       required
       value='$mail'
       title='Correo Electrónico del cliente'>";
echo "</td>";
echo "</tr>";

// presentamos la fuente del contacto y verificamos
// si existe algún valor seleccionado
echo "<tr>";
echo "<td colspan='3' style='vertical-align:middle; width:50%'>
       <b>Contacto: </b>";
echo "<select name='contacto_cliente'
              id='contacto_cliente'
              size='1'
              title='Como se contacto el cliente'>";

// agregamos las opciones
echo "<option></option>";

// según el contacto
if ($contacto == "Facebook"){
    echo "<option selected>Facebook</option>";
} else {
    echo "<option>Facebook</option>";
}
if ($contacto == "Instagram"){
    echo "<option selected>Instagram</option>";
} else {
    echo "<option>Instagram</option>";
}
if ($contacto == "Google"){
    echo "<option selected>Google</option>";
} else {
    echo "<option>Google</option>";
}
if ($contacto == "Mercado Libre"){
    echo "<option selected>Mercado Libre</option>";
} else {
    echo "<option>Mercado Libre</option>";
}
if ($contacto == "Otro"){
    echo "<option selected>Otro</option>";
} else {
    echo "<option>Otro</option>";
}

// cerramos la columna y abrimos otra
echo "</td>";
echo "<td colspan='3' style='vertical-align:middle; width:30%'>";
echo "</td>";
echo "</tr>";

// presentamos los comentarios
echo "<tr>";
echo "<td colspan='5' rowspan='4'>";
echo "<fieldset>";
echo "<legend>Observaciones</legend>";
echo "<textarea
       name='observaciones'
       id='observaciones'
       rows='3' cols='80'
       title='Ingrese los comentarios pertinentes'>";
echo $observaciones;
echo "</textarea>";
echo "</fieldset>";
echo "</td>";

// presenta la fecha de alta
echo "<td>";
echo "<b>Alta: </b>";
echo "<input type='text'
             name='alta_cliente'
             id='alta_cliente'
             size='8'
             value='$fecha_alta'
             title='Fecha de alta del registro'
             readonly>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// presenta el usuario
echo "<tr>";
echo "<td>";
echo "<b>Usuario: </b>";
echo "<input type='text'
             name='usuario_cliente'
             id='usuario_cliente'
             size='8'
             value='$usuario'
             title='Usuario que ingresò el registro'
             readonly>";
echo "</td>";
echo "</tr>";

// abrimos la fila
echo "<tr>";

// según el nivel de acceso
if ($clientes != "Consultar"){

    // presenta los botones de acción
    echo "<td style='text-align:center'>";
    echo "<input type='button'
           name='BtnGrabar'
           id='BtnGrabar'
           value='Grabar'
           title='Ingresa al cliente en la base'
           class='boton_grabar'
           onClick='clientes.GrabaCliente()'>";
    echo "</td>";

// si solo puede consultar
} else {
    echo "<td></td>";
}

// cerramos la fila
echo "</tr>";

// presenta el botón cerrar
echo "<tr>";
echo "<td style='text-align:center'>";
echo "<input type='button'
        name='BtnCancelar'
        value='Cerrar'
        title='Cierra esta ventana'
        class='boton_cancelar'
        onClick='clientes.layerClientes.destroy()'>";
echo "</td>";
echo "</tr>";

// cierra la fila
echo "</tr>";

// cerramos la tabla
echo "</table>";

// cerramos el formulario
echo "</form>";

// definimos el div donde cargamos las ventas
echo "<div id='ventascliente'></div>";

?>
<script>

    // fijamos las propiedades del editor
    CKEDITOR.replace( 'observaciones', {
            height: 100
        } );

    // cargamos la nómina de remitos
    remitos.nominaRemitos();

    // setea el foco en el primer campo
    document.form_clientes.cliente.focus();

</script>
