<?php

/**
 *
 * clientes/clientes.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con la nómina de clientes
 *
*/

// incluimos e instanciamos la clase
require_once ("clientes.class.php");
$datos = new Clientes();

// definimos la tabla
echo "<table style='width:98%; margin-left: auto; margin-right: auto' id='grilla_clientes'>";

// los encabezados de la tabla
echo "<thead>";
echo "<tr>";
echo "<th>Nombre y Apellido</th>";
echo "<th>Telefono</th>";
echo "<th>E-Mail</th>";
echo "<th>Fecha</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// el cuerpo de la tabla
echo "<tbody>";

// obtenemos la nómina de clientes
$nomina = $datos->listarClientes();

// recorremos el array
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el nombre con el enlace
    echo "<td><a href='#'
               title='Modifica los datos del cliente'
               onClick='clientes.NuevoCliente($id_cliente)'>
               $nombre_cliente</a></td>";

    // continuamos presentando los datos
    echo "<td>$telefono_cliente</td>";
    echo "<td>$mail_cliente</td>";
    echo "<td>$fecha_cliente</td>";
    echo "<td>$usuario_cliente</td>";

    // si puede borrar presentamos el botón borrar
    echo "<td>";
    if ($datos->puedeBorrar($id_cliente)){

        // presenta el botón
        echo "<input type='button'
               name='BtnBorrar'
               id='BtnBorrar'
               title='Eliminar el cliente'
               onClick='clientes.BorraCliente($id_cliente)'
               class='botonborrar'>";
        echo "</td>";
        echo "</tr>";

    }

}

// cerramos el cuerpo y la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>

<script>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Nómina de Clientes</h1>");

    // aquí fijamos las propiedades del objeto tabla
    $('#grilla_clientes').datatable({
        pageSize: 15,
        sort: [true, false, false, true, true, false],
        filters: [true, true, false, true, 'select', false],
        filterText: 'Buscar ... '
    });

</script>
