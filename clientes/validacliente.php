<?php

/**
 *
 * clientes/validacliente.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro el nombre y el mail de 
 * un cliente y verifica que no esté repetido
 *
*/

// incluimos e instanciamos la clase
require_once ("clientes.class.php");
$cliente = new Clientes();

// verificamos si está repetido
$registros = $cliente->validaCliente($_GET["cliente"], $_GET["mail"]);

// retornamos
echo json_encode(array("Registros" => $registros));

?>