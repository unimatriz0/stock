<?php

/**
 *
 * clientes/graba_cliente.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del cliente y ejecuta
 * la consulta de edición o inserción, retorna la id del registro afectado
 *
*/

// incluimos e instanciamos la clase
require_once("clientes.class.php");
$cliente = new Clientes();

// seteamos las propiedades recibidas por post
if (!empty($_POST["id"])){
    $cliente->setId($_POST["id"]);
}

// seteamos el resto
$cliente->setDomicilio($_POST["domicilio"]);
$cliente->setLocalidad($_POST["localidad"]);
$cliente->setObservaciones($_POST["observaciones"]);
$cliente->setCliente($_POST["cliente"]);
$cliente->setTelefono($_POST["telefono"]);
$cliente->setMail($_POST["mail"]);
$cliente->setContacto($_POST["contacto"]);

// ejecutamos la consulta
$resultado = $cliente->grabaCliente();

// retorna la id del registro
echo json_encode(array("id" => $resultado));
?>
