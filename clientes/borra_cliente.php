<?php

/**
 *
 * clientes/borra_cliente.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro la id de un cliente y
 * elimina el registro
 *
*/

// incluimos e instanciamos la clase
require_once ("clientes.class.php");
$cliente = new Clientes();

// asignamos el valor y ejecutamos
$resultado = $cliente->borraCliente($_GET["id"]);

// retornamos el estado de la operación
echo json_encode(array("Error" => $resultado));

?>
