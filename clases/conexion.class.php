<?php

/**
 *
 * Class Conexion | clases/conexion.class.php
 *
 * @package     Stock
 * @subpackage  Clases
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * clase inspirada en codigoprogramacion.com que lo que hace es heredar y
 * sobreescribir el constructor de pdo para establecer una conexión con la base
 *
*/

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Conexion extends PDO {

    // definimos el estado del servidor
    private $estado = "Desarrollo";

    // definimos las variables de clase
    private $tipo_de_base = 'mysql';
    private $host = 'localhost';
    private $usuario;
    private $contraseña;

    /**
     * Constructor de la clase, establece la conexión con la base
     * de acuerdo a las constantes establecidas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function __construct() {

        // según el estado del servidor
        if ($this->estado == "Desarrollo"){

            // conexión para el servidor de desarrollo
            $this->usuario = 'claude';
            $this->contrasena = 'gamaeco';
            $this->nombre_de_base = "stock";

        // si está en producción
        } else {

            // conexión para el servidor de producción
            $this->usuario = 'c1350643_stock';
            $this->contrasena = '17ripaRUmi';
            $this->nombre_de_base = 'c1350643_stock';

        }

        // Sobreescribo el método constructor de la clase PDO
        // y utilizamos la captura de errores
        try {

            // llamamos al constructor del padre
            parent::__construct($this->tipo_de_base.':host='.$this->host.';dbname='.$this->nombre_de_base, $this->usuario, $this->contrasena);

            // seteamos la página de códigos
            parent::exec("SET CHARACTER_SET_CLIENT=utf8;");
            parent::exec("SET CHARACTER_SET_RESULTS=utf8;");
            parent::exec("SET COLLATION_CONNECTION=utf8_unicode_ci;");

            // si está en modo desarrollo
            if ($this->estado == "Desarrollo"){

                // fijamos el nivel de pdo para que envíe un warning
                parent::setAttribute(PDO::ERRMODE_WARNING, PDO::ERRMODE_EXCEPTION);

            }

        // si hubo algún error lo presentamos
        } catch(PDOException $e){

            // presenta el error
            echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalle: ' . $e->getMessage();
            exit;

        }

    }

    /**
     * Método público que retorna el nombre de la base en la que
     * estamos conectados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string
     */
    public function getBase(){
        return $this->nombre_de_base;
    }

}

?>
