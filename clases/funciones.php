<?php

/*

    Nombre: funciones.php
    Autor: Lic. Claudio Invernizzi
    Mail: cinvernizzi@gmail.com
    Fecha: 12/05/2016
    Licencia GPL
    Comentarios: Serie de herramientas de validación y depuración

*/

// ValidaFecha
// Parámetros: una cadena en el formato dd/mm/yyyy
// Retorna: true o false
// Propósito: esta función recibe como parámetro una cadena en formato
//            español, la parsea y se la pasa a la función checkdate
//            retorna verdadero si la fecha es correcta, caso contrario
//            retorna falso
function ValidaFecha($fecha){

    // primero parseamos la fecha
    $matriz = explode ("/", $fecha);

    // asignamos los valores
    $dia = $matriz[0];
    $mes = $matriz[1];
    $anio = $matriz[2];

    // ahora verificamos la fecha y retornamos el
    // resultado de la operación
    if (checkdate($mes,$dia,$anio)){
        return true;
    } else {
        return false;
    }

}

// Nombre: StrtoUTF
// Parámetros: una cadena de texto
// Retorna: una cadena de texto codificada en utf8
// Propósito: función que recibe como parámetro una cadena de texto,
//            verifica que esté en utf-8 y en todo caso la codifica
//            y la retorna
function StrtoUTF($cadena){

    // verifica la página de códigos con una
    // comparación estricta
    if (mb_detect_encoding($cadena, 'UTF-8', true) === false) {
        $cadena = utf8_encode($cadena);
    }

    // retorna la cadena
    return $cadena;

}

// Nombre: GrabaConsulta
// Parámetros una cadena de texto
// Retorna: nada
// Propósito: función que recibe como parámetro una consulta sql, que puede
//            ser también una cadena de texto, abre el archivo (o lo crea según
//            el caso) en el disco local, graba la consulta en el mismo y
//            luego cierra el archivo.
function GrabaConsulta($sql){

    // obtiene la raíz del sitio
    $raiz = $_SERVER['DOCUMENT_ROOT'];

    // setea el nombre del archivo agregando el path
    $archivo = $raiz . "/" . "actualizar.sql";

    // abre el archivo
    $puntero = fopen($archivo, "a");

    // agrega el salto de línea al final del archivo
    $sql = $sql . "\n";

    // graba la consulta
    fwrite($puntero, $sql);

    // cierra el archivo
    fclose($puntero);

}
?>
