<?php

/**
 *
 * Class Backup | clases/backup.class.php
 *
 * @package     Stock
 * @subpackage  Clases
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que genera un archivo con el backup de toda la base de datos
 *
*/

// inclusión de archivos
require_once ("conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Backup{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                      // puntero a la base de datos
    protected $SQL;                       // cadena con las consultas
    protected $Base;                      // nombre de la base de datos a copiar
    protected $NombreBackup;              // nombre del archivo comprimido

    /**
     * Constructor de la clase, establece la conexión con la base
     * y genera el nombre del archivo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // creamos el nombre del archivo teniendo en cuenta
        // la fecha
        $this->NombreBackup = "stock-" . date('d-m-Y') . ".zip";

        // inicializamos las cadenas
        $this->SQL = "";
        $this->Base = $this->Link->getBase();

    }

    /**
     *
     * esta es la función pública que retorna el backup
     * @copyright (c) 2016, Lic. Claudio Invernizzi
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @version 1.0
     * @param type $base cadena con el nombre que se dará al archivo
     * @return $NombreBackup cadena con el nombre del archivo
     *
     */
    public function Dump(){

        // generamos el archivo sql
        $this->generaBackup();

        // lo guardamos en disco
        $this->GrabaArchivo();

        // comprimimos y eliminamos el archivo sql
        $this->ComprimirArchivo();

        // retornamos el nombre del archivo
        return $this->NombreBackup;

    }

    /**
     * Procedimiento que crea el backup de todas las bases de datos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function generaBackup(){

        // primero obtenemos un array con los nombres de las tablas
        $tablas = array();
        $resultado = $this->Link->query('SHOW TABLES');

        // obtenemos la matriz completa
        $fila = $resultado->fetchAll(PDO::FETCH_BOTH);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($fila, CASE_LOWER);

        // ahora recorremos el vector con los nombres de cada tabla
        foreach($fila AS $registro){

            // aquí agregamos el nombre del registro que es el nombre
            // de la tabla a la matriz
            $tablas[] = $registro[0];

        }

        // eliminamos la base si existe
        $this->SQL = "DROP DATABASE IF EXIST $this->Base\n";

        // iniciamos la creación de la cadena sql
        $this->SQL .= "CREATE DATABASE IF NOT EXISTS $this->Base;\n\n";
        $this->SQL .= "USE $this->Base ;\n\n";

        // ahora iniciamos un dump recorriendo el vector
        // de tablas
        foreach ($tablas AS $tabla){

            // agregamos la sentencia sql
            $this->SQL .= "DROP TABLE IF EXISTS $tabla;";

            // ahora obtenemos la sentencia de creación
            $resultado = $this->Link->query("SHOW CREATE TABLE $tabla");
            $fila = $resultado->fetch(PDO::FETCH_BOTH);

            // agregamos la sentencia de creación
            $this->SQL .= "\n\n $fila[1] ;\n\n";

            // obtenemos todos los registros de la tabla
            $resultado = $this->Link->query("SELECT * FROM $tabla");
            $fila = array_change_key_case($fila, CASE_LOWER);
            $fila = $resultado->fetchAll(PDO::FETCH_BOTH);

            // iniciamos un bucle
            foreach ($fila AS $registro){

                // ahora agregamos la sentencia
                $this->SQL .= "INSERT INTO $tabla VALUES(";

                // obtenemos la cantidad de campos
                $campos = count($registro);

                // iniciamos un bucle
                for ($i = 0; $i < $campos -1; $i++){

                   // verificamos que el campo no esté vacío
                   if (isset($registro[$i])){

                       // agregamos al texto
                       $this->SQL .= "'$registro[$i]'";

                   // si el campo está vacío
                   } else {

                       // agregamos la cadena vacía
                       $this->SQL .= "''";

                   }

                   // si no es el último campo
                   if ($i < $campos -2 ){

                       // agregamos el separador
                       $this->SQL .= ", ";

                   }

                }

                // cerramos la cadena
                $this->SQL .= ");\n";

            }

            // insertamos retornos de carro entre tablas
            $this->SQL .= "\n\n\n";

        }

    }

    /**
     * Método que toma la cadena con las sentencias sql del backup
     * y graba el archivo en disco
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function GrabaArchivo(){

        // abrimos el archivo para escritura
        $archivo = fopen("backup.sql","w");

        // grabamos la variable en el archivo
        fwrite($archivo, $this->SQL . PHP_EOL);

        // cerramos el archivo
        fclose($archivo);

    }

    /**
     * Método que toma la cadena sql con el backup y o comprime
     * luego elimina el archivo original
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function ComprimirArchivo(){

        // instanciamos la clase zip
        $zip = new ZipArchive();

        // tratamos de crear el archivo comprimido
        if($zip->open($this->NombreBackup,ZIPARCHIVE::CREATE)===true) {

            // agregamos el archivo sql
            $zip->addFile('backup.sql');
            $zip->close();

            // eliminamos el archivo original
            unlink ("backup.sql");

        }

    }

}

?>
