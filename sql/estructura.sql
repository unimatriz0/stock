/*

    Nombre: estructura.sql
    Fecha: 15/04/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com

    Comentarios:
    Archivo con el análisis de la estructura de una base de datos para un
    sistema de control de stock, emisión de remitos de recepción
    El sistema debe, controlar la recepción de unidades (en este caso
    celulares) y emitir un remito.
    Debe además, contar con un módulo de ingreso de repuestos y mercadería
    y un módulo de consumo de esos repuestos que descuente del stock total
    aquí hay tres reportes básicos (ingreso de unidades y repuestos y
    egreso de repuestos).
    El sistema tiene que estar orientado a objetos y con un criterio
    modular que permita agregar otros componentes como facturación,
    alertas, etc.

    La estructura de funcionamiento es la siguiente
    Clientes -> Tabla con los datos de los clientes
    Marcas -> Tabla de las marcas de productos
    Modelos -> Tabla con los modelos correspondientes a cada marca y el
               valor de compra y de venta
    Ingreso -> Elementos que ingresan al stock
    Egreso -> Ventas egresan del stock, tienen que estar relacionados
              con los modelos e indican la cantidad de unidades vendidas
              y el importe de la venta
    Inventario -> El existente de modelos, esta tabla se actualiza por
                  triggers definidos en la base (al ingresar un repuesto
                  al stock o al ser asignado a una venta)
    Remitos -> Contiene el archivo pdf junto con el importe total de la
               venta (que puede constar de varios modelos) existe como
               referencia porque los valores de venta y de valor en
               stock pueden variar
    Vistas con resumen de la información

 */

-- elimina la base si existe
DROP DATABASE IF EXISTS stock;

-- creación de la base de datos
CREATE DATABASE stock;

-- selecciona la base
USE stock;

-- seteamos las propiedades de la conexión
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_unicode_ci;

/*

    Estructura de la tabla usuarios
    La idea es que existan autorizaciones por usuario, de forma tal que
    un usuario pueda ingresar unidades para reparar, pero no esté
    autorizado a ingresar material de stock, de igual forma, el usuario
    que realiza las reparaciones, no debería poder tocar la tabla de
    existencias (sino dibuja lo que quiere)
    id: int, autonumérico, clave única del registro
    Nombre: varchar(250) , nombre completo del usuario
    Usuario: varchar(50), nombre de usuario para la base
    Password: varchar(80), contraseña encriptada
    Clientes: set, tipo de acceso a la tabla de unidades y clientes ingresados
              (consultar, editar, borrar)
    Ingresos: set, tipo de acceso a la tabla de material
              ingresado al stock (consultar, editar, borrar)
    Egresos: set, tipo de acceso a la tabla de material
             utilizado, (consultar, editar, borrar)
    Admin: set, si tiene permiso para administraro usuarios (Si/No)
    Activo: set, si está activo al haber integridad referencial no se puede
            eliminar el usuario

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS usuarios;

-- la recreamos
CREATE TABLE usuarios(
    id int(3) unsigned NOT NULL AUTO_INCREMENT,
    nombre varchar(250) NOT NULL,
    usuario varchar(40) NOT NULL,
    password varchar(100) NOT NULL,
    clientes set('Consultar', 'Editar', 'Borrar'),
    ingresos set('Consultar', 'Editar', 'Borrar'),
    egresos set('Consultar', 'Editar', 'Borrar'),
    administrador set('Si', 'No'),
    activo set('Si', 'No'),
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Usuarios autorizados del sistema';

-- insertamos los registros iniciales
INSERT INTO usuarios (nombre, usuario, password, clientes, ingresos, egresos, administrador, activo)
            VALUES ("Claudio Invernizzi", "claude", MD5("claude"), "Borrar", "Borrar", "Borrar", "Si", "Si"),
                   ("Renso Purriños", "renso", MD5("claude"), "Borrar", "Borrar", "Borrar", "Si", "Si");

/*

    Auditoría de usuarios, la misma estructura añadiendo el campo
    evento

*/

-- elimina la tabla de auditoria
DROP TABLE IF EXISTS auditoria_usuarios;

-- la recreamos
CREATE TABLE auditoria_usuarios(
    id int(3) unsigned NOT NULL,
    nombre varchar(250) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL,
    usuario varchar(50) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL,
    password varchar(50) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL,
    clientes set('Consultar', 'Editar', 'Borrar'),
    ingresos set('Consultar', 'Editar', 'Borrar'),
    egresos set('Consultar', 'Editar', 'Borrar'),
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    administrador set('Si', 'No'),
    activo set('Si', 'No'),
    evento set('Edición','Eliminación') CHARSET utf8 COLLATE utf8_unicode_ci,
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de usuarios';

/*

    Estructura de la base de clientes
    id (int) autonumérico, clave única del registro
    Nombre varchar (250) nombre completo del cliente
    Localidad: varchar(100) localidad donde reside el cliente
    Domicilio: varchar(250) domicilio del cliente
    Telefono: varchar(15) número de teléfono del cliente
    Mail: varchar(30) dirección de correo del cliente
    Contacto: set, fuente de contacto del cliente
    Observaciones: text comentarios del usuario
    Usuario: int, clave del usuario que ingresó el equipo en el sistema
    Fecha: date fecha de alta del registro

    Esta tabla tiene como índice foráneo la id de los usuarios, mas allá
    de la verificación por código, se garantiza que no se puede ingresar
    ningún registro si no está relacionado con la tabla de usuarios
    autorizados

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS clientes;

-- la recreamos
CREATE TABLE clientes (
    id int(5) unsigned NOT NULL AUTO_INCREMENT,
    cliente varchar(250) NOT NULL,
    localidad varchar(100) NOT NULL,
    domicilio varchar(250) NOT NULL,
    telefono varchar(15) NOT NULL,
    mail varchar(30) NOT NULL,
    contacto set('Facebook', 'Twitter', 'Instagram', 'Google', 'Mercado Libre', 'Otro'),
    observaciones text,
    usuario int(3) unsigned NOT NULL,
    fecha date NOT NULL,
    PRIMARY KEY(id),
    KEY cliente (cliente),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Datos de los clientes';

/*

    Estructura de la base de auditoría de recepción
    Comprende los mismos campos salvo que se elimina el contenido del
    remito para no desperdiciar espacio y se agregan un campo
    Evento: tipo de evento (edición, eliminación)

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS auditoria_clientes;

-- la recreamos
CREATE TABLE auditoria_clientes (
    id int(5) unsigned NOT NULL,
    cliente varchar(250) NOT NULL,
    localidad varchar(100) NOT NULL,
    domicilio varchar(250) NOT NULL,
    telefono varchar(15) NOT NULL,
    mail varchar(30) NOT NULL,
    contacto set('Facebook', 'Twitter', 'Google', 'Mercado Libre'),
    observaciones text,
    usuario int(3) unsigned NOT NULL,
    evento set('Edición', 'Eliminación'),
    fecha date NOT NULL,
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY cliente (cliente),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de clientes';

/*

    Tabla auxiliar de colores

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS colores;

-- la creamos desde cero
CREATE TABLE colores(
    id tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    color varchar(20),
    PRIMARY KEY(id),
    KEY color(color)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Diccionario de colores';

-- insertamos los valores iniciales
INSERT INTO colores(color) VALUES
        ('Blanco'),
        ('Negro'),
        ('Gris'),
        ('Rojo'),
        ('Verde'),
        ('Azul'),
        ('Transparente'),
        ('Dorado'),
        ('Plateado'),
        ('Estampado'),
        ('Diseño'),
        ('Madera'),
        ('Otro');

/*

    Estructura de la tabla de marcas
    id int clave de la marca
    marca varchar descripción de la marca

*/

-- eliminamos la tabla de marcas si existe
DROP TABLE IF EXISTS marcas;

-- la creamos desde cero
CREATE TABLE marcas(
    id tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    marca varchar(50) NOT NULL,
    PRIMARY KEY(id),
    KEY marca(marca)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Diccionario de Marcas';

-- cargamos los valores iniciales
INSERT INTO marcas (marca) VALUES
            ('Samsung'),
            ('LG'),
            ('Motorola'),
            ('Sony'),
            ('Nokia'),
            ('Otra');

/*

    Estructura de la tabla de modelos
    id int(3) clave del registro
    marca tinyint(3) clave de la marca
    descripcion varchar(50) descripción del repuesto
    critico int(4) cantidad crítica
    costo decimal, costo de compra
    incmayorista decimal, porcentaje de ganancia mayorista
    incminorista decimal, porcentaje de ganancia minorista
    mayorista decimal, valor de venta mayorista
    minorista decimal, valor de venta minorista
    fecha date fecha de alta
    usuario int(3) clave de la tabla de usuarios

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS modelos;

-- creamos la tabla
CREATE TABLE modelos (
    id int(3) unsigned NOT NULL AUTO_INCREMENT,
    marca tinyint(3) unsigned NOT NULL,
    descripcion varchar(50) NOT NULL,
    critico decimal(10,2) unsigned NOT NULL,
    costo decimal(9,2) unsigned NOT NULL,
    incmayorista decimal(5,2) unsigned NOT NULL,
    incminorista decimal(5,2) unsigned NOT NULL,
    mayorista decimal(9,2) unsigned NOT NULL,
    minorista decimal(9,2) unsigned not null,
    fecha timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(3) unsigned NOT NULL,
    PRIMARY KEY(id),
    KEY marca(marca),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Diccionario de Artículos';

-- creamos la tabla de auditoría de modelos
DROP TABLE IF EXISTS auditoria_modelos;

-- creamos la tabla
CREATE TABLE auditoria_modelos (
    id int(3) unsigned NOT NULL,
    marca tinyint(3) unsigned NOT NULL,
    descripcion varchar(50) NOT NULL,
    critico decimal(10,2) unsigned NOT NULL,
    costo decimal(9,2) unsigned NOT NULL,
    incmayorista decimal(5,2) unsigned NOT NULL,
    incminorista decimal(5,2) unsigned NOT NULL,
    mayorista decimal(9,2) unsigned NOT NULL,
    minorista decimal(9,2) unsigned not null,
    fecha date NOT NULL,
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(3) unsigned NOT NULL,
    KEY id(id),
    KEY marca(marca),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de Artículos';

/*

    Estructura de la tabla de stock
    Esta tabla se utilizaría para consultas, casi sería una vista o
    resumen del estado del sistema, cada ingreso de material incrementa
    los valores en existencia y cada egreso de material a la inversa
    No requiere auditoría
    id: int, autonumérico, clave única del registro
    item: int, clave con la descripción, relacionada con el campo id de
          la tabla de repuestos
    cantidad: decimal, cantidad en existencia
    crítico: int, valor crítico inferior en el que se disparan las
             alarmas
    fecha: date fecha de la última operación
    usuario: int clave del usuario

    Aquí también tenemos claves foráneas, con el item del diccionario de
    repuestos y con la tabla de usuarios autorizados

*/

-- eliminamos si existe
DROP TABLE IF EXISTS inventario;

-- la recreamos
CREATE TABLE inventario(
    id int(5) unsigned NOT NULL AUTO_INCREMENT,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    critico decimal(10,2) unsigned NOT NULL,
    fecha timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(3) unsigned NOT NULL,
    PRIMARY KEY (id),
    KEY item(item),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Resumen de stock existente';

/*

    Tabla de auditoría de inventario, en rigor no es una tabla que
    requiera auditoría, pero la ventaja es que permite tener un
    historial del inventario y conocer con precisión el estado del
    mismo en una determinada fecha

    El control del inventario se realiza a través de los triggers
    de la base de datos y no sería necesario una interfaz
    administrativa

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS auditoria_inventario;

-- la recreamos
CREATE TABLE auditoria_inventario(
    id int(5) unsigned NOT NULL,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    critico decimal(10,2) unsigned NOT NULL,
    fecha date NOT NULL,
    usuario int(3) unsigned NOT NULL,
    evento set('Edición', 'Eliminación'),
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY item(item),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de stock existente';

/*

    Estructura de la tabla de material entrante
    id: int, autonumerico, clave única del registro
    item: int, clave con la descripción, relacionada con el campo id
          de la tabla de repuestos
    cantidad: int, número de unidades ingresantes
    factura: varchar número de factura
    importe: decimal, importe total de la factura
    fecha: fecha en que se ingresó el material al depósito
    usuario: usuario que ingresó el material al depósito

    Esta tabla tiene un índice foráneo a la tabla de repuestos,
    garantiza la integridad estructural de no poder ingresar ningún
    repuesto que no halla sido ingresado en el diccionario (mas allá
    de la validación que se haga en la carga de datos) igual con la
    tabla de usuarios autorizados

*/

-- eliminamos si existe
DROP TABLE IF EXISTS ingresos;

-- la recreamos
CREATE TABLE ingresos(
    id int(5) unsigned NOT NULL AUTO_INCREMENT,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    factura varchar(100) NOT NULL,
    importe decimal(10,2) unsigned NOT NULL,
    fecha timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(3) unsigned NOT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY fecha(fecha),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Materiales ingresados a depósito';

/*

    Estructura de la tabla de auditoría de material entrante
    La misma que en la tabla, agregando el campo evento (es necesario tener
    un registro de las modificaciones y también dar la posibilidad de
    borrar una entrada en stock, pero por eso hay que tener una auditoría
    especialmente de quien hizo los cambios en la tabla)

    Como mysql solo permite definir un trigger para after (update o
    delete) hasta la versióon 5.5 (en la 5.7 permite definir mas
    de un trigger por evento) por compatibilidad vamos a seguir usando
    un solo evento, tampoco permite llamar a procedures dentro de los
    triggers pero si permite definir dentro del trigger varias funciones
    o consultas, esta es la técnica que vamos a utilizar .

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS auditoria_ingresos;

-- la recreamos
CREATE TABLE auditoria_ingresos(
    id int(5) unsigned NOT NULL,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    factura varchar(100) NOT NULL,
    importe decimal(10,2) unsigned NOT NULL,
    fecha date NOT NULL,
    usuario int(3) unsigned NOT NULL,
    evento set('Edición','Eliminación'),
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY item(item),
    KEY fecha(fecha),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de Materiales ingresados a depósito';

/*

    Estructura de la tabla de material saliente
    id: int, autonumérico, clave única del registro
    item: clave del repuesto utilizado
    cantidad: int número de egresos
    importe: decimal importe total del egreso
    tipo_venta: enum indica si cabe el costo mayorista o minorista
    cliente: int cliente que recibió el producto
    tipo_venta: enum, indica si se usó el precio mayorista o minorista
    remito: int clave del remito
    fecha: fecha en que fue entregado el elemento
    usuario: usuario que entregó el elemento

    Esta tabla tiene dos índices foráneos, a la tabla de unidades
    ingresadas para reparar y al diccionario de repuestos, mas allá
    de la validación de datos que se haga en la carga, garantiza la
    integridad estructural

*/

-- eliminamos la table
DROP TABLE IF EXISTS egresos;

-- la recreamos
CREATE TABLE egresos(
    id int(5) unsigned NOT NULL AUTO_INCREMENT,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    importe decimal(10,2) unsigned NOT NULL,
    cliente int(5) unsigned NOT NULL,
    tipo_venta enum("Mayorista", "Minorista") NOT NULL,
    remito int(5) unsigned NOT NULL,
    fecha timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(3) NOT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY remito(remito),
    KEY fecha(fecha),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Egresos del depósito';

/*

    Auditoría de los repuestos utilizados, la misma que en la tabla de
    material, con el agregado del campo evento
    Aquí vamos a usar la misma técnica que con el inventario para
    mantenerlo actualizado, usar varias funciones dentro de un mismo
    trigger

*/

-- eliminamos si existe
DROP TABLE IF EXISTS auditoria_egresos;

-- la recreamos
CREATE TABLE auditoria_egresos(
    id int(5) unsigned NOT NULL,
    item int(5) unsigned NOT NULL,
    cantidad decimal(10,2) unsigned NOT NULL,
    importe decimal(10,2) unsigned NOT NULL,
    cliente int(5) unsigned NOT NULL,
    tipo_venta enum("Mayorista", "Minorista") NOT NULL,
    fecha date NOT NULL,
    usuario int(3) UNSIGNED NOT NULL,
    evento set('Edición','Eliminación'),
    fecha_evento timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY item(item),
    KEY fecha(fecha),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Auditoría de Repuestos';

/*
    Tabla con los remitos

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS remitos;

-- la recreamos
CREATE TABLE remitos (
    id int(5) unsigned NOT NULL AUTO_INCREMENT,
    remito longblob,
    importe decimal(10,2) UNSIGNED NOT NULL,
    cliente int(5) unsigned not null,
    impreso enum("Si", "No") DEFAULT "No",
    pago set('Efectivo', 'Transferencia', 'Cheque', 'Mercado Pago'),
    comprobante varchar(50) NOT NULL,
    usuario int(3) UNSIGNED NOT NULL,
    fecha timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY cliente(cliente),
    KEY usuario(usuario)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='Remitos generados';

/*

    Aquí definimos la vista que va a mostrar el inventario


*/

-- la eliminamos si existe
DROP VIEW IF EXISTS vw_inventario;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_inventario AS
       SELECT inventario.id AS id,
              marcas.marca AS marca,
              modelos.descripcion AS modelo,
              inventario.cantidad * modelos.costo AS costo,
              modelos.incmayorista AS incmayorista,
              modelos.mayorista AS mayorista,
              modelos.incminorista AS incminorista,
              modelos.minorista AS minorista,
              inventario.cantidad AS cantidad,
              inventario.critico AS critico,
              DATE_FORMAT(inventario.fecha,'%d/%d/%Y') AS fecha
       FROM inventario INNER JOIN modelos ON inventario.item = modelos.id
                       INNER JOIN marcas ON modelos.marca = marcas.id
       ORDER BY marcas.marca,
                modelos.descripcion;

/*

    Aquí definimos la vista que va a mostrar el historial de
    material ingresado al inventario

*/

-- eliminamos si existe
DROP VIEW IF EXISTS vw_ingresos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_ingresos AS
       SELECT ingresos.id AS id,
              marcas.marca AS marca,
              marcas.id AS id_marca,
              modelos.descripcion AS modelo,
              modelos.id AS id_modelo,
              ingresos.cantidad AS cantidad,
              ingresos.factura AS factura,
              ingresos.importe AS importe,
              DATE_FORMAT(ingresos.fecha,'%d/%m/%Y') AS fecha,
              usuarios.usuario AS usuario
       FROM ingresos INNER JOIN usuarios ON ingresos.usuario = usuarios.id
                     INNER JOIN modelos ON ingresos.item = modelos.id
                     INNER JOIN marcas ON modelos.marca = marcas.id
       ORDER BY marcas.marca,
                modelos.descripcion;

/*

    Aqui definimos la vista que va a mostrar el historial
    utilizado en los egresos


*/

-- la eliminamos si existe
DROP VIEW IF EXISTS vw_egresos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_egresos AS
       SELECT egresos.id AS id,
              clientes.cliente AS cliente,
              marcas.marca AS marca,
              modelos.descripcion AS modelo,
              egresos.cantidad AS cantidad,
              egresos.importe AS importe,
              egresos.tipo_venta AS tipo_venta,
              egresos.remito AS remito,
              DATE_FORMAT(egresos.fecha, '%d/%m/%Y') AS fecha_egreso,
              usuarios.usuario AS usuario
       FROM egresos INNER JOIN usuarios ON egresos.usuario = usuarios.id
                    INNER JOIN modelos ON egresos.item = modelos.id
                    INNER JOIN marcas ON modelos.marca = marcas.id
                    INNER JOIN clientes ON egresos.cliente = clientes.id;

-- eliminamos si existe
DROP VIEW IF EXISTS vw_modelos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_modelos AS
       SELECT modelos.id AS id,
              marcas.marca AS marca,
              marcas.id AS id_marca,
              modelos.descripcion AS descripcion,
              modelos.critico AS critico,
              modelos.costo AS costo,
              modelos.incmayorista AS incmayorista,
              modelos.mayorista AS mayorista,
              modelos.incminorista AS incminorista,
              modelos.minorista AS minorista,
              DATE_FORMAT(modelos.fecha, '%d/%m/%Y') AS fecha,
              usuarios.usuario AS usuario
       FROM modelos INNER JOIN marcas ON modelos.marca = marcas.id
                    INNER JOIN usuarios ON modelos.usuario = usuarios.id
       ORDER BY marcas.marca,
                modelos.descripcion;

-- eliminamos la vista de remitos
DROP VIEW IF EXISTS vw_remitos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_remitos AS
       SELECT remitos.id AS idremito,
              clientes.cliente AS cliente,
              clientes.localidad AS localidad,
              clientes.domicilio AS domicilio,
              marcas.marca AS marca,
              modelos.descripcion AS descripcion,
              remitos.impreso AS impreso,
              remitos.pago AS pago,
              egresos.cantidad AS cantidad,
              egresos.importe AS importe,
              egresos.tipo_venta AS tipo_venta,
              DATE_FORMAT(remitos.fecha, '%d/%m/%Y') AS fecha,
              remitos.comprobante AS comprobante,
              remitos.remito AS remito,
              remitos.importe AS total,
              clientes.observaciones AS observaciones
        FROM remitos INNER JOIN clientes ON remitos.cliente = clientes.id
                     INNER JOIN egresos ON remitos.id = egresos.remito
                     INNER JOIN modelos ON egresos.item = modelos.id
                     INNER JOIN marcas ON modelos.marca = marcas.id;

-- eliminamos la vista de clientes
DROP VIEW IF EXISTS vw_clientes;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW vw_clientes AS
       SELECT clientes.id AS id,
              clientes.cliente AS cliente,
              clientes.localidad AS localidad,
              clientes.domicilio AS domicilio,
              clientes.telefono AS telefono,
              clientes.mail AS mail,
              clientes.contacto AS contacto,
              clientes.observaciones AS observaciones,
              clientes.usuario AS idusuario,
              usuarios.usuario AS usuario
       FROM clientes INNER JOIN usuarios ON clientes.usuario = usuarios.id;
