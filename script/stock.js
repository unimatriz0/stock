/*

    Archivo con las rutinas javascript del sistema de stock
    Autor: Lic. Claudio Invernizzi
    Fecha: 25/04/2016
    Mail: cinvernizzi@gmail.com
    Licencia: GPL

*/

// Nombre: VerificaCritico
// Parámetros: la id del item a verificar
// Retorna: Nada
// Propósito: función que recibe como parámetro la id de un elemento de stock a verificar, si
//            se encuentra por debajo del crítico presenta una alerta, utilizado en la grabación
//            y edición de unidades
function VerificaCritico(id){

    // declaración de variables
    var mensaje;

    // llama la rutina php
    $.get('repuestos/verifica_inventario.php?id=' + id,
        function(data){

            // si hay elementos
            if (data.length !== 0){

                // inicializa la cadena
                mensaje = "<b>Atención, hay inventario crítico</b><br>";

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // termina de armar el mensaje
                    mensaje += "El repuesto " +  data[i].repuesto + " del ";
                    mensaje += data[i].marca + " " + data[i].modelo + " ";
                    mensaje += "se encuentra con stock crítico.";

                }

                critico = Atencion(mensaje);

            }

        }, "json");

    // cerramos
    return false;

}
