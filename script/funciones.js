/*
 * Nombre: funciones.js
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 01/01/2014
 * Licencia: GPL
 * E-Mail: cinvernizzi@gmail.com
 * Comentarios: serie de funciones utilizadas por varias rutinas del sitio
 *
 */

/**
 * Nombre: Fecha_Correcta
 * @param {string} fecha - una cadena en formato dd-mm-aaaa
 * @return {boolean} - true o false según la verificación
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Función que recibe como parámetro una cadena y verifica si se trata de una
 * fecha correcta, presentando los mensajes de error de acuerdo al problema
 * hallado
 */
function Fecha_Correcta(fecha){

    // inicialización de variables
    var dia = 0;
    var mes = 0;
    var anio = 0;
    var mensaje;

    // verifica la longitud de la cadena
    if (fecha.length !== 10){

        // presenta el mensaje
        mensaje = "La fecha debe ser en formato dd-mm-aaaa";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    }

    // determina el día, mes y año
    dia = fecha.substr(0,2);
    mes = fecha.substr(3,2);
    anio = fecha.substr(6,10);

    // verifica que el año sea un número
    if (isNaN(anio)){

        // presenta el mensaje
        mensaje = "El año debe ser un número";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    // toma el año y verifica que esté comprendido entre
    // 1900 y 2100 (valores lógicos al cabo)
    } else if (anio < 1900 || anio > 2100){

        // presenta el mensaje
        mensaje = "El año debe estar comprendido\n";
        mensaje = mensaje + "entre 1900 y 2100.";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    }

    // verifica que el mes sea un número
    if (isNaN(mes)){

        // presenta el mensaje
        mensaje = "El mes debe ser un número";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    // verifica que el mes no sea mayor que 12
    } else if (mes > 12 || mes < 1){

        // presenta el mensaje
        mensaje = "El mes debe ser mayor que 1\n";
        mensaje = mensaje + "y menor que 12.";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    }

    // verifica que el día sea un número
    if (isNaN(dia)){

        // presenta el mensaje
        mensaje = "El día debe ser un número";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    }

    // si el día es mayor de 31
    if (dia > 31) {

        // setea el mensaje
        mensaje = "El día no puede ser mayor de 31";
        new jBox('Notice', {content: mensaje, color: 'red'});
        return false;

    // si es un mes de treinta días
    } else if (mes === 4 || mes === 6 || mes === 9 || mes === 11){

        // verifica que no sea mayor de 30
        if (dia > 30){

            // presenta el mensaje
            mensaje = "Para el mes " + mes + " el\n";
            mensaje = mensaje + "día no puede ser mayor de 30";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

    // si el mes es febrero
    } else if (mes === 2){

        // si es un año biciesto
        if (anio % 4 === anio / 4){

            if (dia > 29){

                // presenta el mensaje
                mensaje = "Febrero no puede tener mas de 29 días";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        // de otra forma
        } else {

            if (dia > 28){

                // presenta el mensaje
                mensaje = "Febrero no puede tener mas de 28 días";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

    }

    // si llegó hasta aquí es porque está todo bien
    return true;

}

/**
 * Nombre: echeck
 * @param {string} str una cadena con un mail
 * @return {boolean} - true o false según la cadena
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Función que recibe como parámetro una cadena de texto y retorna correcto o
 * falso si es una dirección de mail correcta
 * Obtenido de Javascript Source
 */
function echeck(str) {

  // definición de variables
  var at="@";
  var dot=".";
  var lat=str.indexOf(at);
  var lstr=str.length;
  var ldot=str.indexOf(dot);
  if (str.indexOf(at)===-1){
     return false;
  }
  if (str.indexOf(at)=== -1 || str.indexOf(at)=== 0 || str.indexOf(at) === lstr){
     return false;
  }
  if (str.indexOf(dot)=== -1 || str.indexOf(dot) === 0 || str.indexOf(dot) === lstr){
      return false;
  }
  if (str.indexOf(at,(lat+1)) !== -1){
     return false;
  }
  if (str.substring(lat-1,lat) === dot || str.substring(lat+1,lat+2) === dot){
     return false;
  }
  if (str.indexOf(dot,(lat+2)) === -1){
     return false;
  }
  if (str.indexOf(" ") !== -1){
     return false;
  }
   return true;
}

/**
 * Nombre: stringToDate
 * @param {string} cadena - con una fecha en formato dd/mm/aaaa
 * @return {string} - un objeto fecha
 * @author Claudio Invernizzi - <cinvernizzi@gmail.com>
 * función que recibe una cadena del objeto input fecha y retorna el objeto fecha
 * asume que la cadena se encuentra en el formato dd/mm/aaaa
 */
function stringToDate(cadena) {

    // declaración de variables
    var dia = cadena.substring(0, 2);
    var mes = cadena.substring(3, 5);
    var anio = cadena.substring(6, 10);

    // compone la nueva fecha
    cadena = anio + "," + mes + "," + dia;

    // crea un nuevo objeto fecha y le pasa el string
    var fecha = new Date(cadena);

    // retorna el objeto
    return fecha;

}

/**
 * Nombre: dateToString
 * @param {string} fecha - objeto en formato fecha
 * @return {string} - cadena formateada en formato mysql
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * función que recibe un objeto fecha y retorna una cadena
 * con la fecha en formato mysql
 */
function dateToString(fecha) {

    // inicialización de variables
    var mes = (fecha.getMonth() + 1).toString();
    var dia = fecha.getDate().toString();

    // convierte
    if(mes.length == 1){
        mes = "0" + mes;
    }
    if(dia.length == 1) {
        dia = "0" + dia;
    }

    // retornamos
    return fecha.getFullYear() + "-" + mes + "-" + dia;

}

/**
 * Nombre: dateToFecha
 * @param {string} fecha - un objeto fecha
 * @return {string} - cadena en formato español
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * funcion que recibe un objeto fecha y retorna una cadena
 * con la fecha en formato español
 */
function dateToFecha(fecha){

    // inicialización de variables
    var mes = (fecha.getMonth() + 1).toString();
    var dia = fecha.getDate().toString();

    // convierte
    if(mes.length == 1) {
        mes = "0" + mes;
    }
    if(dia.length == 1) {
        dia = "0" + dia;
    }

    // retornamos
    return dia + "/" + mes + "/" + fecha.getFullYear();

}

/**
 * Nombre: Fecha_Mayor
 * @param {string} fecha - un objeto fecha
 * @param {string} fecha2 - un objeto fecha
 * @return {boolean}
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * función que recibe como dato dos objetos fecha, devuelve
 * verdadero si la segunda fecha es mayor que la primera
 * o si ambas fechas son iguales
 */
function Fecha_Mayor(fecha, fecha2){

    // inicializa la variable de retorno
    var resultado = false;

    // si el segundo año es mayor
    if (fecha2.getFullYear() > fecha.getFullYear()) {

        // retorna verdadero
        resultado = true;

    // si son iguales
    } else if (fecha2.getFullYear() === fecha.getFullYear()) {

        // verifica el mes
        if (fecha2.getMonth() > fecha.getMonth()){

            // retorna verdadero
            resultado = true;

        // si son iguales
        } else if (fecha2.getMonth() === fecha.getMonth()) {

            // verifica el día
            if (fecha2.getDate() > fecha.getDate()){

                // retorna verdadero
                resultado = true;

            }

        }

    }

    // retorna la variable control
    return resultado;

}

/**
 * Nombre: FechaDiff
 * @param {string} inicial - fecha inicial
 * @param {string} final - fecha final
 * @return {int} - entero con los días transcurridos
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * función que recibe dos fechas en formado dd/mm/YYYY y calcula
 * los dias transcurridos entre la primera (inicial) y la
 * segunda (final)
 */
function FechaDiff(inicial,final){

    // descompone la fecha usando la barra como separador
    var aFecha1 = inicial.split('/');
    var aFecha2 = final.split('/');

    // convierte las fechas a formato utc
    var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
    var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);

    // calcula la diferencia y los dias transcurridos
    var dif = fFecha2 - fFecha1;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));

    // retorna la cantidad de dias
    return dias;

}

/**
 * Nombre: fechaActual
 * @return {string} - una cadena con la fecha del sistema
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * función que retorna la fecha actual en formato dd/mm/YYYY
 */
function fechaActual(){

    // obtenemos la fecha del sistema
    var ahora = new Date();
    var fecha = (ahora.getDate() + "/" + (ahora.getMonth() +1) + "/" + ahora.getFullYear());

    // retorna la cadena
    return fecha;

}

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @param {string} fecha - una cadena en formato dd/mm/YYYY
 * @return {string} dia - una cadena con el día de la semana
 * Método que recibe como parámetro una cadena en formato
 * español, crea el objeto fecha de javascript y retorna
 * el día de la semana que corresponde
 */
function diaSemana(fecha){

    // declaración de variables
    var dias = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');

    // convertimos a formato fecha de javascript
    fecha = stringToDate(fecha);

    // obtenemos el día
    var hoy = dias[fecha.getDay()];

    // retornamos
    return hoy;

}
