<?php

/**
 *
 * usuarios/usuarios.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la nómina de usuarios autorizados
 *
*/

// obtenemos el path de inclusión e insertamos la clase
require_once ("usuarios.class.php");

// instanciamos la clase
$usuario = new Usuarios();

// le pasamos a la clase el texto a buscar
$resultado = $usuario->listaUsuarios();

// inicia sesión y verifica si es administrador
$esadmin = $_COOKIE["Administrador"];

// define la tabla
echo "<table width='95%' align='center' border='0' id='usuarios'>";

// define los encabezados
echo "<thead>";
echo "<tr>";
echo "<th><b>Nombre</b></th>";
echo "<th><b>Usuario</b></th>";
echo "<th><b>Clientes</b></th>";
echo "<th><b>Ingresos</b></th>";
echo "<th><b>Egresos</b></th>";
echo "<th><b>Administrador</b></th>";
echo "<th><b>Activo</b></th>";
echo "<th></th>";            // aquí va el botón grabar
echo "<th></th>";            // aquí va el botón eliminar
echo "</tr>";
echo "</thead>";

// define el cuerpo de la tabla
echo "<tbody>";

// como recibimos la matriz de resultados en forma de un array
foreach($resultado AS $fila){

    // obtenemos el registro
    extract($fila);

    // abrimos la fila
    echo "<tr>";

    // definimos el formulario
    echo "<form name='$id_usuario' id='$id_usuario'>";

    // presentamos los datos

    // el nombre de usuario
    echo "<td>";
    echo "<input type='text' name='nombre_usuario'
           value='$nombre_usuario'
           size='20'
           required
           id='nombre_$id_usuario'
           title='Nombre completo del usuario'
           </td>";

    // el usuario de la base no se puede modificar
    echo "<td>$usuarios_usuario</td>";

    // si puede editar clientes
    echo "<td>";
    ControlCombo("clientes",$clientes_usuario, "editar_$id_usuario", "Indique si puede modificar clientes");
    echo "</td>";

    // si puede editar ingresos
    echo "<td>";
    ControlCombo("ingresos",$ingresos_usuario, "ingresos_$id_usuario", "Indique si puede ingresar mercadería");
    echo "</td>";

    // si puede editar egresos
    echo "<td>";
    ControlCombo("egresos",$egresos_usuario, "egresos_$id_usuario", "Indique si puede utilizar mercadería");
    echo "</td>";

    // si es administrador
    echo "<td>";
    ComboSiNo("administrador", $administrador_usuario, "administrador_$id_usuario", "Indique si puede administrar el sistema");
    echo "</td>";

    // si está activo
    echo "<td>";
    ComboSiNo("activo", $activo_usuario, "activo_$id_usuario", "Indique si el usuario está activo");
    echo "</td>";

    // si es administrador
    if ($esadmin == "Si"){

        // agregamos el botón grabar
        echo "<td>";
        echo "<input type='button' name='Btn_Grabar'
              title='Graba los datos del Usuario'
              class='botongrabar'
              onClick='usuarios.ActualizarUsuario($id_usuario)'>";
        echo "</td>";

        // si puede eliminar
        echo "<td>";
        if ($usuario->puedeBorrar($id_usuario)){

            // presenta el botón eliminar
            echo "<input type='button' name='BtnBorrar'
                   title='Eliminar el Usuario'
                   onClick='usuarios.BorraUsuario($id_usuario)'
                   class='botonborrar'>";
        }

        // cierra la columna
        echo "</td>";

    // si no es administrador
    } else {

        // dejamos las dos columnas en blanco
        echo "<td></td><td></td>";

    }
    // cerramos el formulario
    echo "</form>";

    // cerramos la fila
    echo "</tr>";

}

// cierra el cuerpo
echo "</tbody>";

// cierra la tabla
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

// Nombre: ComboSiNo
// Parámetros: el nombre de un control
//             el valor de un campo de la base
//             el valor del texto de ayuda
// Retorna: Nada
// Propósito: crea un select con el nombre del control recibido
//            el campo, la id y las opciones si / no del combo
function ComboSiNo($control, $campo, $id_control, $titulo=false){

    // crea el control
    echo "<select name='$control' size='1'
           id='$id_control'
           required";

    // si recibió el titulo
    if ($titulo){

        // lo agrega
        echo "title='$titulo'>";

    // si no lo recibió
    } else {

        // agrega uno genérico
        echo "title='Seleccione el valor de la lista'>";

    }

    // agregamos el primer elemento en blanco
    echo "<option></option>";

    // según el valor ingresado
    switch($campo){

        // si es si
        case "Si":
            echo "<option selected>Si</option>";
            echo "<option>No</option>";
            break;

        // si es no
        case "No":
            echo "<option>Si</option>";
            echo "<option selected>No</option>";
            break;

        // en otro caso
        default:
            echo "<option>Si</option>";
            echo "<option>No</option>";
            break;

    }

    // retornamos
    return;

}

// Nombre: ControlCombo
// Parámetros: el nombre de un control
//             el valor de un campo de la base
//             el valor del texto de ayuda
// Retorna: Nada
// Propósito: esta función crea un select con el nombre del control recibido
//            y agrega los elementos del combo que permiten seleccionar el
//            nivel de acceso del usuario, si el valor del campo de la base
//            recibido coincide con alguno de los elementos lo selecciona
//            por defecto
function ControlCombo($control, $campo, $id_control, $titulo=false){

    // abre el control
    echo "<select name='$control' size='1'
           id='$id_control'
           required";

    // si recibió el título
    if ($titulo){
        echo "title='$titulo'>";
    } else {
        echo "title='Seleccione el valor correspondiente'>";
    }

    // agrega el primer elemento en blanco
    echo "<option></option>";

    // según el valor del campo recibido
    switch ($campo) {

        // si puede consultar
        case "Consultar":
            echo "<option selected>Consultar</option>";
            echo "<option>Editar</option>";
            echo "<option>Borrar</option>";
            break;

        // si puede editar
        case "Editar":
            echo "<option>Consultar</option>";
            echo "<option selected>Editar</option>";
            echo "<option>Borrar</option>";
            break;

        // si puede borrar
        case "Borrar":
            echo "<option>Consultar</option>";
            echo "<option>Editar</option>";
            echo "<option selected>Borrar</option>";
            break;

        // valor por defecto
        default:
            echo "<option>Consultar</option>";
            echo "<option>Editar</option>";
            echo "<option>Borrar</option>";
            break;

    }

    // cierra el combo
    echo "</select>";

    // retornamos
    return;

}

?>
<SCRIPT>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Usuarios Autorizados</h1>");

    // definimos las propiedades de la tabla
    $('#usuarios').datatable({
        pageSize: 15,
        sort: [true, true, false, false, false, false, false, false, false],
        filters: [true, true, false, false, false, false, false, false, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
