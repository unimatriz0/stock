<?php

/**
 *
 * usuarios/salir.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Script que elimina las variables de sesión del usuario actual
 *
*/

// destruimos la cookie
setcookie("ID", "", 1 , "/");

// retorna siempre verdadero
echo json_encode(array("Error" => true));
?>
