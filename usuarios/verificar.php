<?php

/**
 *
 * usuarios/verificar.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que verifica si el usuario inició sesión en cuyo
 * caso retorna true
 *
*/

// iniciamos las variables
$estado = "";

// si existe la sesión
if (isset($_COOKIE["ID"])){

    // si no esta vacia
    if ($_COOKIE["ID"] == ""){
        $estado = true;
    } else {
        $estado = false;
    }

// si no esta autenticado
} else {
    $estado = false;
}

// retorna el estado
echo json_encode(array("Error" => $estado));
?>
