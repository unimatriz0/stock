<?php

/**
 *
 * usuarios/form_usuario.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que crea el formulario de carga de un nuevo usuario
*/

// obtenemos el nivel de acceso
$esadmin = $_COOKIOE["Administrador"];

// presenta el título
echo "<h2>Nuevo Usuario</h2>";

// definimos el formulario
echo "<form name='usuario' id='usuario'>";

// define la tabla
echo "<table width='95%' align='center' border='0'>";

// pide el nombre del usuario
echo "<tr>";
echo "<td colspan='3'>
      <b>Nombre Completo: </b>
      <input type='text' name='nombre'
           size='35'
           required
           title='Nombre completo del usuario'>";
echo "</td>";
echo "</tr>";

// pide el usuario
echo "<tr>";
echo "<td><b>Usuario: </b>
      <input type='text' name='usuario'
           size='10'
           required
           title='Usuario de la base de datos'>";
echo "</td>";

// pide la contraseña
echo "<td><b>Contraseña: </b>
      <input type='password' name='password'
           size='10'
           required
           title='Contraseña de ingreso'>";
echo "</td>";

// pide la verificación
echo "<td><b>Repetición: </b>
      <input type='password' name='repeticion'
           size='10'
           required
           title='Repita la contraseña'>";
echo "</td>";
echo "</tr>";

// pide si puede editar clientes
echo "<tr>";
echo "<td><b>Clientes: </b>";
ControlCombo("clientes", "Indique si podrá editar clientes");
echo "</td>";

// pide si puede editar ingresos
echo "<td><b>Ingresos: </b>";
ControlCombo("ingresos", "Indique si podrá editar ingresos");
echo "</td>";

// pide si puede editar egresos
echo "<td><b>Egresos: </b>";
ControlCombo("egresos", "Indique si podrá editar egresos");
echo "</td>";
echo "</tr>";

// pide si puede administrar
echo "<tr>";
echo "<td><b>Administrador: </b>";
ComboSiNo("administrador", "Indique si puede administrar el sistema");
echo "</td>";

// pide si está activo
echo "<td colspan='2'><b>Activo: </b>";
ComboSiNo("activo", "Indique si el usuario está activo");
echo "</td>";
echo "</tr>";

// inserta un separador
echo "<tr><td colspan='3'><br></td></tr>";

// si es administrador
if ($esadmin == "Si"){

    // presenta el botón grabar
    echo "<tr>";
    echo "<td style='text-align:center'>";
    echo "<input type='button' name='BtnGrabar'
          value='Grabar'
          class='boton_grabar'
          title='Agrega el usuario a la base'
          onClick='usuarios.GrabaUsuario()'>";
    echo "</td>";

    // presenta el botón cancelar
    echo "<td style='text-align:center'>";
    echo "<input type='button' name='BtnCancelar'
          value='Cancelar'
          class='boton_cancelar'
          title='Cierra este formulario'
          onClick='usuarios.layerUsuarios.destroy()'>";
    echo "</td>";
    echo "<td></td>";
    echo "</tr>";

}

// cierra la tabla
echo "</table>";

// cerramos el formulario
echo "</form>";

// Nombre: ComboSiNo
// Parámetros: el nombre de un control
//             el valor del texto de ayuda
// Retorna: Nada
// Propósito: crea un select con el nombre del control recibido
//            y las opciones si / no del combo
function ComboSiNo($control, $titulo=false){

    // crea el control
    echo "<select name='$control' size='1'
           required ";

    // si recibió el titulo
    if ($titulo){

        // lo agrega
        echo "title='$titulo'>";

    // si no lo recibió
    } else {

        // agrega uno genérico
        echo "title='Seleccione el valor de la lista'>";

    }

    // agregamos el primer elemento en blanco
    echo "<option></option>";
    echo "<option>Si</option>";
    echo "<option>No</option>";

    // retornamos
    return;

}

// Nombre: ControlCombo
// Parámetros: el nombre de un control
//             el valor del texto de ayuda
// Retorna: Nada
// Propósito: esta función crea un select con el nombre del control recibido
//            y agrega los elementos del combo que permiten seleccionar el
//            nivel de acceso del usuario, si el valor del campo de la base
//            recibido coincide con alguno de los elementos lo selecciona
//            por defecto
function ControlCombo($control, $titulo=false){

    // abre el control
    echo "<select name='$control' size='1'
           id='$control'
           required ";

    // si recibió el título
    if ($titulo){
        echo "title='$titulo'>";
    } else {
        echo "title='Seleccione el valor correspondiente'>";
    }

    // agrega el primer elemento en blanco
    echo "<option></option>";
    echo "<option>Consultar</option>";
    echo "<option>Editar</option>";
    echo "<option>Borrar</option>";

    // cierra el combo
    echo "</select>";

    // retornamos
    return;

}

?>
