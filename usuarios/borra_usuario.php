<?php

/**
 *
 * usuarios/borra_usuario.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la id de un usuario, instancia
 * la clase y ejecuta la consulta de eliminación, asume que la
 * posibilidad de eliminar ya fue verificada
 *
*/

// incluimos e instanciamos la clase
require_once ("usuarios.class.php");
$usuario = new Usuarios();

// seteamos la id y ejecutamos la eliminación
$usuario->setId($_GET["id"]);
$resultado = $usuario->borraUsuario();

$jsondata["id"] = $resultado;
echo json_encode($jsondata);

?>
