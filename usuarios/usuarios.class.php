<?php

/**
 *
 * Class Usuarios | usuarios/usuarios.class.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que provee los métodos de acceso a la tabla de usuarios del sistema
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Usuarios{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                  // clave del registro
    protected $Usuario;             // nombre del usuario
    protected $Nombre;              // nombre completo del usuario
    protected $Password;            // contraseña de acceso encriptada
    protected $NuevoPass;           // nuevo password del usuario
    protected $Clientes;            // autorizado para la tabla de clientes
    protected $Egresos;             // autorizado para la tabla de egresos
    protected $Ingresos;            // autorizado para la tabla de ingresos
    protected $Administrador;       // puede administrar el sistema (si/no)
    protected $Activo;              // si el usuario está activo
    protected $Link;                // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

    }

    /**
     * destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación de variables
    public function setId($id){

        // verifica que sea un número
        if (is_numeric($id)){

            $this->Id = $id;

        // si no es un número
        } else {

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        }

    }

    // asigna el nombre completo
    public function setNombre($nombre){
        $this->Nombre = $nombre;
    }

    // asigna el nombre de usuario
    public function setUsuario($usuario){

        // verifica la longitud
        if (strlen($usuario) < 4 || strlen($usuario) > 15){

            // abandona con mensaje de error
            echo "La longitud del nombre de usuario es de " . strlen($usuario);
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Usuario = $usuario;

        }

    }

    // asigna el password verificando la longitud
    public function setPassword($password){

        // verifica la longitud
        if (strlen($password) < 4 || strlen($password) > 15){

            // abandona con mensaje de error
            echo "La longitud de la contraseña es de " . strlen($password);
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Password = $password;

        }

    }

    // asigna el password verificando la longitud
    public function setNuevoPassword($nuevo_password){

        // verifica la longitud
        if (strlen($nuevo_password) < 4 || strlen($nuevo_password) > 15){

            // abandona con mensaje de error
            echo "La longitud de la nueva contraseña es de " . strlen($password);
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->NuevoPass = $nuevo_password;

        }

    }

    // asigna si puede editar clientes
    public function setClientes($clientes){

        // verifica el valor recibido
        if ($clientes != "Consultar" && $clientes != "Editar" && $clientes != "Borrar"){

            // presenta el mensaje y abandona
            echo "Mal asignado el valor de clientes, valor recibido $clientes";
            exit;

        // si recibió correctamente
        } else {

            // lo asigna
            $this->Clientes = $clientes;

        }

    }

    // asigna si puede editar ingresos
    public function setIngresos($ingresos){

        // verifica el valor recibido
        if ($ingresos != "Consultar" && $ingresos != "Editar" && $ingresos != "Borrar"){

            // presenta el mensaje y abandona
            echo "Mal asignado el valor de ingresos, valor recibido $ingresos";
            exit;

        // si recibió correctamente
        } else {

            // lo asignamos
            $this->Ingresos = $ingresos;

        }

    }

    // asigna si puede editar egresos
    public function setEgresos($egresos){

        // verifica el valor recibido
        if ($egresos != "Consultar" && $egresos != "Editar" && $egresos != "Borrar"){

            // presenta el mensaje y abandona
            echo "Mal asignado el valor de egresos, valor recibido $egresos";
            exit;

        // si recibió correctamente
        } else {

            // lo asigna
            $this->Egresos = $egresos;

        }

    }

    // asigna si es administrador
    public function setAdministrador($administrador){

        // verifica que sea si o no
        if ($administrador != "Si" && $administrador != "No"){

            // presenta el mensaje y abandona
            echo "Mal definido el valor de administrador, se recibió $aministrador";
            exit;

        // si lo recibió bien
        } else {

            // lo asigna
            $this->Administrador = $administrador;

        }

    }

    // asigna si está activo
    public function setActivo($activo){

        // verifica que sea si o no
        if ($activo != "Si" && $activo != "No"){

            // presenta el mensaje y abandona
            echo "Mal definido el valor de activo, se recibió $activo";
            exit;

        // si lo recibió bien
        } else {

            // lo asigna
            $this->Activo = $activo;

        }

    }
    // funciones de recuperación de datos
    public function getIdUsuario(){
        return $this->Id;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getClientes(){
        return $this->Clientes;
    }
    public function getIngresos(){
        return $this->Ingresos;
    }
    public function getEgresos(){
        return $this->Egresos;
    }
    public function getAdministrador(){
        return $this->Administrador;
    }
    public function getActivo(){
        return $this->Activo;
    }

    /**
      * método público que recibe como parámetro una id de un registro
      * y asigna los valores a la clase con los datos de ese usuario
      * si no recibe una id, verifica si está seteada la propiedad de
      * la clase
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * @param int $id - clave del usuario
      */
    public function obtenerUsuario($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está asignada
            if (empty($this->Id)){

                // abandona por error
                echo "No se ha asignado la clave del usuario";
                exit;

            // si está asignada la id
            } else {

                // la asigna a la variable del método
                $id = $this->Id;

            }

        }

        // declaramos las variables
        $id_usuario = 0;
        $nombre_usuario = "";
        $password_usuario = "";
        $usuarios_usuario = "";
        $clientes_usuario = "";
        $ingresos_usuario = "";
        $administrador_usuario = "";
        $activo_usuario = "";

        // ahora componemos la consulta, traemos el password
        // encriptado también porque lo vamos a usar en el
        // cambio de password
        $consulta = "SELECT usuarios.id AS id_usuario,
                            usuarios.nombre AS nombre_usuario,
                            usuarios.password AS password_usuario,
                            usuarios.usuario AS usuarios_usuario,
                            usuarios.clientes AS clientes_usuario,
                            usuarios.ingresos AS ingresos_usuario,
                            usuarios.egresos AS egresos_usuario,
                            usuarios.administrador AS administrador_usuario,
                            usuarios.activo AS activo_usuario
                     FROM usuarios
                     WHERE usuarios.id = '$id';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

            //asignamos a las variables de clase
            $this->Id = $id_usuario;
            $this->Nombre = $nombre_usuario;
            $this->Usuario = $usuarios_usuario;
            $this->Password = $password_usuario;
            $this->Clientes = $clientes_usuario;
            $this->Ingresos = $ingresos_usuario;
            $this->Egresos = $egresos_usuario;
            $this->Administrador = $administrador_usuario;
            $this->Activo = $activo_usuario;

            // retorna verdadero
            return true;

        }

    }

    /** método público que recibe como parámetro un texto y busca la
      * ocurrencia del mismo en la tabla de usuarios, retorna el
      * vector de los usuarios encontrados o falso en todo caso
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * @param string $texto - cadena a buscar
      * @return array
      */
    public function buscarUsuario($texto){

        // verifica el valor recibido
        if (empty($texto)){

            // presenta el mensaje y abandona
            echo "No se ha recibido un texto a buscar";
            exit;

        }

        // compone la consulta
        $consulta = "SELECT usuarios.id AS id_usuario,
                            usuarios.nombre AS nombre_usuario,
                            usuarios.usuario AS usuarios_usuario,
                            usuarios.clientes AS clientes_usuario,
                            usuarios.ingresos AS ingresos_usuario,
                            usuarios.egresos AS egresos_usuario,
                            usuarios.administrador AS administrador_usuario,
                            usuarios.activo AS activo_usuario
                     FROM usuarios
                     WHERE usuarios.nombre LIKE '%$texto%';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

            // retorna la matriz
            return $fila;

        }

    }

    /**
     * método público retorna la nómina de usuarios autorizados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaUsuarios(){

        // compone la consulta
        $consulta = "SELECT usuarios.id AS id_usuario,
                            usuarios.nombre AS nombre_usuario,
                            usuarios.usuario AS usuarios_usuario,
                            usuarios.clientes AS clientes_usuario,
                            usuarios.ingresos AS ingresos_usuario,
                            usuarios.egresos AS egresos_usuario,
                            usuarios.administrador AS administrador_usuario,
                            usuarios.activo AS activo_usuario
                     FROM usuarios
                     ORDER BY usuarios.nombre;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

            // retorna la matriz
            return $fila;

        }

    }

    /**
     * método público que actualiza el password de un usuario
     * asume que los valores de password nuevo y viejo, como
     * así también la id del usuario se encuentran asignados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function nuevoPassword(){

        // asume que los valores ya fueron seteados y que
        // tanto el password viejo como el nuevo no se
        // encuentran encriptados

        // primero verificamos que el password viejo
        // coincida con el de la base de datos
        $consulta = "SELECT COUNT(usuarios.id) AS registros
                     FROM usuarios
                     WHERE usuarios.id = '$this->Id' AND
                           usuarios.password = MD5('$this->Password');";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y lo pasamos a variables
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si hay registros
        if ($registros != 0){

            // componemos la consulta de actualización
            $consulta = "UPDATE usuarios SET
                                password = MD5('$this->NuevoPass')
                         WHERE id = '$this->Id';";
            $resultado = $this->Link->prepare($consulta);
            $resultado->execute();

            // seteamos el estado de la operación
            $estado = true;

        // si las contraseñas no coinciden
        } else {

            // seteamos el estado de la operación
            $estado = false;

        }

        // retorna el estado
        return $estado;
    }

    /**
     *  método público que graba el registro del usuario, produce
      * la consulta de inserción o actualización según el caso y
      * retorna el estado de la operación
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * @return int
      */
    public function grabaUsuario(){

        // asumimos que las variables ya se encuentran
        // inicializadas y ya verificado el nombre de usuario

        // si es una inserción
        if (empty($this->Id)){
            $this->nuevoUsuario();
        } else {
            $this->editaUsuario();
        }

        // retorna la id del registro insertado / editado
        return $this->Id;

    }

    /**
     * Método protegido que inserta un nuevo registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoUsuario(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO usuarios
                            (usuario,
                             nombre,
                             password,
                             clientes,
                             ingresos,
                             egresos,
                             administrador,
                             activo)
                            VALUES
                            ('$this->Usuario',
                             '$this->Nombre',
                             MD5('$this->Password'),
                             '$this->Clientes',
                             '$this->Ingresos',
                             '$this->Egresos',
                             '$this->Administrador',
                             '$this->Activo');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // obtiene la id del registro insertado
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método progetido que edita el registro del usuario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaUsuario(){

        // llamamos el evento de auditoría
        $this->auditoriaUsuarios("Edición");

        // componemos la consulta con los nuevos valores
        $consulta = "UPDATE usuarios SET
                            usuarios.nombre = '$this->Nombre',
                            usuarios.clientes = '$this->Clientes',
                            usuarios.ingresos = '$this->Ingresos',
                            usuarios.egresos = '$this->Egresos',
                            usuarios.administrador = '$this->Administrador',
                            usuarios.activo = '$this->Activo'
                        WHERE usuarios.id = '$this->Id';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

    /**
     * procedimiento que recibe la id de un registro y elimina el
     * usuario de la base de datos, retorna el resultado de la
     * operación porque puede no eliminarlo para no perder integridad
     * referencial
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del usuario
     */
    public function borraUsuario($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definida por la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No se ha recibido la clave del usuario a eliminar";
                exit;

            // si está definida
            } else {

                // la asigna
                $id = $this->Id;

            }

        }

        // llamamos la auditoría
        $this->auditoriaUsuarios("Eliminación");

        // ahora intentamos eliminar el registro
        $consulta = "DELETE FROM usuarios
                     WHERE id = '$id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el estado de la operación
        return $estado;

    }

    /**
     * procedimiento usado al ingreso, verifica el nombre de usuario
     * y la contraseña y setea las variables de sesión del sistema
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function Validar(){

        // obtiene el password de la base usamos una rutina independiente
        // para realizar la verificación porque si llamamos a obtenerUsuario
        // va a pisar las variables de la clase

        // componemos y ejecutamos la consulta
        $consulta = "SELECT usuarios.id AS id,
                            usuarios.usuario AS usuario,
                            usuarios.password AS password,
                            usuarios.clientes AS clientes,
                            usuarios.ingresos AS ingresos,
                            usuarios.egresos AS egresos,
                            usuarios.administrador AS administrador,
                            usuarios.activo AS activo
                     FROM usuarios
                     WHERE usuarios.usuario = '$this->Usuario' AND
                           usuarios.password = MD5('$this->Password') AND
                           usuarios.activo = 'Si';";
        $resultado = $this->Link->query($consulta);

        // si encontró registros
        if ($resultado->rowCount() != 0){

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

            // grabamos las variables de sesión
            setcookie("ID", $id, 0, "/");
            setcookie("Usuario",$usuario, 0, "/");
            setcookie("Clientes",$clientes, 0, "/");
            setcookie("Ingresos",$ingresos, 0, "/");
            setcookie("Egresos",$egresos, 0, "/");
            setcookie("Administrador",$administrador, 0, "/");

            // asignamos en las variables de clase
            $this->setId($id);
            $this->setUsuario($usuario);
            $this->setClientes($clientes);
            $this->setIngresos($ingresos);
            $this->setEgresos($egresos);
            $this->setAdministrador($administrador);

            // retornamos correcto
            $correcto = true;

        // si no encontró el usuario
        } else {

            $correcto = false;

        }

        // retorna el resultado o el estado de la operación
        return $correcto;

    }

    /**
     * procedimiento utilizado en las altas, verifica que el nombre
     * de usuario no se encuentre repetido
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function VerificaUsuario(){

        // primero verifica que el nombre de usuario no se encuentre
        // repetido en la base de usuarios
        $consulta = "SELECT usuarios.id AS id
                     FROM usuarios
                     WHERE usuarios.usuario = '$this->Usuario'; ";
        $resultado = $this->Link->query($consulta);

        // si lo encontró
        if ($resultado->rowCount() != 0){

            // asigna el valor
            $estado = true;

        // si no lo encontró
        } else {

            // asigna el estado
            $estado = false;

        }

        // retorna el resultado de la operación
        return $estado;

    }

    /**
     * función que verifica si se puede eliminar el usuario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del usuario
     * @return boolean
     */
    public function puedeBorrar($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definida por la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No he recibido la clave del usuario a verificar";
                exit;

            // si está definido
            } else {

                // lo asigna
                $id = $this->Id;

            }

        }

        // asigna la variable control
        $estado = true;

        // verifica si ingresó clientes
        $consulta = "SELECT COUNT(clientes.id) AS unidades
                     FROM clientes
                     WHERE clientes.usuario = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si tiene registros
        if ($unidades != 0){
            $estado = false;
        }

        // verifica si ingresó inventario
        $consulta = "SELECT COUNT(inventario.id) AS unidades
                     FROM inventario
                     WHERE inventario.usuario = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si tiene registros
        if ($unidades != 0){
            $estado = false;
        }

        // verifica si ingresó modelos
        $consulta = "SELECT COUNT(modelos.id) AS unidades
                     FROM modelos
                     WHERE modelos.usuario='$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si tiene registros
        if ($unidades != 0){
            $estado = false;
        }

        // retorna el estado
        return $estado;

    }

    // método protegido que recibe como parámetro la acción y luego
    // ejecuta la consulta de inserción en la tabla de auditoría de
    // usuarios
    protected function auditoriaUsuarios($evento){

        // primero obtenemos los datos del registro actual
        $consulta = "SELECT usuarios.id AS id_usuario,
                            usuarios.nombre AS nombre_usuario,
                            usuarios.usuario AS usuario_usuario,
                            usuarios.password AS password_usuario,
                            usuarios.clientes AS clientes_usuario,
                            usuarios.ingresos AS ingresos_usuario,
                            usuarios.egresos AS egresos_usuario,
                            usuarios.administrador AS administrador_usuario,
                            usuarios.activo AS activo_usuario
                     FROM usuarios
                     WHERE usuarios.id = '$this->Id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // ahora componemos la consulta de inserción en la
        // tabla de auditoría
        $consulta = "INSERT INTO auditoria_usuarios
                            (id,
                             nombre,
                             usuario,
                             password,
                             clientes,
                             ingresos,
                             egresos,
                             administrador,
                             activo,
                             evento)
                            VALUES
                            ('$id_usuario',
                             '$nombre_usuario',
                             '$usuario_usuario',
                             '$password_usuario',
                             '$clientes_usuario',
                             '$ingresos_usuario',
                             '$egresos_usuario',
                             '$administrador_usuario',
                             '$activo_usuario',
                             '$evento');";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // retornamos
        return;

    }

    /**
     * Método que recibe como parámetro el nombre de un usuario
     * y verifica que no se encuentre declarado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $usuario
     * @return int
     */
    public function validaUsuario($usuario){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(usuarios.id) AS registros
                     FROM usuarios
                     WHERE usuarios.usuario = '$usuario';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables y retornamos
        extract($fila);
        return $registros;

    }

}
?>
