<?php

/**
 *
 * clientes/backup.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que genera el backup de la base
 *
*/

// inclusión de archivos
require_once ("../clases/backup.class.php");

// obtenemos el nivel de acceso
$esadmin = $_COOKIE["Administrador"];

// si es administrador
if ($esadmin == "Si"){

    // instanciamos la clase y generamos el backup
    $copia = new Backup();
    $sql = $copia->Dump();

    // presentamos un mensaje y ofrecemos un enlace de descarga
    echo "<table width='50%' align='center' border='0'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th><br>Copia de Seguridad Generada<br></th>";
    echo "</tr>";
    echo "</thead>";
    echo "</body>";
    echo "<tr>";
    echo "<td>";
    echo "<br>";
    echo "<p>Pulse sobre el enlace para descargar el archivo</p>";
    echo "<a href='usuarios/$sql'
             title='Descarga el Backup'>Descargar</a>";
    echo "</td>";
    echo "</tr>";
    echo "</tbody>";
    echo "</table>";

// si no tiene permisos
} else {

    // presenta el mensaje, usamos tablas para respetar el mismo formato
    echo "<table width='50%' align='center' border='0'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th><br>Usted no tiene permisos para generar un Backup<br></th>";
    echo "</tr>";
    echo "<thead>";
    echo "</table>";

}
?>
