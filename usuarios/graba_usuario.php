<?php

/**
 *
 * usuarios/graba_usuario.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del usuario y
 * luego graba el registro en la base, retorna la id del
 * registro afectado
 *
*/

// incluimos e instanciamos la clase
require_once ("usuarios.class.php");
$usuario = new Usuarios();

// asignamos las propiedades

// si recibió la id
if (!empty($_POST["id"])){
    $usuario->setId($_POST["id"]);
}

// si está dando un alta recibe también el nombre de usuario
// y la contraseña
if (!empty($_POST["usuario"])){
    $usuario->setUsuario($_POST["usuario"]);
}
if (!empty($_POST["password"])){
    $usuario->setPassword($_POST["password"]);
}

// asigna el resto de las propiedades
$usuario->setNombre($_POST["nombre"]);
$usuario->setClientes($_POST["clientes"]);
$usuario->setIngresos($_POST["ingresos"]);
$usuario->setEgresos($_POST["egresos"]);
$usuario->setAdministrador($_POST["administrador"]);
$usuario->setActivo($_POST["activo"]);

// actualiza la base
$resultado = $usuario->grabaUsuario();

// retorna la id del registro afectado
echo json_encode(array("Id" => $resultado));
?>
