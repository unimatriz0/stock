<?php

/**
 *
 * usuarios/nuevo_password.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe el nuevo password y el anterior y actualiza
 * el password del usuario activo en la base, en este caso retorna verdadero
 * si algo salió mal o no coinciden el usuario y el password, retorna
 * falso
 *
*/

// obtenemos el path de inclusión e insertamos la clase
require_once ("usuarios.class.php");

// instanciamos la clase
$usuario = new Usuarios();

// seteamos las propiedades recibidas por post
$usuario->setPassword($_POST["viejo_pass"]);
$usuario->setNuevoPassword($_POST["nuevo_pass"]);

// obtenemos la id del usuario
$id = $_COOKIE["ID"];

// seteamos la id del usuario
$usuario->setId($id);

// cambiamos la contraseña
$resultado = $usuario->nuevoPassword();

// retornamos al javascript el resultado de la operación
echo json_encode(array("Error" => $resultado));
?>
