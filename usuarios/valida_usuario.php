<?php

/**
 *
 * usuarios/valida_usuario.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del usuario y verifica
 * si este se encuentra autorizado
 *
*/

// obtenemos el path de inclusión e insertamos la clase
require_once ("usuarios.class.php");

// instanciamos la clase
$usuario = new Usuarios();

// setea el usuario y la contraseña
$usuario->setUsuario($_POST["usuario"]);
$usuario->setPassword($_POST["password"]);

// verifica las credenciales
$resultado = $usuario->Validar();

// si está acreditado
if ($resultado){

    // retornamos las variables de sesión
    echo json_encode(array("ID" => $usuario->getIdUsuario(),
                           "Usuario" => $usuario->getUsuario(),
                           "Clientes" => $usuario->getClientes(),
                           "Ingresos" => $usuario->getIngresos(),
                           "Egresos" => $usuario->getEgresos(),
                           "Administrador" => $usuario->getAdministrador()));

// si no acreditó
} else {

    // retorna el estado
    echo json_encode(array("Error" => $resultado));

}
?>
