/*
 * Nombre: usuarios.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 15/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock enlos usuarios
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los usuarios
 */
class Usuarios {

    /**
     * Constructor de la clase
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initUsuarios();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initUsuarios(){

        // inicializamos las variables de clase
        this.Id = 0;                   // clave del usuario
        this.Nombre = "";              // nombre del usuario
        this.Usuario = "";             // usuario para la base
        this.Password = "";            // contraseña de ingreso
        this.Clientes = "";            // autorizado para clientes
        this.Ingresos = "";            // autorizado para ingresos
        this.Egresos = "";             // autorizado para egresos
        this.Administrador = "";       // si es administrador
        this.Activo = "";              // si está activo
        this.Correcto = true;          // switch de usuario repetido

        // definimos el layer
        this.layerUsuarios = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla de usuarios autorizados
     */
    Usuarios(){

        // cargamos en el contenido la nómina de usuarios
        $('#contenido').load('usuarios/usuarios.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica si el usuario está autenticado, en
     * caso contrario presenta el formulario de identificación
     */
    VerificaUsuario(){

        // llama la rutina php
        $.get('usuarios/verificar.php',
            function(data){

                // si no está autenticado
                if (data.Error !== true){

                    // pedimos se autentique
                    usuarios.layerUsuarios = new jBox('Modal', {
                                        animation: 'flip',
                                        closeOnEsc: false,
                                        closeOnClick: false,
                                        closeOnMouseleave: false,
                                        closeButton: false,
                                        repositionOnContent: true,
                                        overlay: true,
                                        minWidth: 400,
                                        theme: 'TooltipBorder',
                                        ajax: {
                                            url: 'usuarios/form_ingreso.html',
                                            reload: 'strict'
                                        }
                                    });
                    usuarios.layerUsuarios.open();

                    // retorna
                    return false;

                // si devolvió correcto
                } else {

                    // verifica si hay unidades en stock critico
                    inventario.HayCritico();

                    // retorna
                    return false;

                }

            }, "json");

        // retorna
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que reinicia el formulario de ingreso y setea
     * el foco en el primer campo
     */
    CancelaIngreso(){

        // reinicia el formulario y retorna
        document.ingreso.reset();
        document.ingreso.usuario.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica se hallan ingreso los datos en
     * el formulario de ingreso antes de enviarlos al
     * servidor
     */
    Ingresar(){

        // declaración de variables
        var mensaje;

        // verifica se halla ingresado el nombre de usuario
        if (document.ingreso.usuario.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar su nombre de usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.ingreso.usuario.focus();
            return false;

        }

        // verifica que el nombre de usuario tenga una longitud coherente
        if (document.ingreso.usuario.value.length < 3 || document.ingreso.usuario.value.length > 16){

            // presenta el mensaje y retorna
            mensaje = "El nombre de usuario debe tener entre<br>";
            mensaje += "4 y 15 carateres de longitud.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.ingreso.usuario.focus();
            return false;

        }

        // verifica se halla ingresado la contraseña
        if (document.ingreso.password.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar su contraseña";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.ingreso.password.focus();
            return false;

        }

        // verifica la longitud de la contraseña
        if (document.ingreso.password.value.length < 3 || document.ingreso.usuario.value.length > 16){

            // presenta el mensaje y retorna
            mensaje = "La contraseña de ingreso debe tener entre<br>";
            mensaje += "4 y 15 caracteres de longitud.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.ingreso.password.focus();
            return false;

        }

        // serializa el formulario y lo envía, recibe como respuesta
        // el estado de la operación
        $.ajax({
            type: "POST",
            url: "usuarios/valida_usuario.php",
            data: $("#ingreso").serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.Error == false){

                    // presenta el mensaje de error
                    mensaje = "Ha ocurrido un error, verifique el<br>";
                    mensaje += "nombre de usuario y la contraseña<br>";
                    mensaje += "que ha ingresado.";
                    new jBox('Notice', {content: mensaje, color: 'red'});
                    document.ingreso.usuario.focus();

                // si resultó correcto
                } else {

                    // asigna en el sessionstorage
                    sessionStorage.setItem("Id", data.ID);
                    sessionStorage.setItem("Usuario", data.Usuario);
                    sessionStorage.setItem("Clientes", data.Clientes);
                    sessionStorage.setItem("Ingresos", data.Ingresos);
                    sessionStorage.setItem("Egresos", data.Egresos);
                    sessionStorage.setItem("Administrador", data.Administrador);

                    // cierra el canvas
                    usuarios.layerUsuarios.destroy();

                    // verifica si hay stock critico
                    inventario.HayCritico();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de cambio de contraseña
     */
    NuevoPassword(){

        // cargamos el formulario
        this.layerUsuarios = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            title: 'Nuevo Password',
                            draggable: 'title',
                            repositionOnContent: true,
                            overlay: false,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            width: 300,
                            theme: 'TooltipBorder',
                            ajax: {
                                url: 'usuarios/form_password.html',
                                reload: 'strict'
                            }
                    });
        this.layerUsuarios.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Verifica el formulario de cambio de contraseña y envía
     * los datos por ajax
     */
    actualizaPassword(){

        // declaración de variables
        var mensaje;

        // verifica se halla ingresado el password viejo
        if (document.nuevo_pass.viejo_pass.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar su contraseña actual ";
            mensaje += "para verificación.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.nuevo_pass.viejo_pass.focus();
            return false;

        }

        // verifica se halla ingresado el password nuevo
        if (document.nuevo_pass.nuevo_pass_usuario.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la nueva contraseña";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.nuevo_pass.nuevo_pass_usuario.focus();
            return false;

        // verifica la longitud del nuevo password
        } else if (document.nuevo_pass.nuevo_pass.value.length < 6){

            // presenta el mensaje y retorna
            mensaje = "La contraseña debe tener al menos ";
            mensaje += "6 caracteres";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.nuevo_pass.nuevo_pass.focus();
            return false;

        }

        // verifica que el nuevo y la repetición coincidan
        if (document.nuevo_pass.nuevo_pass.value !== document.nuevo_pass.repe_pass.value){

            // presenta el mensaje y retorna
            mensaje = "La nueva contraseña y su repetición ";
            mensaje += "no coinciden, verifique por favor.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.nuevo_pass.nuevo_pass.focus();
            return false;

        }

        // si llegó hasta aquí envía el formulario
        // serializa el formulario y lo envía, recibe como respuesta
        // el estado de la operación
        $.ajax({
            type: "POST",
            url: "usuarios/nuevo_password.php",
            data: $("#nuevo_pass").serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.Error === false){
                    mensaje = "Ha ocurrido un error, verifique\n";
                    mensaje += "la contraseña actual que está\n";
                    mensaje += "utilizando.";
                    new jBox('Notice', {content: mensaje, color: 'red'});
                } else {
                    mensaje = "Contraseña Actualizada";
                    new jBox('Notice', {content: mensaje, color: 'green'});
                    usuarios.layerUsuarios.destroy();
                }
            },
        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica en las altas
     * que no estemos ingresando un usuario repetido
     */
    validaUsuario(usuarioCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'usuarios/validanuevo.php?usuario='+this.Usuario,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    usuarios.Correcto = true;
                } else {
                    usuarios.Correcto = false;
                }

            }});

        // retornamos
        usuarioCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - la clave del usuario
     * Método utilizado en la edición de usuarios, verifica los
     * datos del formulario y asigna los valores en las variables
     * de clase antes de enviarlo al servidor
     */
    ActualizarUsuario(id){

        // inicializamos las variables para estar seguros
        // que no arrastramos basura
        this.initUsuarios();

        // definición de variables
        var mensaje;

        // asigna la clave
        this.Id = id;

        // verifica el nombre completo del usuario
        this.Nombre = document.getElementById("nombre_"+id).value;
        if (this.Nombre === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el Nombre completo del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).nombre_usuario.focus();
            return false;

        }

        // verifica las propiedades de los clientes
        this.Clientes = document.getElementById("editar_"+id).value;
        if (this.Clientes === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar de la lista el nivel de acceso ";
            mensaje += "del usuario en la tabla de clientes.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).clientes.focus();
            return false;

        }

        // verifica las propiedades del ingreso al stock
        this.Ingresos = document.getElementById("ingresos_"+id).value;
        if (this.Ingresos === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar de la lista el nivel de acceso ";
            mensaje += "del usuario en la tabla de ingresos de repuestos.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).ingresos.focus();
            return false;

        }

        // verifica las propiedades del egreso al stock
        this.Egresos = document.getElementById("egresos_"+id).value;
        if (this.Egresos === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar de la lista el nivel de acceso ";
            mensaje += "del usuario en la tabla de salidas de repuestos.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).egresos.focus();
            return false;

        }

        // verifica si es o no administrador
        this.Administrador = document.getElementById("administrador_"+id).value;
        if (this.Administrador === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si el usuario puede o no administrar ";
            mensaje += "el sistema";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).administrador.focus();
            return false;

        }

        // verifica si está activo
        this.Activo = document.getElementById("activo_"+id).value;
        if (this.Activo === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si el usuario está activo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).activo.focus();
            return false;

        }

        // grabamos el registro
        this.grabarRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de nuevo usuario y
     * luego lo envía al servidor
     */
    GrabaUsuario(){

        // inicializamos las variables de clase para estar
        // seguros de no arrastrar basura
        this.initUsuarios();

        // declaración de variables
        var mensaje;

        // verifica se halla ingresado el nombre
        this.Nombre = document.usuario.nombre.value;
        if ( this.Nombre === ""){

            // presenta el mensaje y abandona
            mensaje = "Debe ingresar el nombre completo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.nombre.focus();
            return false;

        }

        // verifica se halla ingresado el usuario
        this.Usuario = document.usuario.usuario.value;
        if (this.Usuario === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar un usuario para identificarse ";
            mensaje += "en la base.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.usuario.focus();
            return false;

        // verifica la longitud del usuario
        } else if (this.Usuario.length < 6){

            // presenta el mensaje y retorna
            mensaje = "El nombre de usuario no puede tener ";
            mensaje += "menos de 6 caracteres";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.usuario.focus();
            return false;

        }

        // verifica se halla ingresado la contraseña
        this.Password = document.usuario.password.value;
        if (this.Password === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar su contraseña de acceso";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.password.focus();
            return false;

        // verifica la longitud de la contraseña
        } else if (this.Password.length < 6){

            // presenta el mensaje y retorna
            mensaje = "La contraseña debe tener al menos ";
            mensaje += "6 caracteres";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.password.focus();
            return false;

        }

        // verifica que la contraseña y su repetición coincidan
        if (this.Password !== document.usuario.repeticion.value){

            // presenta el mensaje y retorna
            mensaje = "La contraseña y su repetición no coinciden, ";
            mensaje += "por favor verifique.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.password.focus();
            return false;

        }

        // verifica si puede editar clientes
        this.Clientes = document.usuario.clientes.value;
        if (this.Clientes === ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el nivel de acceso del usuario ";
            mensaje += "a la base de clientes.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.clientes.focus();
            return false;

        }

        // verifica si puede editar ingresos
        this.Ingresos = document.usuario.ingresos.value;
        if (this.Ingresos === ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el nivel de acceso del usuario ";
            mensaje += "a la base de ingresos de mercaderia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.ingresos.focus();
            return false;

        }

        // verifica si puede editar egresos
        this.Egresos = document.usuario.egresos.value;
        if (this.Egresos === ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el nivel de acceso del usuario ";
            mensaje += "a la base de salida de mercadería.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.egresos.focus();
            return false;

        }

        // verifica si puede administrar
        this.Administrador = document.usuario.administrador.value;
        if (this.Administrador === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si el usuario podrá administrar ";
            mensaje += "el sistema";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.administrador.focus();
            return false;

        }

        // verifica si está activo
        this.Activo = document.usuario.activo.value;
        if (this.Activo === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si el usuario estará activo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.usuario.activo.focus();
            return false;

        }

        // verificamos por callback que no este repetido
        this.validaUsuario(function(usuarioCorrecto){

            // si está repetido
            if (!usuarios.Correcto){

                // presenta el mensaje
                mensaje = "Ese nombre de usuario está en uso !!!";
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

        // grabamos el registro
        this.grabarRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que anvía los datos del formulario al servidor
     */
    grabarRegistro(){

        // si está repetido
        if (!this.Correcto){
            return false;
        }

        // crea el objeto formdata y asigna las
        // propiedades
        var formulario = new FormData();

        // si está editando
        if (this.Id != 0){
            formulario.append("id", this.Id);
        }
        formulario.append("nombre", this.Nombre);
        formulario.append("usuario", this.Usuario);
        formulario.append("password", this.Password);
        formulario.append("clientes", this.Clientes);
        formulario.append("ingresos", this.Ingresos);
        formulario.append("egresos", this.Egresos);
        formulario.append("administrador", this.Administrador);
        formulario.append("activo", this.Activo);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "usuarios/graba_usuario.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // actualizamos la grilla de usuarios
                    usuarios.Usuarios();

                    // si está el formulario activo lo destruye
                    if (typeof(usuarios.layerUsuarios != "undefined")){
                        usuarios.layerUsuarios.destroy();
                    }

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro Actualizado", color: 'green'});

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de un nuevo usuario
     */
    NuevoUsuario(){

        // cargamos el formulario
        this.layerUsuarios = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        title: 'Nuevo Usuario',
                        draggable: 'title',
                        repositionOnContent: true,
                        overlay: false,
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        theme: 'TooltipBorder',
                        ajax: {
                            url: 'usuarios/form_usuario.php',
                            reload: 'strict'
                        }
                });
        this.layerUsuarios.open();

    }

    /**
     * @author Claudio Invernizz <cinvernizzi@gmail.com>
     * Método que cierra la sesión
     */
    Salir(){

        // llama la rutina php de elimación de la sesión, llamamos
        // así para que ejecute en forma sincrónica
        $.get('usuarios/salir.php',
            function(data){

                // si retornó error (que siempre retorna correcto)
                if (data.Error === true){

                    // llama la rutina de verificación de usuario
                    usuarios.VerificaUsuario();

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del usuario a eliminar
     * Método que pide confirmación antes de eliminar el usuario
     */
    BorraUsuario(id){

     // definimos el cuadro de diálogo de confirmación
     var Confirmacion = new jBox('Confirm', {
                    animation: 'flip',
                    title: 'Eliminar Usuario',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    overlay: false,
                    content: 'Está seguro que desea eliminar el registro?',
                    theme: 'TooltipBorder',
                    confirm: function() {

                        // llama la rutina php
                        $.get('usuarios/borra_usuario.php?id='+id,
                            function(data){

                                // si retornó correcto
                                if (data.Error != 0){

                                    // recargamos la grilla para reflejar los cambios
                                    usuarios.Usuarios();

                                    // presenta el mensaje
                                    new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                // si no pudo eliminar
                                } else {

                                    // presenta el mensaje
                                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                }

                            }, "json");

                    },
                    cancel: function(){
                        Confirmacion.destroy();
                    },
                    confirmButton: 'Aceptar',
                    cancelButton: 'Cancelar'

                });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la generación del backup
     */
    Backup(){

        // llamamos la rutina
        $('#contenido').load('usuarios/backup.php');

    }

}