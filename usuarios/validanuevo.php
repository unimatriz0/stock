<?php

/**
 *
 * usuarios/validanuevo.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (24/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro un nombre de usuario
 * y verifica que este no se encuentre repetido
 *
*/

// incluimos e instanciamos la clase
require_once("usuarios.class.php");
$usuario = new Usuarios();

// verificamos si existe
$registros = $usuario->validaUsuario($_GET["usuario"]);

// retorna el estado
echo json_encode(array("Registros" => $registros));

?>
