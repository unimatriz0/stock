/*
 * Nombre: ingresos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock enlos usuarios
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los ingresos al
 * inventario
 */
class Ingresos {

    /**
     * Constructor de la clase
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initIngresos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initIngresos(){

        // inicializamos las variables
        this.IdIngreso = 0;               // clave del registro
        this.IdItem = 0;                  // clave del item
        this.Descripcion = "";            // nombre del item
        this.Cantidad = 0;                // cantidad ingresada
        this.Factura = "";                // número de factura
        this.Importe = 0;                 // importe abonado
        this.Fecha = "";                  // fecha de ingreso
        this.layerIngreso = "";           // layer del formulario emergente

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla con los
     * ingresos al inventario
     */
    Ingresos(){

        // cargamos en el contenido la nómina de clientes
        $('#contenido').load('ingresos/ingresos.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro a editar
     * Método que verifica el formulario de ingresos y asigna los
     * valores a las variables de clase, si no recibe la clave
     * asume que se trata de un alta
     */
    GrabaIngreso(id){

        // declaración de variables
        var mensaje;

        // inicializamos las variables de clase para no
        // arrastrar basura
        this.initIngresos();

        // si no recibió la id
        if (typeof(id) === "undefined"){

            // lo asigna
            id = "nueva";

        // si recibió
        } else {

            // asigna en la variable de clase
            this.IdIngreso = id;

        }

        // si está dando un alta
        if (this.IdIngreso == 0){

            // verifica se halla seleccionado una marca
            if (document.getElementById("marca_ingreso").value == 0){

                // presenta el mensaje y retorna
                mensaje = "Debe seleccionar una marca de la lista";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("marca_ingreso").focus();
                return false;

            }

            // verifica se halla seleccionado un modelo
            this.IdItem = document.getElementById("modelo_ingreso").value;
            if (this.IdItem == 0){

                // presenta el mensaje y retorna
                mensaje = "Debe seleccionar un modelo";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("modelo_ingreso").focus();
                return false;

            }

        }

        // verifica se halla ingresado la cantidad
        this.Cantidad = document.getElementById("cantidad_" + id).value;
        if (this.Cantidad == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la cantidad de unidades";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidad_" + id).focus();
            return false;

        // verifica que la cantidad sea un número
        } else if (isNaN(this.Cantidad)){

            // presenta el mensaje y retorna
            mensaje = "La cantidad ingresada a depósito debe ";
            mensaje += "ser un número";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidad_" + id).focus();
            return false;

        }

        // si no se ingresó la factura presenta el alerta
        this.Factura = document.getElementById("factura_" + id).value;
        if (this.Factura == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay datos de la facturación";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // si no se ingresó el importe presenta el alerta
        this.Importe = document.getElementById("importe_" + id).value;
        if(this.Importe == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay datos del importe abonado";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // si no es un número
        } else if (isNaN(this.Importe)){

            // presenta el mensaje y retorna
            mensaje = "El importe debe ser un número";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // enviamos al servidor
        this.actualizaRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos al servidor
     */
    actualizaRegistro(){

        // crea el objeto formulario
        var formulario = new FormData();

        // si está editando
        if (this.IdIngreso != 0){

            // agrega la id del ingreso
            formulario.append("id", this.IdIngreso);

        // si está insertando
        } else {

            // agrega la id del modelo
            formulario.append("modelo", this.IdItem);

        }

        // agrega al formulario el resto de los campos
        formulario.append("cantidad", this.Cantidad);
        formulario.append("factura", this.Factura);
        formulario.append("importe", this.Importe);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "ingresos/graba_ingreso.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si hubo un error
                if (data.Error === false){

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió todo bien
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // si fue llamado desde el layer emergente
                    if (typeof(ingresos.layerIngreso) != "undefined"){

                        // destruye el layer
                        ingresos.layerIngreso.destroy();

                    // si fue llamado desde la grilla
                    } else {

                        // recarga la grilla
                        ingresos.Ingresos();

                    }

                    // reinicia las variables de clase
                    ingresos.initIngresos();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el abm de ingresos al cambiar el select
     * de marca, actualiza el select de modelos
     */
    cambiaMarca(){

        // declaración de variables
        var marca = document.getElementById("marca_ingreso").value;

        // si no hay una marca seleccionada
        if (marca == 0){
            return false;
        }

        // cargamos el select
        modelos.listaModelos("modelo_ingreso", marca);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina php para generar el excel de ingresos
     */
    Exportar(){

        // redirigimos el navegador
        window.location = "ingresos/xls_ingresos.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del ingreso
     * Mètodo que pide confirmaciòn antes de eliminar un registro
     */
    BorraIngreso(id){

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                    animation: 'flip',
                    title: 'Eliminar Ingreso',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    overlay: false,
                    content: 'Está seguro que desea eliminar el registro?',
                    theme: 'TooltipBorder',
                    confirm: function() {

                        // llama la rutina php
                        $.get('ingresos/borra_ingreso.php?id='+id,
                            function(data){

                                // si retornó correcto
                                if (data.Error != 0){

                                    // recargamos la grilla para reflejar los cambios
                                    ingresos.Ingresos();

                                    // presenta el mensaje
                                    new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                // si no pudo eliminar
                                } else {

                                    // presenta el mensaje
                                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                }

                            }, "json");

                    },
                    cancel: function(){
                        Confirmacion.destroy();
                    },
                    confirmButton: 'Aceptar',
                    cancelButton: 'Cancelar'

                });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} modelo - clave del modelo
     * @param {string} descripcion - nombre del modelo
     * Método llamado desde la grilla de modelos que abre un layer
     * emergente y permite declarar un ingreso para el modelo
     */
    nuevoIngreso(modelo, descripcion){

        // asignamos en las variables de clase
        this.IdItem = modelo;
        this.Descripcion = descripcion;

        // cargamos el formulario
        this.layerIngreso = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            title: 'Ingreso de artículos',
                            draggable: 'title',
                            repositionOnContent: true,
                            overlay: false,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            theme: 'TooltipBorder',
                            ajax: {
                                url: 'ingresos/form_ingresos.html',
                                reload: 'strict'
                            }
                });
        this.layerIngreso.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo llamado desde el formulario de ingresos (el emergente
     * de la grilla de modelos) que verifica los datos antes de
     * enviarlo al servidor
     */
    ingresoModelos(){

        // si fue llamado desde la grilla de modelos siempre es
        // un alta y ya està asignada la id del modelo

        // declaración de variables
        var mensaje;

        // si no ingresó la cantidad
        if (document.getElementById("cantidad_ingreso").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar la cantidad ingresada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidad_ingreso").focus();
            return false;

        // si ingresò
        } else {

            // asigna en la variable de clase
            this.Cantidad = document.getElementById("cantidad_ingreso").value;

        }

        // si no ingresò la factura presenta el alerta
        if (document.getElementById("factura_ingreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay información sobre la factura";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // de cualquier forma asigna en la variable de clase
        this.Factura = document.getElementById("factura_ingreso").value;

        // si no ingresò el importe
        if (document.getElementById("importe_ingreso").value == 0){

            // presenta el mensaje y retorna
            mensaje = "No hay información del importe de la factura";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // asigna en la variable de clase
        this.Importe = document.getElementById("importe_ingreso").value;

        // grabamos el registro
        this.actualizaRegistro();

    }

}