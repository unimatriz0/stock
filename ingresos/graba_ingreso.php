<?php

/**
 *
 * ingresos/graba_ingreso.php
 *
 * @package     Stock
 * @subpackage  Ingresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos de un ingreso y
 * ejecuta la consulta de actualizaciòn en el servidor
 *
*/

// incluye e instancia la clase
require_once ("ingresos.class.php");
$ingreso = new Ingresos();

// obtenemos los valores y los asignamos
if (!empty($_POST["id"])){
    $ingreso->setId($_POST["id"]);
}
if (!empty($_POST["modelo"])){
    $ingreso->setItem($_POST["modelo"]);
}

// agrega la cantidad
$ingreso->setCantidad($_POST["cantidad"]);
$ingreso->setFactura($_POST["factura"]);
$ingreso->setImporte($_POST["importe"]);

// ejecuta la consulta
$resultado = $ingreso->grabaIngreso();

// retorna el resultado de la operación
echo json_encode(array("Error" => $resultado));
?>
