<?php

/**
 *
 * Class Ingresos | ingresos/ingresos.class.php
 *
 * @package     Stock
 * @subpackage  Ingresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla el abm del material ingresado al depòsito
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definiciòn de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Ingresos{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                    // clave del registro
    protected $Item;                  // clave del repuesto
    protected $Cantidad;              // número de unidades ingresadas
    protected $Factura;               // identificación de la factura
    protected $Importe;               // valor total de la factura
    protected $Fecha;                 // fecha actual
    protected $Usuario;               // clave del usuario
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexiòn con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // obtenemos la id del usuario
        $this->Usuario = $_COOKIE["ID"];

    }

    /**
     * Destructor de la clase, cierra la conexiòn con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación
    public function setId($id){

        // si es un número
        if (is_numeric($id)){

            // lo asigna
            $this->Id = $id;

        // si no es número
        } else {

            // abandona por error
            echo "La clave del ingreso debe ser un número";
            exit;

        }

    }
    public function setItem($item){

        // verifica que sea un número
        if (is_numeric($item)){

            // lo asigna
            $this->Item = $item;

        // si no es numérico
        } else {

            // abandona por error
            echo "La clave del ítem debe ser un número";
            exit;

        }

    }
    public function setCantidad($cantidad){

        // verifica que sea un número
        if (is_numeric($cantidad)){

            // lo asigna
            $this->Cantidad = $cantidad;

        // si no es número
        } else {

            // abandona por error
            echo "La cantidad a ingresar debe ser un número";
            exit;

        }

    }
    public function setFactura($factura){

        // directamente lo asigna
        $this->Factura = $factura;

    }
    public function setImporte($importe){

        // si es un número
        if (is_numeric($importe)){

            // lo asigna
            $this->Importe = $importe;

        // si no es número
        } else {

            // abandona por error
            echo "El importe de la factura debe ser un número";
            exit;

        }

    }

    /**
     * Método pùblico que lista la nòmina completa de ingresos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listarIngresos(){

        // directamente abre la vista
        $consulta = "SELECT vw_ingresos.id AS id_ingreso,
                            vw_ingresos.marca AS marca_ingreso,
                            vw_ingresos.modelo AS modelo_ingreso,
                            vw_ingresos.cantidad AS cantidad_ingreso,
                            vw_ingresos.factura AS factura_ingreso,
                            vw_ingresos.importe AS importe_ingreso,
                            vw_ingresos.fecha AS fecha_ingreso,
                            vw_ingresos.usuario AS usuario_ingreso
                     FROM vw_ingresos;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $fila;

    }

    /**
     * Mètodo que ejecuta la consulta de inserciòn o ediciòn,
     * retorna el estado de la operaciòn
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    // método público que graba en la base un registro
    public function grabaIngreso(){

        // si es una inserción
        if (empty($this->Id)){
            $estado = $this->nuevoIngreso();
        } else {
            $estado = $this->editaIngreso();
        }

        // retorna el resultado de la operación
        return $estado;

    }

    /**
     * Mètodo protegido que inserta un nuevo ingreso
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function nuevoIngreso(){

        // compone la consulta
        $consulta = "INSERT INTO ingresos
                            (item,
                             cantidad,
                             factura,
                             importe,
                             fecha,
                             usuario)
                            VALUES
                            ('$this->Item',
                             '$this->Cantidad',
                             '$this->Factura',
                             '$this->Importe',
                             '$this->Fecha',
                             '$this->Usuario');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // si pudo insertar
        if ($estado){

            // obtiene la id del registro insertado
            $this->Id = $this->Link->lastInsertId();

            // actualizamos el inventario
            $consulta = "UPDATE inventario SET
                                inventario.cantidad = inventario.cantidad + $this->Cantidad,
                                inventario.fecha = '$this->Fecha',
                                inventario.usuario = '$this->Usuario'
                         WHERE inventario.item = '$this->Item';";
            $resultado = $this->Link->prepare($consulta);
            $resultado->execute();

        }

        // retornamos el resultado de la operaciòn
        return $estado;

    }

    /**
     * Mètodo protegido que edita el registro de ingresos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function editaIngreso(){

        // actualiza la auditoría
        $this->auditoriaIngresos("Edición");

        // actualizamos el inventario
        $this->actualizaInventario("edicion");

        // actualiza el registro en la edición solo permite
        // modificar la cantidad
        $consulta = "UPDATE ingresos SET
                            cantidad = '$this->Cantidad',
                            factura = '$this->Factura',
                            importe = '$this->Importe',
                            fecha = '$this->Fecha',
                            usuario = '$this->Usuario'
                        WHERE ingresos.id = '$this->Id';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

    }

    /**
     * Método que actualiza el inventario antes de editar
     * o borrar el registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento - el tipo de evento
     */
    protected function actualizaInventario($evento){

        // obtenemos la cantidad ingresada en el registro
        $consulta = "SELECT ingresos.cantidad AS cantidadvieja
                     FROM ingresos
                     WHERE ingresos.id = '$this->Id';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el valor
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si está eliminando
        if ($evento == "eliminacion"){

            // compone la consulta
            $consulta = "UPDATE inventario SET
                                cantidad = cantidad - $cantidadvieja
                         WHERE inventario.item = '$this->Item';";

        // si está editando
        } else {

            // compone la consulta
            $consulta = "UPDATE inventario SET
                                cantidad = cantidad - $cantidadvieja + $this->Cantidad
                         WHERE inventario.item = '$this->Item';";

        }

        // ejecutamos la consulta
        $this->Link->exec($consulta);

    }

    /**
     * Mètodo que elimina el registro de la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     * @return boolean
     */
    // método público que elimina un registro en la base
    public function borraIngreso($id){

        // asigna en la variable de clase
        $this->Id = $id;

        // actualizamos el inventario
        $this->actualizaInventario("eliminacion");

        // ejecuta la consulta de auditoría
        $this->auditoriaIngresos("Eliminación");

        // ejecuta la consulta de eliminación
        $consulta = "DELETE FROM ingresos
                     WHERE id = '$this->Id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el estado de la operación
        return $estado;

    }

    /**
     * Mètodo que actualiza la tabla de auditorìa, recibe como
     * paràmetro el evento
     * @param string $evento - eliminaciòn o ediciòn
     */
    // método protegido que recibe como parámetro el evento
    // y ejecuta la consulta de inserción en la tabla de auditoría
    protected function auditoriaIngresos($evento){

        // obtiene los datos del registro actual
        $consulta = "SELECT ingresos.id AS id_ingreso,
                            ingresos.item AS item_ingreso,
                            ingresos.cantidad AS cantidad_ingreso,
                            ingresos.factura AS factura_ingreso,
                            ingresos.importe AS importe_ingreso,
                            ingresos.fecha AS fecha_ingreso,
                            ingresos.usuario AS usuario_ingreso
                     FROM ingresos
                     WHERE ingresos.id = '$this->Id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($fila);

        // inserta en la tabla de auditoría
        $consulta = "INSERT INTO auditoria_ingresos
                            (id,
                             item,
                             cantidad,
                             factura,
                             importe,
                             fecha,
                             usuario,
                             evento)
                            VALUES
                            ('$id_ingreso',
                             '$item_ingreso',
                             '$cantidad_ingreso',
                             '$factura_ingreso',
                             '$importe_ingreso',
                             '$fecha_ingreso',
                             '$usuario_ingreso',
                             '$evento');";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // si está editando
        if ($evento == "Edición"){

            // actualiza el inventario
            $consulta = "UPDATE inventario SET
                                inventario.cantidad = inventario.cantidad - $cantidad_ingreso + $this->Cantidad,
                                inventario.fecha = '$this->Fecha',
                                inventario.usuario = '$this->Usuario'
                         WHERE inventario.item = '$item_ingreso';";

        // si está eliminando
        } else {

            // descuenta del inventario la cantidad eliminada
            $consulta = "UPDATE inventario SET
                                cantidad = cantidad - $cantidad_ingreso
                         WHERE item = '$item_ingreso';";

        }

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

}
?>
