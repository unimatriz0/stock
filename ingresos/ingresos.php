<?php

/**
 *
 * ingresos/ingresos.php
 *
 * @package     Stock
 * @subpackage  Ingresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la nómina de ingresos de materiales
 *
*/

// obtenemos el usuario y el nivel de acceso de la sesión
$usuario = $_COOKIE["Usuario"];
$nivelingresos = $_COOKIE["Ingresos"];

// incluimos e instanciamos la clase
require_once ("ingresos.class.php");
$ingresos = new Ingresos();

// si puede dar altas
if ($nivelingresos != "Consultar"){

    // definimos la tabla
    echo "<table style='width:98%; margin-left: auto; margin-right: auto' id='ingresos'>";

    // definimos el encabezado, lo hacemos en una tabla aparte
    // para evitar que la tabla ordene la fila del nuevo registro
    echo "<tr>";
    echo "<td colspan='9'>";
    echo "<b>Nuevo Ingreso</b>";
    echo "</td>";
    echo "</tr>";

    // presentamos los encabezados porque sino queda desarmado
    echo "<tr>";
    echo "<td><b>Marca</b></td>";
    echo "<td><b>Modelo</b></td>";
    echo "<td><b>Cantidad</b></td>";
    echo "<td><b>Factura</b></td>";
    echo "<td><b>Importe</b></td>";
    echo "<td><b>Usuario</b></td>";
    echo "<td><b>Fecha</b></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "</tr>";

    // definimos el formulario
    echo "<form name='form_ingreso'>";

    // abrimos la fila
    echo "<tr>";

    // presentamos el combo de la marca (llama al mismo evento que
    // en los repuestos para recargar el combo de modelos)
    echo "<td>";
    echo "<select name='marca_ingreso'
           size='1'
           onChange='ingresos.cambiaMarca()'
           id='marca_ingreso'
           title='Seleccione la marca de la lista'>";

    // los elementos los cargamos por javascript

    // cerramos los tags
    echo "</select>";
    echo "</td>";

    // presentamos el combo del modelo por ahora en blanco
    // hasta que seleccione la marca y luego el evento onchange
    // para recargar el combo de repuestos
    echo "<td>";
    echo "<select name='modelo_ingreso'
                  id='modelo_ingreso'
                  size='1'
                  title='Seleccione el artículo'>";

    // los elementos los cargamos por javascript
    echo "<option value='0'>Seleccione</option>";

    // cerramos los tags
    echo "</select>";
    echo "</td>";

    // pide la cantidad a ingresar
    echo "<td>";
    echo "<input type='number'
           name='cantidad'
           size='5'
           step='0.01'
           class='numerogrande'
           id='cantidad_nueva'
           title='Indique la cantidad de unidades ingresadas'>";
    echo "</td>";

    // pide el número de factura
    echo "<td>";
    echo "<input type='text'
                 name='factura_nueva'
                 id='factura_nueva'
                 size='12'
                 title='Número de factura abonada'>";
    echo "</td>";

    // pide el importe de la factura
    echo "<td>";
    echo "<input type='number'
                 name='importe_nueva'
                 id='importe_nueva'
                 class='numerogrande'
                 step='0.01'
                 title='Importe total abonado'>";
    echo "</td>";

    // presenta el usuario (solo lectura)
    echo "<td>$usuario</td>";

    // obtenemos la fecha actual
    $fecha = date('d/m/Y');

    // presenta la fecha (solo lectura)
    echo "<td>$fecha</td>";

    // presenta el botón agregar
    echo "<td>";
    echo "<input type='button'
           name='BtnEditar'
           id='BtnEditar'
           title='Ingresar un nuevo repuesto'
           onClick='ingresos.GrabaIngreso()'
           class='botonagregar'>";
    echo "</td>";

    // la columna del botón borrar y cierra la fila
    echo "<td></td></tr>";

    // cerramos el formulario
    echo "</form>";

    // cierra la tabla
    echo "</table>";

}

// definimos la tabla
echo "<table style='width:98%; margin-left: auto; margin-right: auto' id='ingresos'>";

// los encabezados de la tabla
echo "<thead>";
echo "<tr>";
echo "<th>Marca</th>";
echo "<th>Modelo</th>";
echo "<th>Cantidad</th>";
echo "<th>Factura</td>";
echo "<th>Importe</td>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// el cuerpo de la tabla
echo "<tbody>";

// obtiene el listado de ingresos
$lista_ingreso = $ingresos->listarIngresos();

// recorremos el array
foreach ($lista_ingreso AS $registro){

    // obtenemos el registro
    extract($registro);

    // definimos el formulario
    echo "<form name='ingreso'>";

    // abrimos la fila
    echo "<tr>";

    // presentamos la marca
    echo "<td>$marca_ingreso</td>";

    // presentamos el modelo
    echo "<td>$modelo_ingreso</td>";

    // permite editar la cantidad
    echo "<td>";
    echo "<input type='number'
           id='cantidad_$id_ingreso'
           size='5'
           class='numerogrande'
           value='$cantidad_ingreso'
           title='Número de unidades ingresadas'>";
    echo "</td>";

    // pide el número de factura
    echo "<td>";
    echo "<input type='text'
                 name='factura_$id_ingreso'
                 id='factura_$id_ingreso'
                 size='12'
                 value='$factura_ingreso'
                 title='Número de factura abonada'>";
    echo "</td>";

    // pide el importe de la factura
    echo "<td>";
    echo "<input type='text'
                 name='importe_$id_ingreso'
                 id='importe_$id_ingreso'
                 value='$importe_ingreso'
                 size='12'
                 title='Importe total abonado'>";
    echo "</td>";

    // presenta el usuario
    echo "<td>$usuario_ingreso</td>";

    // presenta la fecha
    echo "<td>$fecha_ingreso</td>";

    // si el nivel de acceso le permite editar
    if ($nivelingresos != "Consultar"){

        // presenta el botón editar
        echo "<td>";
        echo "<input type='button'
               name='BtnEditar'
               title='Modificar datos del Ingreso'
               onClick='ingresos.GrabaIngreso($id_ingreso)'
               class='botoneditar'>";
        echo "</td>";

        // si puede borrar
        if ($nivelingresos == "Borrar"){

            // presenta el botón borrar
            echo "<td>";
            echo "<input type='button'
                   name='BtnBorrar'
                   title='Eliminar el ingreso'
                   onClick='ingresos.BorraIngreso($id_ingreso)'
                   class='botonborrar'>";
            echo "</td>";

        // si no puede borrar
        } else {

            // presenta la columna
            echo "<td></td>";

        }

    // si solo puede consultar
    } else {

        // presenta las dos columnas en blanco
        echo "<td></td><td></td>";

    }

    // cierra la fila
    echo "</tr>";

    // cerramos el formulario
    echo "</form>";

}

// cerramos el cuerpo y la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>

<script>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Entradas de Mercadería</h1>");

    // cargamos el combo de marcas
    marcas.nominaMarcas("marca_ingreso");

    // aquí fijamos las propiedades del objeto tabla
    // definimos las propiedades de la tabla
    $('#ingresos').datatable({
        pageSize: 15,
        sort:    [true, true, true, true, true, true,     true, false, false],
        filters: [true, true, true, true, true, 'select', true, false, false],
        filterText: 'Buscar ... '
    });

</script>
