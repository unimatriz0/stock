<?php

/**
 *
 * ingresos/borra_ingreso.php
 *
 * @package     Stock
 * @subpackage  Ingresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la id e un registro y
 * ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once ("ingresos.class.php");
$ingreso = new Ingresos();

// asignamos el valor y ejecutamos
$resultado = $ingreso->borraIngreso($_GET["id"]);

// retornamos el estado de la operación
echo json_encode(array("Error" => $resultado));

?>
