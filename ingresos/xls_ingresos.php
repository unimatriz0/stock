<?php

/**
 *
 * ingresos/xls_ingresos.php
 *
 * @package     Stock
 * @subpackage  Ingresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (04/10/2018)
 * @copyright   Copyright (c) 2018, KiwiDigital
 *
 * Procedimiento que genera un xls con la nómina de ingresos al depósito
 *
*/

// incluimos e instanciamos las clases
require_once ("ingresos.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
$ingresos = new Ingresos();
$hoja = new PHPExcel();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Ingresos al Depósito")
					  ->setSubject("Modelos en inventario")
					  ->setDescription("Resumen de la las ingresos de inventario")
					  ->setKeywords("Stock")
					  ->setCategory("Reportes");

// obtenemos la nómina de modelos
$nomina = $ingresos->listarIngresos();

// si hubo registros
if (count($nomina) != 0){

    // leemos la plantilla
    $hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

    // establecemos el ancho de las columnas
    $hoja->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $hoja->getActiveSheet()->getColumnDimension('B')->setWidth(60);
    $hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('F')->setWidth(15);

    // contador de filas
    $fila = 13;

    // fijamos el estilo del título
    $estilo = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 12,
            'name'  => 'Verdana'
        ));

    // fijamos el estilo de los encabezados
    $encabezado = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 10,
            'name'  => 'Verdana'
        ));

    // centramos las columnas
    $hoja->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // presenta el título
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B3', 'Sistema de Control de Stock');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B5', 'Ingresos al Depósito');

    // centramos las celdas de los títulos
    $hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);

    // establecemos la fuente
    $hoja->getDefaultStyle()->getFont()->setName('Arial')
         ->setSize(10);

    // presenta los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Marca')
          ->setCellValue('B' . $fila, 'Modelo')
          ->setCellValue('C' . $fila, 'Cantidad')
          ->setCellValue('D' . $fila, 'Factura')
          ->setCellValue('E' . $fila, 'Importe')
          ->setCellValue('F' . $fila, 'Fecha');

    // establecemos la fuente de los encabezados
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);

    // volvemos a incrementar la fila
    $fila++;

    // iniciamos un bucle recorriendo el vector
    foreach ($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // presenta el registro
        $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila , $marca_ingreso)
            ->setCellValue('B' . $fila , $modelo_ingreso)
            ->setCellValue('C' . $fila , $cantidad_ingreso)
            ->setCellValue('D' . $fila , $factura_ingreso)
            ->setCellValue('E' . $fila , $importe_ingreso)
            ->setCellValue('F' . $fila , $fecha_ingreso);

        // fijamos el formato de las celdas numéricas
        $hoja->getActiveSheet()->getStyle('E' . $fila)->getNumberFormat()->setFormatCode("#,###.##");

        // incrementamos el contador
        $fila++;

    }

    // renombramos la hoja
    $hoja->getActiveSheet()->setTitle('Ingresos');

    // fijamos la primer hoja como activa para abrirla predeterminada
    $hoja->setActiveSheetIndex(0);

    // creamos el writer y lo dirigimos al navegador en formato 2007
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="ingresos.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
    $Writer->save('php://output');

}

?>