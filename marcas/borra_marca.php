<?php

/**
 *
 * marcas/borra_marca.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la id de una marca, y ejecuta
 * la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// asignamos el valor y ejecutamos
if (!empty($_GET["id"])){
    $resultado = $marca->borraMarca($_GET["id"]);
}

// retornamos el estado de la operación
echo json_encode(array("Error" => $resultado));

?>