<?php

/**
 *
 * marcas/valida_marca.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (24/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get el nombre de una marca y verifica
 * que no se encuentre repetido antes de dar el alta
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// verificamos si existe
$registros = $marca->validaMarca($_GET["marca"]);

// retornamos el estado de la operación
echo json_encode(array("Registros" => $registros));

?>