<?php

/**
 *
 * marcas/graba_marca.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post os datos del formulario
 * y ejecuta la consulta en la base de datos
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marcas = new Marcas();

// obtenemos los datos recibidos por post
if (!empty($_POST["marca"])){
    echo
    $marcas->setMarca($_POST["marca"]);
}
if (!empty($_POST["id"])){
    $marcas->setId($_POST["id"]);
}

// capturamos el resultado
$resultado = $marcas->grabaMarca();

// retornamos el resultado
echo json_encode(array("Error" => $resultado));

?>
