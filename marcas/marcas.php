<?php

/**
 *
 * marcas/marcas.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que arma la grilla con la nómina de marcas
 *
*/

// incluye e instancia la clase
require_once ("marcas.class.php");
$marcas = new Marcas();

// obtiene la nómina de marcas
$lista = $marcas->listaMarcas();

// inicia sesión y obtiene el nivel de acceso
$esadmin = $_COOKIE["Administrador"];

// define la tabla (los tags margin son para centrar la tabla por css)
echo "<table style='width:50%; margin-left: auto; margin-right: auto'>";

// definimos el encabezado
echo "<tr>";
echo "<th>Clave</th>";
echo "<th>Marca</th>";
echo "<th></th>";           // para el botón editar
echo "<th></th>";           // para el botón eliminar

// definimos el cuerpo de la tabla
echo "<tbody>";

// recorremos el array
foreach ($lista AS $elemento){

    // obtenemos el registro
    extract($elemento);

    // define el formulario
    echo "<form name='form_marcas'>";

    // abrimos la fila
    echo "<tr>";

    // presenta la id (solo lectura)
    echo "<td>$id_marca</td>";

    // presenta la marca
    echo "<td>";
    echo "<input type='text'
           name='marca'
           id='marca_$id_marca'
           size='20'
           value='$nombre_marca'
           title='Nombre de la marca'>";
    echo "</td>";

    // si es admin
    if ($esadmin == "Si"){

        // presenta el botón editar
        echo "<td>";
        echo "<input type='button'
               name='BtnEditar'
               title='Modificar la marca'
               class='botoneditar'
               onClick='marcas.grabaMarca($id_marca)'>";
        echo "</td>";

        // si puede borrar, presenta el botón eliminar
        echo "<td>";
        if($marcas->puedeBorrar($id_marca)){
            echo "<input type='button'
                   name='BtnBorrar'
                   title='Eliminar la marca'
                   class='botonborrar'
                   onClick='marcas.BorraMarca($id_marca)'>";
        }
        echo "</td>";

    // si no es admin
    } else {

        // presenta las dos columnas en blanco
        echo "<td></td><td></td>";

    }

    // cerramos la fila
    echo "</tr>";

    // cierra el formulario
    echo "</form>";

}

// si es admín permite insertar
if ($esadmin == "Si"){

    // abre el formulario
    echo "<form name='form_marcas'>";

    // abre una nueva fila
    echo "<tr>";

    // presenta el campo texto para agregar una nueva marca
    echo "<td></td>";           // la fila de la clave en blanco
    echo "<td>";
    echo "<input type='text'
           name='marca'
           id='marca_nueva'
           size='20'
           title='Nombre de la marca'>";
    echo "</td>";

    // presenta el botón agregar
    echo "<td>";
    echo "<input type='button'
           name='BtnEditar'
           title='Agregar una nueva marca'
           class='botonagregar'
           onClick='marcas.grabaMarca()'>";
    echo "</td>";
    echo "<td></td>";

    // cierra la fila
    echo "</tr>";

    // cierra el formulario
    echo "</form>";

}

// cerramos la tabla y el cuerpo
echo "</tbody></table>";

?>
<script>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Marcas Utilizadas</h1>");

</script>
