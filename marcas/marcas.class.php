<?php

/**
 *
 * Class Marcas | marcas/marcas.class.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de marcas
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Marcas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                       // clave del registro
    protected $Marca;                    // nombre de la marca
    protected $Link;                     // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setId($id){

        // verifica que sea un número
        if (is_numeric($id)){

            // lo asigna
            $this->Id = $id;

        // si no es un número
        } else {

            // abandona por error
            echo "La clave de la marca debe ser un número";
            exit;

        }

    }
    public function setMarca($marca){
        $this->Marca = $marca;
    }

    /**
     * Método que ejecuta la consulta de edición o insercion en la base
     * retorna el estado de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function grabaMarca(){

        // si está insertando
        if (empty($this->Id)){
            $estado = $this->nuevaMarca();
        } else {
            $estado = $this->editaMarca();
        }

        // retorna el resultado de la operación
        return $estado;

    }

    /**
     * Método que inserta un nuevo registro en la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function nuevaMarca(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO marcas
                            (marca)
                            VALUES
                            ('$this->Marca');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el resultado de la operación
        return $estado;

    }

    /**
     * Método que edita el registro en la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function editaMarca(){

        // compone la consulta de edición
        $consulta = "UPDATE marcas SET
                            marca = '$this->Marca'
                     WHERE id = '$this->Id';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el resultado de la operación
        return $estado;

    }

    /**
     * Método que retorna la nómina de marcas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaMarcas(){

        // compone y ejecuta la consulta
        $consulta = "SELECT marcas.id AS id_marca,
                            marcas.marca AS nombre_marca
                     FROM marcas;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $fila;

    }

    /**
     * Método que recibe como parámetro la id de una marca y ejecuta
     * la consulta de eliminación
     * @param int $id - clave del registro
     * @return boolean
     */
    public function borraMarca($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definido en la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No he recibido la clave de la marca a borrar";
                exit;

            // si está definido
            } else {

                // lo asigna
                $id = $this->Id;

            }

        }

        // compone y ejecuta la consulta de eliminación
        $consulta = "DELETE FROM marcas
                     WHERE id = '$id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el estado de la operación
        return $estado;

    }

    /**
     * Método que determina si se puede eliminar un registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     * @return boolean
     */
    // función que determina si se puede eliminar una marca
    public function puedeBorrar($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definido por la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No he recibido la clave de la marca a verificar";
                exit;

            // si está definido
            } else {

                // lo asigna
                $id = $this->Id;

            }

        }

        // verifica si hay un modelo definido
        $consulta = "SELECT COUNT(modelos.id) AS cantidad
                     FROM modelos
                     WHERE modelos.marca = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si hubo registros
        if ($cantidad != 0){
            return false;
        } else {
            return true;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $marca - nombre de la marca a verificar
     * @return int
     * Mètodo utilizado para evitar la inserción de registros
     * duplicados, recibe como parámetro el nombre de una
     * marca y retorna el número de registros encontrados
     */
    public function validaMarca($marca){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(marcas.id) AS registros
                     FROM marcas
                     WHERE marcas.marca = '$marca';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables y retornamos
        extract($fila);
        return $registros;

    }

}

?>
