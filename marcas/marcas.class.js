/*
 * Nombre: marcas.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 15/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock en las marcas
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de las marcas
 */
class Marcas {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initMarcas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initMarcas(){

        // inicializamos las variables
        this.IdMarca = 0;                 // clave del registro
        this.Marca = "";                  // nombre de la marca
        this.Correcto = true;             // switch de marca repetida

    }

    /**
     * Método que carga en el contenido la grilla con el diccionario
     */
    Marcas(){

        // cargamos en el contenido el formulario
        $('#contenido').load('marcas/marcas.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica que no se
     * encuentre repetida la marca
     */
    validaMarca(marcaCorrecta){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'marcas/valida_marca.php?marca='+this.Marca,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    marcas.Correcto = true;
                } else {
                    marcas.Correcto = false;
                }

            }});

        // retornamos
        marcaCorrecta(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que recibe la id del registro (o nulo si está insertando)
     * verifica el formulario antes de enviarlo al servidor
     */
    grabaMarca(id){

        // declaración de variables
        var mensaje;

        // inicializamos las variables de clase
        this.initMarcas();

        // si no recibió la id es que está dando un alta
        if (typeof(id) === "undefined"){

            // asigna el valor del nuevo registro
            id = "nueva";

        // si redibió
        } else {

            // la asigna en la variable de clase
            this.IdMarca = id;

        }

        // verifica si se ingresó el nombre
        this.Marca = document.getElementById("marca_" + id).value;
        if (this.Marca === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la marca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).marca.focus();
            return false;

        }

        // si està dando altas
        if (this.IdMarca == 0){

            // verificamos por callback que no este repetido
            this.validaMarca(function(marcaCorrecta){

                // si está repetido
                if (!marcas.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa marca ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.actualizaRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase envía el
     * formulario al servidor
     */
    actualizaRegistro(){

        // si la marca està repetida
        if (!this.Correcto){
            return false;
        }

        // usamos el objeto form de html 5
        var formulario = new FormData();
        formulario.append("marca", this.Marca);

        // si está editando
        if (this.IdMarca !== 0){

            // agrega al formulario
            formulario.append("id", this.IdMarca);

        }

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "marcas/graba_marca.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error === false){

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si hubo algún error
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                    // recarga el div de datos que es menos
                    // complicado que agregar por script a la tabla
                    marcas.Marcas();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave de la marca
     * Método que recibe como parámetro la id de un registro
     * y pide confirmación antes de eliminarlo
     */
    BorraMarca(id){

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                        animation: 'flip',
                        title: 'Eliminar Marca',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        overlay: false,
                        content: 'Está seguro que desea eliminar el registro?',
                        theme: 'TooltipBorder',
                        confirm: function() {

                            // llama la rutina php
                            $.get('marcas/borra_marca.php?id='+id,
                                function(data){

                                    // si retornó correcto
                                    if (data.Error != 0){

                                        // recargamos la grilla para reflejar los cambios
                                        marcas.Marcas();

                                        // presenta el mensaje
                                        new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                    // si no pudo eliminar
                                    } else {

                                        // presenta el mensaje
                                        new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                    }

                                }, "json");

                        },
                        cancel: function(){
                            Confirmacion.destroy();
                        },
                        confirmButton: 'Aceptar',
                        cancelButton: 'Cancelar'

                    });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento del elemento html
     * @param {int} clave del elemento predeterminado
     * Método que recibe como parámetro la id de un elemento html y
     * carga la nómina de marcas en ese elemento, si además recibe
     * una id predetermina el valor
     */
    nominaMarcas(idelemento, clave){

        // limpia el combo
        $("#" + idelemento).html('');

        // llama la rutina php
        $.get('marcas/nomina_marcas.php',
            function(data){

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si coincide con el valor recibido y no indefinido
                    if (typeof(clave) != "undefined"){

                        // si coincide
                        if (clave == data[i].id){

                            // lo predetermina
                            $("#" + idelemento).append("<option selected value=" + data[i].id + ">" + data[i].marca + "</option>");

                        // si no coincide
                        } else {

                            // lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].marca + "</option>");

                        }

                    // si no recibió la clave
                    } else {

                        // lo agrega
                        $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].marca + "</option>");

                    }

                }

            }, "json");

    }

}