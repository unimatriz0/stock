<?php

/**
 *
 * marcas/nomina_marcas.php
 *
 * @package     Stock
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (23/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que retorna un array json con la nómina de marcas
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marcas = new Marcas();

// obtenemos la nómina de marcas
$nomina = $marcas->listaMarcas();

// definimos el array
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al array
    $jsondata[] = array("id" =>    $id_marca,
                        "marca" => $nombre_marca);

}

// retornamos el array
echo json_encode($jsondata);

?>
