<?php

/**
 *
 * index.php
 *
 * @package     Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que verifica la plataforma en que se está utilizando
 * la aplicación y redirige al sitio móvil o de escritorio
 *
*/

// incluimos e instanciamos las clases
require_once ("clases/Mobile_Detect.php");
$movil = new Mobile_Detect();

// si es un móvil o una tablet
if ($movil->isMobile() || $movil->isTablet() ){

    // redirigimos al sitio móvil
    header("Location: movil/index.html");

// si es una versión de escritorio
} else {

    // redirigimos
    header("Location: escritorio.html");

}
?>