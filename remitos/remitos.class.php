<?php

/**
 *
 * Class Remitos | remitos/remitos.class.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de remitos
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definición de la clase
class Remitos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                 // puntero a la base de datos
    protected $IdRemito;             // clave del registro
    protected $Remito;               // contenido del documento
    protected $Importe;              // importe total del remito
    protected $Cliente;              // clave del cliente
    protected $Usuario;              // nombre del usuario
    protected $IdUsuario;            // clave del usuario
    protected $Fecha;                // fecha de generación del remito

    // constructor de la clase
    function __construct(){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->IdRemito = 0;
        $this->Remito = "";
        $this->Importe = 0;
        $this->Cliente = 0;
        $this->Usuario = "";
        $this->Fecha = "";

        // obtenemos la id del usuario
        $this->IdUsuario = $_COOKIE["ID"];

    }

    // destructor de la clase
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdRemito($idremito){
        $this->IdRemito = $idremito;
    }
    public function setRemito($remito){
        $this->Remito = $remito;
    }
    public function setImporte($importe){
        $this->Importe = $importe;
    }
    public function setCliente($cliente){
        $this->Cliente = $cliente;
    }

    // métodos de retorno de valores
    public function getIdRemito(){
        return $this->IdRemito;
    }
    public function getRemito(){
        return $this->Remito;
    }
    public function getImporte(){
        return $this->Importe;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * Método que recibe como parámetro la clave de un remito y
     * asigna en las variables de clase los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idremito - clave del registro
     */
    public function getDatosRemito($idremito){

        // inicializamos las variables
        $importe = 0;
        $idcliente = 0;
        $fecha = "";

        // componemos la consulta
        $consulta = "SELECT remitos.id AS idremito,
                            remitos.importe AS importe,
                            remitos.cliente AS idcliente,
                            DATE_FORMAT(remitos.fecha, '%d/%m/%Y') AS fecha
                     FROM remitos
                     WHERE remitos.id = '$idremito';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro y lo asignamos
        extract($fila);
        $this->IdRemito = $idremito;
        $this->Importe = $importe;
        $this->Cliente = $idcliente;
        $this->Fecha = $fecha;

    }

    /**
     * Método público que recibe como parámetro la clave de un cliente
     * y retorna la nómina de remitos de ese cliente
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int cliente - clave del registro
     * @return array
     */
    public function nominaRemitos($cliente){

        // componemos la consulta
        $consulta = "SELECT remitos.id AS idremito,
                            remitos.importe AS importe,
                            usuarios.usuario AS usuario,
                            DATE_FORMAT(remitos.fecha, '%d/%m/%Y') AS fecha
                     FROM remitos INNER JOIN usuarios ON remitos.usuario = usuarios.id
                     WHERE remitos.cliente = '$cliente';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método público que ejecuta la consulta de actualización o
     * edición según corresponde, retorna la id del registro
     * @return int
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function grabaRemito(){

        // si està insertando
        if ($this->IdRemito == 0){
            $this->nuevoRemito();
        } else {
            $this->editaRemito();
        }

        // retornamos la id
        return $this->IdRemito;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function nuevoRemito(){

        // componemos la consulta
        $consulta = "INSERT INTO remitos
                            (remito,
                             importe,
                             cliente,
                             impreso,
                             usuario)
                            VALUES
                            ('$this->Remito',
                             '$this->Importe',
                             '$this->Cliente',
                             'No',
                             '$this->IdUsuario');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // obtiene la id del registro insertado
        $this->IdRemito = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function editaRemito(){

        // inicializamos las variables
        $importe = 0;

        // componemos la consulta
        $consulta = "UPDATE remitos SET
                            remito = '$this->Remito',
                            importe = remitos.importe + '$this->Importe',
                            cliente = '$this->Cliente',
                            impreso = 'No',
                            usuario = '$this->IdUsuario'
                     WHERE remitos.id = '$this->IdRemito';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // obtenemos el total del remito
        $consulta = "SELECT remitos.importe AS importe
                     FROM remitos
                     WHERE remitos.id = '$this->IdRemito';";
        $resultado = $this->Link->query($consulta);

        // lo asignamos en la variable de clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Importe = $importe;

    }

    /**
     * Método que retorna un array con los remitos sin imprimir
     * @author Claudio Invernizi <cinvernizzi@gmail.com>
     * @return array
     */
    public function sinImprimir(){

        // componemos la consulta
        $consulta = "SELECT remitos.id AS idremito,
                            clientes.cliente AS cliente,
                            remitos.pago AS pago,
                            remitos.importe AS total,
                            DATE_FORMAT(remitos.fecha,'%d/%m/%Y') AS fecha
                     FROM remitos INNER JOIN clientes ON remitos.cliente = clientes.id
                     WHERE remitos.impreso = 'No'
                     ORDER BY remitos.fecha;";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que recibe como parámetro la clave de un remito y lo marca
     * como impreso
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idremito
     */
    public function marcaImpreso($idremito){

        // componemos la consulta
        $consulta = "UPDATE remitos SET
                            impreso = 'Si'
                     WHERE remitos.id = '$idremito';";
        $this->Link->exec($consulta);

    }
}