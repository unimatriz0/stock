<?php

/**
 *
 * remitos/confirmaimp.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (22/10/2018)
 * @copyright   Copyright (c) 2018, KiwiDigital
 *
 * Método que recibe por get la clave de un remito y lo marca como
 * impreso
 *
*/

// incluimos e instanciamos las clases
require_once ("remitos.class.php");
$remito = new Remitos();

// lo marcamos como impreso
$remito->marcaImpreso($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>