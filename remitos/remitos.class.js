/*
 * Nombre: remitos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 01/10/2018
 * Licencia: GPL
 * Comentarios: Funciones que controlan el formulario de remitos

 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para la generación de remitos
 */
class Remitos {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initRemitos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initRemitos(){

        // inicializamos las variables
        this.IdRemito = 0;                  // clave del registro
        this.Importe = 0;                   // importe total del remito
        this.Cliente = 0;                   // clave del cliente
        this.Usuario = "";                  // nombre del usuario
        this.Fecha = "";                    // fecha de alta / modificación

        // el layer de los remitos
        this.layerRemito = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el div de clientes la nómina de
     * remitos del usuario
     */
    nominaRemitos(){

        // obtenemos la id del cliente
        var cliente = document.getElementById("id").value;

        // cargamos la grilla
        $('#ventascliente').load('remitos/lista_remitos.php?cliente='+cliente);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método que obtiene la nómina de remitos 
     */
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método que presenta en el layer emergente el remito que recibe
     * como clave
     */
    imprimeRemito(idremito){

        // generamos el certificado y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento, en el evento onclose pedimos confirmación
        // para marcar el remito como impreso
        this.layerRemito = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        onCloseComplete: function(){
                            remitos.confirmaImpresion(idremito);
                        },
                        title: 'Remito de Entrega',
                        draggable: 'title',
                        repositionOnContent: true,
                        theme: 'TooltipBorder',
                        zIndex: 15000,
                        ajax: {
                            url: 'remitos/generar_remito.php?remito='+idremito,
                            reload: 'strict'
                        }
                    });
        this.layerRemito.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método llamado al cerrar la presentación del pdf que pide
     * confirmación para marcar el remito como impreso
     */
    confirmaImpresion(idremito){

        // declaración de variables
        var mensaje;

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Impresión de Remitos',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: 'Desea marcar el remito como impreso?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // llama la rutina php
                                $.get('remitos/confirmaimp.php?id='+idremito,
                                    function(data){

                                        // si retornó correcto
                                        if (data.Error != 0){

                                            // recargamos la grilla para reflejar los cambios
                                            remitos.sinImprimir();

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Impresión registrada", color: 'green'});

                                            // eliminamos el layer del remito
                                            remitos.layerRemito.destroy();

                                        // si no pudo eliminar
                                        } else {

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                        }

                                    }, "json");

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el contenido los remitos sin imprimir
     */
    sinImprimir(){

        // cargamos la grilla
        $('#contenido').load('remitos/sinimpresion.php');

    }

}