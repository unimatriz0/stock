<?php

/**
 *
 * remitos/sinimpresion.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que presenta la grilla de los remitos sin imprimir
 *
*/

// inclusión de archivos
require_once ("remitos.class.php");
$remito = new Remitos();

// obtenemos los remitos sin imprimir
$nomina = $remito->sinImprimir();

// definimos la tabla
echo "<table width='95%' 
       align='center' 
       border='0'
       id='grilla_sin_impresion'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>N°</th>";
echo "<th align='left'>Fecha</th>";
echo "<th align='left'>Cliente</th>";
echo "<th align='left'>Pago</th>";
echo "<th align='left'>Total</th>";
echo "<th align='left'>Imp.</th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo de la tabla
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro y el enlace
    echo "<tr>";
    echo "<td>$idremito</td>";
    echo "<td>$fecha</td>";
    echo "<td>$cliente</td>";
    echo "<td>$pago</td>";
    echo "<td>" . number_format($total, 2, ',', '.') . "</td>";
    echo "<td>";
    echo "<input type='button' 
                 name='btnImprimeRemito'
                 id='btnImprimeRemito'
                 class='botonimprimir'
                 title='Imprimir el Remito'
                 onClick='remitos.imprimeRemito($idremito)'>";
    echo "</td>";
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>
<script>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Remitos sin Imprimir</h1>");

    // aquí fijamos las propiedades del objeto tabla
    $('#grilla_sin_impresion').datatable({
        pageSize: 15,
        sort:    [true,  true, true, true,     false, false],
        filters: [false, true, true, 'select', false, false],
        filterText: 'Buscar ... '
    });

</script>