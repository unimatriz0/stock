<?php

/**
 *
 * remitos/getimporte.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro la clave de un remito y
 * retorna en un array json el valor del remito, utilizado en
 * la edición de egresos
 *
*/

// incluimos e instanciamos la clase
require_once("remitos.class.php");
$remito = new Remitos();

// obtenemos los datos del remito
$remito->getDatosRemito($_GET["remito"]);
$importe = $remito->getImporte();

// retorna el valor
echo json_encode(array("Importe" => $importe));

?>
