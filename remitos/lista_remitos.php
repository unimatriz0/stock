<?php

/**
 *
 * remitos/lista_remitos.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro la clave de un cliente y
 * presenta la nómina de remitos de ese cliente
 *
*/

// inclusión de archivos
require_once ("remitos.class.php");
$remito = new Remitos();

// obtenemos la nómina de remitos
$nomina = $remito->nominaRemitos($_GET["cliente"]);

// definimos la tabla
echo "<table width='95%'
             align='center'
             border='0'
             id='tablaventas'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Fecha</th>";
echo "<th>Remito</th>";
echo "<th>Importe</th>";
echo "<th>Usuario</th>";
echo "<th>";
echo "<input type='button'
             name='btnNuevoRemito'
             id='btnNuevoRemito'
             title='Pulse para agregar un nuevo remito'
             class='botonagregar'
             onClick='egresos.muestraEgresos()'>";
echo "</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo
echo "<tbody>";

// si hay registros
if (count($nomina) > 0){

    // recorremos el vector
    foreach($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro
        echo "<td>$fecha</td>";
        echo "<td>$idremito</td>";
        echo "<td>$importe</td>";
        echo "<td>$usuario</td>";

        // presenta el botón editar
        echo "<td>";
        echo "<input type='button'
                     id='btnEditaRemito'
                     name='btnEditaRemito'
                     title='Pulse para editar el remito'
                     class='botoneditar'
                     onClick='egresos.muestraEgresos($idremito)'>";
        echo "</td>";

        // presenta el botòn imprimir
        echo "<td>";
        echo "<input type='button'
                     id='btnImprimeRemito'
                     name='btnImprimeRemito'
                     title='Pulse para imprimir el remito'
                     class='botonimprimir'
                     onClick='remitos.imprimeRemito($idremito)'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

}

// cerramos la tabla
echo "</tbody></table>";

// definimos el paginador
echo "<div class='paging'></div>";

?>

<script>

    // aquí fijamos las propiedades del objeto tabla
    $('#tablaventas').datatable({
        pageSize: 5,
        sort:    [true, true, false, false, false, false],
        filters: [true, true, false, false, false, false],
        filterText: 'Buscar ... '
    });

</script>