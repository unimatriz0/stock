/*
 * Nombre: clientes.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 15/12/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock en la tabla de clientes
 *              para la interfaz móvil
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no tiene que haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los
 * clientes en la interfaz móvil
 */
class Clientes {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos la clase
        this.initClientes();
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de la clase
     */
    initClientes(){

        // inicializamos las variables
        this.layerClientes = "";
        this.layerRemitos = "";
        this.IdCliente = "";
        this.NombreCliente = "";
        this.IdRemito = 0;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de clientes y la agrega
     * a la grilla
     */
    listaClientes(){

        // lo llamamos asincrónico
        $.ajax({
            url: "clientes/lista_clientes.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                clientes.cargaTabla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} fila - array con los datos del cliente
     * Metodo que recibe como parámetro un array con el registro
     * y lo agrega como una fila al dom de la tabla
     */
    cargaTabla(fila){

        // declaramos las variables
        var tabla = $('#tabla-clientes').DataTable();
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < fila.length; i++){

            // arma el enlace para ver el cliente
            enlace =  "<input type='button' ";
            enlace += "        name='btnGrabar' ";
            enlace += "        class='botoninformacion' ";
            enlace += "        title='Pulse para ver el registro' ";
            enlace += "        onClick='clientes.verCliente(" + fila[i].Id + ", " + String.fromCharCode(34) + fila[i].Nombre + String.fromCharCode(34) + ")';>";

            // agregamos el registro
            tabla.row.add( [
                fila[i].Nombre,
                fila[i].Telefono,
                fila[i].Mail,
                enlace
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * @param {string} nombre - nombre del cliente
     * Metodo que recibe como parámetro la clave del registro, carga en
     * el diálogo emergente los datos del mismo y lo muestra
     */
    verCliente(id, nombre){

        // almacenamos en las variables de clase
        this.IdCliente = id;
        this.NombreCliente = nombre;

        // cargamos el formulario de clientes
        this.layerClientes = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    title: 'Edición de Clientes',
                    draggable: 'title',
                    repositionOnContent: true,
                    overlay: false,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    theme: 'TooltipBorder',
                    ajax: {
                        url: 'clientes/form_clientes.html',
                        reload: 'strict'
                    }
            });

        // mostramos el layer
        this.layerClientes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cargar la página con las ventas
     * del cliente, a partir de las variables de clase
     * obtiene las compras del cliente y luego carga la
     * tabla
     */
    comprasCliente(){

        // obtenemos los remitos del cliente
        // lo llamamos asincrónico
        $.ajax({
            url: "remitos/remito_cliente.php?cliente="+clientes.IdCliente,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // llamamos la rutina de creación de la tabla
                clientes.verRemitos(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - array con los remitos
     * Método que recibe como parámetro un array de datos y
     * agrega la tabla al dom del documento con la info de
     * los remitos de un cliente
     */
    verRemitos(datos){

        // declaramos las variables
        var tabla = $('#compras-clientes').DataTable({
                      responsive: true,
                      "language":{
                        "url": "script/dataTables.spanish.lang"
                    }});
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // arma el enlace para ver el cliente
            enlace =  "<input type='button' ";
            enlace += "        name='btnCargar' ";
            enlace += "        class='botoninformacion' ";
            enlace += "        title='Pulse para ver el registro' ";
            enlace += "        onClick='clientes.examinarVentas(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                datos[i].Id,
                datos[i].Fecha,
                datos[i].Importe,
                enlace
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método que recibe como parámetro la clave de un remito
     * obtiene los datos del mismo y lo presenta en el diálogo
     * emergente con la info del mismo, si no lo recibe asume
     * que se trata de un nuevo remito
     */
    examinarVentas(idremito){

        // setea en la variable de clase el remito
        if (typeof(idremito) != "undefined"){
            this.IdRemito = idremito;
        } else {
            this.IdRemito = 0;
        }

        // cargamos la página del remito, le subimos el
        // zIndex para estar seguros que aparece por encima
        this.layerRemitos = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    title: 'Remitos de Ventas',
                    draggable: 'title',
                    repositionOnContent: true,
                    overlay: false,
                    onCloseComplete: function(){
                        ventas.finalizarVenta();
                    },
                    zIndex: 15000,
                    theme: 'TooltipBorder',
                    ajax: {
                        url: 'remitos/form_remitos.html',
                        reload: 'strict'
                    }
            });

        // mostramos el layer
        this.layerRemitos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que a partir de la variable de clase obtiene los datos
     * del remito y los agrega a la grilla
     */
    examinarRemito(){

        // obtenemos los remitos de la venta
        // lo llamamos asincrónico
        $.ajax({
            url: "ventas/nomina_ventas.php?remito="+clientes.IdRemito,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // llamamos la rutina de creación de la tabla
                clientes.mostrarVentas(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - array con los datos de la venta
     * Método que recibe como parámetro un array con los
     * datos de la venta y los agrega a la tabla
     */
    mostrarVentas(datos){

        // declaramos las variables
        var tabla = $('#remitos-ventas').DataTable({
            responsive: true,
            "language":{
              "url": "script/dataTables.spanish.lang"
          }});
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // arma el enlace para ver el cliente
            enlace =  "<input type='button' ";
            enlace += "        name='btnBorrar' ";
            enlace += "        class='botonborrar' ";
            enlace += "        title='Pulse para eliminar la venta' ";
            enlace += "        onClick='ventas.borraVenta(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                    datos[i].Marca,
                    datos[i].Modelo,
                    datos[i].Cantidad,
                    datos[i].Importe,
                    enlace
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recarga la grilla de remitos del cliente sin reinicializar
     * la tabla 
     */
    recargar(){

        // definimos la tabla y la limpiamos
        var table = $('#compras-clientes').DataTable();
        table.clear();

        // recargamos el contenido
        clientes.listarCompras();
        
    }
 
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo llamado al efectuar una venta que recarga la 
     * grilla sin redefinir la tabla
     */
    listarCompras(){

        // obtenemos los remitos del cliente
        // lo llamamos asincrónico
        $.ajax({
            url: "remitos/remito_cliente.php?cliente="+clientes.IdCliente,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // llamamos la rutina de creación de la tabla
                clientes.cargarCompras(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - array con los remitos
     * Método que recibe como parámetro un array de datos y
     * agrega la tabla al dom del documento con la info de
     * los remitos de un cliente
     */
    cargarCompras(datos){

        // declaramos las variables
        var tabla = $('#compras-clientes').DataTable();
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // arma el enlace para ver el cliente
            enlace =  "<input type='button' ";
            enlace += "        name='btnCargar' ";
            enlace += "        class='botoninformacion' ";
            enlace += "        title='Pulse para ver el registro' ";
            enlace += "        onClick='clientes.examinarVentas(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                datos[i].Id,
                datos[i].Fecha,
                datos[i].Importe,
                enlace
            ] ).draw( false );

        }

    }
    
}