<?php

/**
 *
 * clientes/lista_clientes.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que lista la nómina de clientes y la retorna 
 * como un array json
 *
*/

// incluimos e instanciamos las clases
require_once("clientes.class.php");
$clientes = new Clientes();

// obtenemos la nómina
$nomina = $clientes->listarClientes();

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Nombre"   => $nombre_cliente, 
                       "Telefono" => $telefono_cliente,
                       "Mail"     => $mail_cliente, 
                       "Id"       => $id_cliente);

}

// retornamos el vector
echo json_encode($listado);

?>