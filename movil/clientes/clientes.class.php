<?php

/**
 *
 * Class Clientes | clientes/clientes.class.php
 *
 * @package     Stock
 * @subpackage  Clientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones de la tabla de clientes
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Clientes{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                    // clave del registro
    protected $Cliente;               // nombre del cliente
    protected $Localidad;             // localidad del cliente
    protected $Domicilio;             // domicilio del cliente
    protected $Telefono;              // teléfono o móvil del cliente
    protected $Mail;                  // correo del cliente
    protected $Contacto;              // modalidad de contacto del cliente
    protected $Observaciones;         // comentarios del usuario
    protected $Usuario;               // clave del usuario que ingresó
    protected $FechaAlta;             // fecha de alta del registro
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // obtenemos la id del usuario
        $this->Usuario = $_COOKIE["Id"];

    }

    /**
     * Desctructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación
    public function setId($id){

        // verifica sea un número
        if (is_numeric($id)){

            // lo asigna
            $this->Id = $id;

        // si no es un número
        } else {

            // presenta el mensaje y abandona
            echo "La clave del cliente debe ser un número";
            exit;

        }

    }
    public function setCliente($cliente){

        // lo convierte a letra capital y asigna
        $this->Cliente = ucwords(strtolower($cliente));

    }
    public function setLocalidad($localidad){

        // lo convierte a mayúsculas
        $this->Localidad = strtoupper($localidad);

    }
    public function setDomicilio($domicilio){

        // lo convierte a letra capital
        $this->Domicilio = ucwords(strtolower($domicilio));

    }
    public function setTelefono($telefono){
        $this->Telefono = $telefono;
    }
    public function setMail($mail){
        $this->Mail = $mail;
    }
    public function setContacto($contacto){
        $this->Contacto = $contacto;
    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }
    public function setUsuario($usuario){

        // verifica que sea la clave
        if(is_numeric($usuario)){

            // asigna el valor
            $this->Usuario = $usuario;

        // si no es un número
        } else {

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        }

    }

    // métodos de retorno de datos
    public function getId(){
        return $this->Id;
    }
    public function getCliente(){
        return $this->Cliente;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getDomicilio(){
        return $this->Domicilio;
    }
    public function getTelefono(){
        return $this->Telefono;
    }
    public function getMail(){
        return $this->Mail;
    }
    public function getContacto(){
        return $this->Contacto;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que busca los datos de un cliente a partir del texto
     * que recibe
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto
     * @return array
     */
    public function buscaCliente($texto){

        // compone la consulta
        $consulta = "SELECT clientes.id AS id_cliente,
                            clientes.cliente AS cliente_cliente,
                            clientes.localidad AS localidad_cliente,
                            clientes.domicilio AS domicilio_cliente,
                            clientes.telefono AS telefono_cliente,
                            clientes.mail AS mail_cliente,
                            clientes.contacto AS contacto_cliente,
                            usuarios.usuarios AS usuario_cliente
                     FROM clientes INNER JOIN usuarios ON clientes.usuario = usuarios.id
                     WHERE clientes.cliente LIKE '%$texto%' OR
                           clientes.localidad LIKE '%$texto%' OR
                           clientes.telefono LIKE '%$texto%'
                     ORDER BY clientes.cliente;";
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

            // retorna la matriz
            return $fila;

        }

    }

    /**
     * Método que retorna un array con los datos de todos los clientes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listarClientes(){

            // componemos la consulta
            $consulta = "SELECT clientes.id AS id_cliente,
                                clientes.cliente AS nombre_cliente,
                                clientes.localidad AS localidad_cliente,
                                clientes.domicilio AS domicilio_cliente,
                                clientes.telefono AS telefono_cliente,
                                clientes.mail AS mail_cliente,
                                clientes.contacto AS contacto_cliente,
                                usuarios.usuario AS usuario_cliente,
                                DATE_FORMAT(clientes.fecha, '%d/%m/%Y') AS fecha_cliente
                         FROM clientes LEFT JOIN usuarios ON clientes.usuario = usuarios.id
                         ORDER BY clientes.cliente;";
            $resultado = $this->Link->query($consulta);

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

            // retornamos el array
            return $fila;

    }

    /**
     * Métoddo que recibe la id de un cliente, si no la recibe la
     * busca en las variables de clase, obtiene el registro y lo
     * asigna en las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id
     * @return array
     */
    public function obtenerCliente($id){

        // inicializamos las variables locales
        $id_cliente = 0;
        $cliente_cliente = "";
        $localidad_cliente = "";
        $domicilio_cliente = "";
        $telefono_cliente = "";
        $contacto_cliente = "";
        $observaciones_cliente = "";
        $usuario_cliente = "";
        $fecha_alta = "";

        // compone la consulta
        $consulta = "SELECT clientes.id AS id_cliente,
                            clientes.cliente AS cliente_cliente,
                            clientes.localidad AS localidad_cliente,
                            clientes.domicilio AS domicilio_cliente,
                            clientes.telefono AS telefono_cliente,
                            clientes.mail AS mail_cliente,
                            clientes.contacto AS contacto_cliente,
                            clientes.observaciones AS observaciones_cliente,
                            usuarios.usuario AS usuario_cliente,
                            DATE_FORMAT(clientes.fecha, '%d/%m/%Y') AS fecha_alta
                     FROM clientes INNER JOIN usuarios ON clientes.usuario = usuarios.id
                     WHERE clientes.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

            //asignamos a las variables de clase
            $this->Id = $id_cliente;
            $this->Cliente = $cliente_cliente;
            $this->Localidad = $localidad_cliente;
            $this->Domicilio = $domicilio_cliente;
            $this->Telefono = $telefono_cliente;
            $this->Mail = $mail_cliente;
            $this->Contacto = $contacto_cliente;
            $this->Observaciones = $observaciones_cliente;
            $this->Usuario = $usuario_cliente;
            $this->FechaAlta = $fecha_alta;

            // retorna verdadero
            return true;

        }

    }

    /**
     * Método que ejecuta la consulta de actualización o inserción
     * y retorn al id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaCliente(){

        // si no tiene id el cliente
        if (empty($this->Id)){
            $this->nuevoCliente();
        } else {
            $this->editaCliente();
        }

        // retorna la id del registro editado / insertado
        return $this->Id;

    }

    /**
     * Método que ejecuta la inserción de un nuevo registro en la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoCliente(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO clientes
                            (cliente,
                             localidad,
                             domicilio,
                             telefono,
                             mail,
                             contacto,
                             observaciones,
                             usuario,
                             fecha)
                            VALUES
                            ('$this->Cliente',
                             '$this->Localidad',
                             '$this->Domicilio',
                             '$this->Telefono',
                             '$this->Mail',
                             '$this->Contacto',
                             '$this->Observaciones',
                             '$this->Usuario',
                             '$this->Fecha');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // obtiene la id del registro insertado
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición del registro de clientes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaCliente(){

        // llama la consulta de auditoría
        $this->auditoriaCliente("Edición");

        // compone la consulta de edición
        $consulta = "UPDATE clientes SET
                            cliente = '$this->Cliente',
                            localidad = '$this->Localidad',
                            domicilio = '$this->Domicilio',
                            telefono = '$this->Telefono',
                            mail = '$this->Mail',
                            contacto = '$this->Contacto',
                            observaciones = '$this->Observaciones',
                            usuario = '$this->Usuario',
                            fecha = '$this->Fecha'
                        WHERE clientes.id = '$this->Id';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

    /**
     * Método que recibe como parámetro la clave del cliente y ejecuta
     * la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id
     */
    public function borraCliente($id){

        // la asigna a la clase
        $this->Id = $id;

        // llama la auditoría
        $this->auditoriaCliente("Eliminación");

        // compone y ejecuta la consulta
        $consulta = "DELETE FROM clientes
                     WHERE id = '$this->Id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el estado
        return $estado;

    }

    /**
     * Método que retorna si se puede borrar el cliente sin dejar
     * unidades huérfanas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del cliente
     * @return boolean
     */
    public function puedeBorrar($id){

        // inicializamos las variables
        $registros = 0;

        // compone la consulta buscando si el cliente tiene
        // declarada alguna unidad
        $consulta = "SELECT COUNT(egresos.id) AS registros
                     FROM egresos
                     WHERE egresos.cliente = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si no tiene registros
        if ($registros == 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método protegido que recibe como parámetro el evento y realiza
     * la consulta de auditoría
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento
     */
    protected function auditoriaCliente($evento){

        // obtiene los datos del registro actual
        $consulta = "SELECT clientes.id AS id_cliente,
                            clientes.cliente AS cliente_cliente,
                            clientes.localidad AS localidad_cliente,
                            clientes.domicilio AS domicilio_cliente,
                            clientes.telefono AS telefono_cliente,
                            clientes.mail AS mail_cliente,
                            clientes.contacto AS contacto_cliente,
                            clientes.observaciones AS observaciones_cliente,
                            clientes.usuario AS usuario_cliente,
                            clientes.fecha AS fecha_cliente
                     FROM clientes
                     WHERE clientes.id = '$this->Id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // ahora insertamos en la tabla de auditoría
        $consulta = "INSERT INTO auditoria_clientes
                            (id,
                             cliente,
                             localidad,
                             domicilio,
                             telefono,
                             mail,
                             contacto,
                             observaciones,
                             usuario,
                             fecha,
                             evento)
                            VALUES
                            ('$id_cliente',
                             '$cliente_cliente',
                             '$localidad_cliente',
                             '$domicilio_cliente',
                             '$telefono_cliente',
                             '$mail_cliente',
                             '$contacto_cliente',
                             '$observaciones_cliente',
                             '$usuario_cliente',
                             '$fecha_cliente',
                             '$evento');";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // retorna
        return;

    }

    /**
     * Método que recibe el nombre y el mail de un cliente y verifica
     * que no se encuentre repetido
     * @author Claudio Invernizi <cinvernizzi@gmail.com>
     * @param string cliente - nombre del cliente
     * @param string mail - el mail del cliente
     */
    public function validaCliente($cliente, $mail){

        // componemos la consulta
        $consulta = "SELECT COUNT(clientes.id) AS registros
                     FROM clientes
                     WHERE clientes.cliente = '$cliente' AND
                           clientes.mail = '$mail';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y retornamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

}
?>
