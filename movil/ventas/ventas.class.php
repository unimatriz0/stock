<?php

/**
 *
 * Class Ventas | ventas/ventas.class.php
 *
 * @package     Stock
 * @subpackage  Ventas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones de la tabla de egresos del
 * depósito
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definición de la clase
class Ventas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdEgreso;             // clave del registro de egresos
    protected $Item;                 // clave del item
    protected $Marca;                // marca del item
    protected $Modelo;               // modelo del item
    protected $Cantidad;             // número elementos
    protected $Importe;              // importe facturado
    protected $TipoVenta;            // tipo de venta
    protected $Remito;               // clave del remito
    protected $Fecha;                // fecha de la operación
    protected $Usuario;              // clave del usuario
    protected $Link;                 // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdEgreso = 0;
        $this->Item = "";
        $this->Marca = "";
        $this->Modelo = "";
        $this->Cantidad = "";
        $this->Importe = "";
        $this->TipoVenta = "";
        $this->Remito = "";
        $this->Fecha = "";

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // obtenemos la id del usuario
        $this->Usuario = $_COOKIE["Id"];

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables permite asignar nulo
    // para forzar la inserción de un registro
    public function setIdEgreso($id_egreso){

        // lo asigna
        $this->IdEgreso = $id_egreso;

    }
    public function setUsuario($usuario){

        // verifica que sea un número
        if (is_numeric($usuario)){

            // lo asigna
            $this->Usuario = $usuario;

        // si no es una clave
        } else {

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        }

    }
    public function setItem($item){

        // verifica que sea un número
        if (is_numeric($item)){

            // lo asigna
            $this->Item = $item;

        // si no es un número
        } else {

            // abandona por error
            echo "La clave del repuesto debe ser un número";
            exit;

        }

    }
    public function setCantidad($cantidad){

        // verifica que sea un número
        if (is_numeric($cantidad)){

            // lo asigna
            $this->Cantidad = $cantidad;

        // si no es un número
        } else {

            // abandona por error
            echo "La cantidad de repuestos debe ser un número";
            exit;

        }

    }
    public function setImporte($importe){

        // verifica sea un número
        if(is_numeric($importe)){

            // lo asigna
            $this->Importe = $importe;

        // si no es un número
        } else {

            // abandona por error
            echo "El importe debe ser un número";
            exit;

        }

    }
    public function setCliente($cliente){
        $this->Cliente = $cliente;
    }
    public function setRemito($remito){
        $this->Remito = $remito;
    }
    public function setTipoVenta($tipo){
        $this->TipoVenta = $tipo;
    }

    // métodos de retorno de valor
    public function getIdEgreso(){
        return $this->IdEgreso;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getItem(){
        return $this->Item;
    }
    public function getCantidad(){
        return $this->Cantidad;
    }
    public function getImporte(){
        return $this->Importe;
    }
    public function getRemito(){
        return $this->Remito;
    }
    public function getCliente(){
        return $this->Cliente;
    }
    public function getTipoVenta(){
        return $this->TipoVenta;
    }
    public function getMarca(){
        return $this->Marca;
    }
    public function getModelo(){
        return $this->Modelo;
    }

    /**
     * Método que recibe como parámetro una cadena de texto
     * y retorna un array con los registros que cumplen
     * con el criterio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - texto a buscar
     * @return array
     */
    public function buscaEgreso($texto){

        // usamos la vista predefinida
        $consulta = "SELECT *
                     FROM vw_egresos
                     WHERE vw_egresos.cliente LIKE '%$texto%' OR
                           vw_egresos.marca = '$texto' OR
                           vw_egresos.modelo LIKE '%$texto%' OR
                           vw_egresos.remito = '$texto'
                     ORDER BY egresos.cliente;";
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

            // retorna la matriz
            return $fila;

        }

    }

    /**
     * Método que llamma la consulta de edición o inserción según corresponda
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaEgreso(){

        // si no está definida la clave
        if (empty($this->IdEgreso)){
            $this->nuevoEgreso();
        } else {
            $this->editaEgreso();
        }

        // obtenemos la marca y el modelo del item
        $consulta = "SELECT vw_egresos.marca AS marca,
                            vw_egresos.modelo AS modelo
                     FROM vw_egresos
                     WHERE vw_egresos.id = '$this->IdEgreso';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y lo asignamos a las variables de clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Marca = $marca;
        $this->Modelo = $modelo;

        // retorna la id del registro
        return $this->IdEgreso;

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoEgreso(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO egresos
                            (item,
                             cantidad,
                             importe,
                             cliente,
                             tipo_venta,
                             remito,
                             usuario)
                            VALUES
                            ('$this->Item',
                             '$this->Cantidad',
                             '$this->Importe',
                             '$this->Cliente',
                             '$this->TipoVenta',
                             '$this->Remito',
                             '$this->Usuario');";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // obtiene la id del registro insertado
        $this->IdEgreso = $this->Link->lastInsertId();

        // actualiza el inventario
        $consulta = "UPDATE inventario SET
                            cantidad = cantidad - $this->Cantidad,
                            usuario = '$this->Usuario'
                     WHERE item = '$this->Item';";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaEgreso(){

        // llama la consulta de auditoría
        $this->auditoriaEgresos("Edición");

        // compone la consulta de edición
        $consulta = "UPDATE egresos SET
                            item = '$this->Item',
                            cantidad = '$this->Cantidad',
                            importe = '$this->Importe',
                            remito = '$this->Remito',
                            cliente = '$this->Cliente',
                            tipo_venta = '$this->TipoVenta',
                            usuario = '$this->Usuario'
                     WHERE id = '$this->IdEgreso';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

    /**
     * Método que recibe como parámetro la id de un registro
     * y asigna en las variables de clase los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     */
    public function getEgreso($id){

        // inicializamos las variables
        $id_egreso = 0;
        $id_unidad = 0;
        $item = 0;
        $cantidad = 0;
        $importe = 0;
        $tipo_venta = "";
        $fecha_egreso = "";
        $usuario = "";

        // compone y ejecuta la consulta
        $consulta = "SELECT egresos.id AS id_egreso,
                            egresos.unidad AS id_unidad,
                            egresos.item AS id_item,
                            egresos.cantidad AS cantidad,
                            egresos.importe AS importe,
                            egresos.tipo_venta AS tipo_venta,
                            DATE_FORMAT(egresos.fecha,'%d/%m/%Y') AS fecha_egreso,
                            usuarios.usuario AS usuario
                     FROM egresos INNER JOIN usuarios ON egresos.usuario = usuarios.id
                     WHERE egresos.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables y asignamos
            extract($fila);
            $this->IdEgreso = $id_egreso;
            $this->IdUnidad = $id_unidad;
            $this->Item = $id_item;
            $this->Cantidad = $cantidad;
            $this->TipoVenta = $tipo_venta;
            $this->Fecha = $fecha_egreso;
            $this->Usuario = $usuario;

        }

    }

    /**
     * Mètodo que recibe como parámetro la clave de un remito y retorna
     * el array con la nómina de egresos correspondientes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - clave del remito
     * @return array
     */
    public function nominaEgresos($remito){

        // componemos la consulta
        $consulta = "SELECT vw_egresos.id AS idegreso,
                            vw_egresos.marca AS marca,
                            vw_egresos.modelo AS modelo,
                            vw_egresos.cantidad AS cantidad,
                            vw_egresos.importe AS importe
                     FROM vw_egresos
                     WHERE vw_egresos.remito = '$remito';";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz y retornamos
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que lista la totalidad de los egresos para el reporte
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaEgreso(){

        // usamos la vista
        $consulta = "SELECT DISTINCT(vw_remitos.idremito) AS remito,
                            vw_remitos.cliente AS cliente,
                            vw_remitos.impreso AS impreso,
                            vw_remitos.pago AS pago,
                            vw_remitos.tipo_venta AS tipo_venta,
                            vw_remitos.total AS importe,
                            vw_remitos.fecha AS fecha_egreso
                     FROM vw_remitos
                     ORDER BY STR_TO_DATE(vw_remitos.fecha, '%d/%m/%Y') DESC;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $fila;

    }

    /**
     * Método que elimina el egreso de la tabla y llama la consulta
     * de actualización de los totales de inventario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro a borrar
     */
    public function borraEgreso($id){

        // asignamos en la variable de clase
        $this->IdEgreso = $id;

        // llama la consulta de auditoría
        $this->auditoriaEgresos("Eliminación");

        // actualizamos el total del remito (el importe lo obtuvimos
        // antes de realizar la auditoría)
        $consulta = "UPDATE remitos SET
                            importe = remitos.importe - '$this->Importe'
                     WHERE remitos.id = '$this->Remito';";

        // ejecutamos la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // compone la consulta de eliminación de la tabla de egresos
        $consulta = "DELETE FROM egresos WHERE id = '$this->IdEgreso';";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // ahora obtenemos el importe actualizado
        $consulta = "SELECT remitos.importe
                     FROM remitos
                     WHERE remitos.id = '$this->Remito';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $importe;

    }

    /**
     * Método que inserta el registro en la tabla de auditoría
     * recibe como parámetro la operación realizada
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento
     */
    protected function auditoriaEgresos($evento){

        // obtiene los datos del registro actual
        $consulta = "SELECT egresos.id AS id_egreso,
                            egresos.item AS item_egreso,
                            egresos.cantidad AS cantidad_egreso,
                            egresos.importe AS importe_egreso,
                            egresos.tipo_venta AS tipo_venta,
                            egresos.cliente AS cliente_egreso,
                            egresos.remito AS remito_egreso,
                            egresos.fecha AS fecha_egreso
                     FROM egresos
                     WHERE egresos.id = '$this->IdEgreso';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // asignamos en la variable de clase el importe que luego es la que
        // usamos para actualizar el remito
        $this->Importe = $importe_egreso;
        $this->Remito = $remito_egreso;

        // inserta en la tabla de auditoría
        $consulta = "INSERT INTO auditoria_egresos
                                 (id,
                                  item,
                                  cantidad,
                                  importe,
                                  tipo_venta,
                                  cliente,
                                  fecha,
                                  usuario,
                                  evento)
                                 VALUES
                                 ('$id_egreso',
                                  '$item_egreso',
                                  '$cantidad_egreso',
                                  '$importe_egreso',
                                  '$tipo_venta',
                                  '$cliente_egreso',
                                  '$remito_egreso',
                                  '$fecha_egreso',
                                  '$this->Usuario',
                                  '$evento');";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // si está editando
        if ($evento == "Edición"){

            // actualiza el inventario
            $consulta = "UPDATE inventario SET
                                cantidad = cantidad - $this->Cantidad + $cantidad_egreso
                                fecha = '$this->Fecha',
                                usuario = '$this->Usuario'
                         WHERE item = '$this->Item';";

        // si está eliminando
        } else {

            // actualiza el inventario
            $consulta = "UPDATE inventario SET
                                cantidad = cantidad + $cantidad_egreso,
                                fecha = '$this->Fecha',
                                usuario = '$this->Usuario',
                         WHERE item = '$item_egreso';";

        }

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

    }

}
