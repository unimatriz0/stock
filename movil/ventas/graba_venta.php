<?php

/**
 *
 * egresos/graba_egreso.php
 *
 * @package     Stock
 * @subpackage  Egresos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/10/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Mètodo que recibe por post los datos del formulario del egreso
 * y ejecuta la consulta en el servidor
 *
*/

// incluimos e instanciamos las clases
require_once("ventas.class.php");
require_once("../remitos/remitos.class.php");
$egreso = new Ventas();
$remito = new Remitos();

// primero verificamos el remito
if (!empty($_POST["Remito"])){
    $remito->setIdRemito($_POST["Remito"]);
}

// actualizamos el resto de los datos del remito
$remito->setImporte($_POST["Importe"]);
$remito->setCliente($_POST["Cliente"]);

// grabamos y obtenemos la id
$idremito = $remito->grabaRemito();

// obtenemos el total del remito
$importe = $remito->getImporte();

// ahora insertamos el egreso
$egreso->setItem($_POST["Item"]);
$egreso->setRemito($idremito);
$egreso->setCantidad($_POST["Cantidad"]);
$egreso->setImporte($_POST["Importe"]);
$egreso->setCliente($_POST["Cliente"]);
$egreso->setTipoVenta($_POST["Tipo"]);

// grabamos el registro
$egreso->grabaEgreso();

// retornamos la clave del remito y el importe total
// y los datos de la venta
echo json_encode(array("Id" =>      $idremito,
                       "Importe" => $importe,
                       "Marca"   => $egreso->getMarca(),
                       "Modelo"  => $egreso->getModelo(),
                       "IdEgreso"=> $egreso->getIdEgreso()));

?>