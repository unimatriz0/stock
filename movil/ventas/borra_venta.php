<?php

/**
 *
 * ventas/borra_venta.php
 *
 * @package     Stock
 * @subpackage  Ventas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro la clave de un registro y
 * elimina el mismo
 *
*/

// incluimos e instanciamos las clases
require_once("ventas.class.php");
$ventas = new Ventas();

// obtenemos la nómina
$importe = $ventas->borraEgreso($_GET["venta"]);

// lo enviamos
echo json_encode(array("Importe" => $importe));

?>