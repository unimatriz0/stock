<?php

/**
 *
 * ventas/nomina_ventas.php
 *
 * @package     Stock
 * @subpackage  Ventas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro la clave de un remito
 * y retorna en un array json la nómina de artículos que corresponden
 * a ese remito
 *
*/

// incluimos e instanciamos las clases
require_once("ventas.class.php");
$ventas = new Ventas();

// obtenemos la nómina
$nomina = $ventas->nominaEgresos($_GET["remito"]);

// inicializamos las variables
$egresos = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al array
    $egresos[] = array("Id"       => $idegreso,
                       "Marca"    => $marca,
                       "Modelo"   => $modelo,
                       "Cantidad" => $cantidad,
                       "Importe"  => $importe);

}

// lo enviamos
echo json_encode($egresos);

?>