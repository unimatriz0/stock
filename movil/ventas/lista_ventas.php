<?php

/**
 *
 * ventas/lista_ventas.php
 *
 * @package     Stock
 * @subpackage  Ventas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que lista la nómina de ventas realizadas sin imprimir 
 * y la retorna como un array json
 *
*/

// incluimos e instanciamos las clases
require_once("ventas.class.php");
$ventas = new Ventas();

// obtenemos la nómina
$nomina = $ventas->listaEgreso();

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Fecha"   => $fecha_egreso, 
                       "Cliente" => $cliente,
                       "Tipo"    => $tipo_venta, 
                       "Importe" => $importe);

}

// retornamos el vector
echo json_encode($listado);

?>