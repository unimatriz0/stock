
/*
 * Nombre: ventas.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/12/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock para las ventas o egresos
 *              en la interfaz móvil
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control del inventario
 */
class Ventas {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initVentas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initVentas(){

        // inicializamos las variables
        this.IdItem = 0;                  // clave del item del egreso
        this.Cantidad = 0;                // cantidad egresada
        this.Importe = 0;                 // importe de la venta

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de inventario y lo agrega
     * a la grilla de la interfaz movil
     */
    listaVentas(){

        // lo llamamos asincrónico
        $.ajax({
            url: "ventas/lista_ventas.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                ventas.cargaTabla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} fila - array con los datos del cliente
     * Metodo que recibe como parámetro un array con el registro
     * y lo agrega como una fila al dom de la tabla
     */
    cargaTabla(fila){

        // declaramos las variables
        var tabla = $('#tabla-ventas').DataTable();

        // recorre el vector de resultados
        for(var i=0; i < fila.length; i++){

            // agregamos el registro
            tabla.row.add( [
                fila[i].Fecha,
                fila[i].Cliente,
                fila[i].Tipo,
                fila[i].Importe
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idventa - clave del registro
     * Método que recibe como parámetro la clave de una venta
     * y pide confirmación antes de ejecutar la eliminación
     */
    borraVenta(idventa){

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                    animation: 'flip',
                    title: 'Eliminar Venta',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    overlay: false,
                    zIndex: 20000,
                    content: 'Está seguro que desea eliminar el registro?',
                    theme: 'TooltipBorder',
                    confirm: function() {

                        // llama la rutina de eliminación
                        ventas.eliminaVenta(idventa);

                    },
                    cancel: function(){
                        Confirmacion.destroy();
                    },
                    confirmButton: 'Aceptar',
                    cancelButton: 'Cancelar'

                });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idventa - clave del registro
     * Método llamado desde la grilla de clientes que elimina
     * una venta y luego recarga tanto la grilla de ventas
     * como la de inventario
     */
    eliminaVenta(idventa){

        // lo llamamos sincrónico
        $.ajax({
            url: "ventas/borra_venta.php?venta="+idventa,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                if (data.Importe != 0){

                    // la rutina retorna el importe actualizado
                    // del remito, actualizamos el formulario
                    document.getElementById("importeTotal").value = data.Importe;

                    // recargamos las grillas de ventas, inventario y del remito
                    var table = $('#remitos-ventas').DataTable();
                    table.clear();
                    clientes.examinarRemito();

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar en el formulario
     * de ventas, simplemente lo resetea y fija el foco
     */
    cancelarVenta(){

        // reiniciamos el formulario
        document.getElementById("remitosventas").reset();
        document.getElementById("item_egreso").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón finalizar la venta
     * o al cerrar el formulario, actualiza las grillas de
     * remitos del cliente, de egresos de materiales y de
     * remitos sin imprimir agregando el registro y luego
     * destruye el layer
     */
    finalizarVenta(){

        // destruimos el layer 
        clientes.layerRemitos.destroy();

        // recargamos la grilla
        clientes.listarCompras();
        
    }

    /**
     * @autor Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iditem - clave del item seleccionado
     * Método llamado al seleccionar un item de autocomplete
     * que fija el valor de la clave del item en el formulario
     */
    setIdItem(iditem){

        // fijamos el valor
        document.getElementById("iditem-egreso").value = iditem;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito clave del remito
     * @return {double} valor del remito
     * Método que obtiene el valor del total del remito y
     * retorna
     */
    totalRemito(idremito){

        // lo llamamos sincrónico
        $.ajax({
            url: "remitos/getimporte.php?remito="+idremito,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                if (data.Importe != 0){

                    // actualizamos el campo del formulario
                    document.getElementById("importeTotal").value = data.Importe;

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que antes de
     * enviar al servidor verifica los datos del formulario
     */
    validaVenta(){

        // declaración de variables
        var mensaje;

        // verifica se halla seleccionado un artículo
        if (document.getElementById("iditem-egreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el artículo a vender";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("iditem-egreso").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.IdItem = document.getElementById("iditem-egreso").value;

        }

        // verifica se halla indicado la cantidad
        var cantidad = document.getElementById("cantidadegreso").value;
        if (cantidad == "" || cantidad == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar la cantidad a vender";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cantidadegreso").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Cantidad = cantidad;

        }

        // verifica exista un valor parcial
        var importe = document.getElementById("importeegreso").value;
        if (importe == 0 || importe == ""){

            // presenta el mensaje y retorna
            mensaje = "El importe total es incorrecto.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("importeegreso").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Importe = importe;
        }

        // envía los datos al servidor
        this.grabaVenta();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase graba
     * la venta en la base
     */
    grabaVenta(){

        // creamos el formulario
        var datosVenta = new FormData();

        // asignamos las variables
        datosVenta.append("Item", this.IdItem);
        datosVenta.append("Cantidad", this.Cantidad);
        datosVenta.append("Importe", this.Importe);

        // agregamos el número de remito
        if (document.getElementById("nroRemito").value != ""){
            datosVenta.append("Remito", document.getElementById("nroRemito").value);
        }

        // agregamos el cliente y el tipo que siempre es minorista
        datosVenta.append("Cliente", document.getElementById("idcliente").value);
        datosVenta.append("Tipo", "Minorista");

        // serializa el formulario y lo envía, recibe como respuesta
        // la clave del remito
        $.ajax({
            type: "POST",
            url: "ventas/graba_venta.php",
            data: datosVenta,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió bien
                } else {

                    // almacenamos la id en el formulario y en la
                    // variable de clase
                    document.getElementById("nroRemito").value = data.Id;
                    document.getElementById("importeTotal").value = data.Importe;

                    // carga en la grilla la nómina de egresos
                    // declaramos las variables
                    var tabla = $('#remitos-ventas').DataTable();

                    // componemos el botón
                    var texto =  "<input type='button' ";
                    texto += "           name='btnBorrar' ";
                    texto += "           class='botonborrar' ";
                    texto += "           title='Pulse para borrar la venta' ";
                    texto += "           onClick='ventas.borraVenta(" + data.IdEgreso + ")';>";

                    // agregamos el registro
                    tabla.row.add( [
                        data.Marca,
                        data.Modelo,
                        ventas.Cantidad,
                        ventas.Importe,
                        texto
                    ] ).draw( false );

                    // recargamos los remitos del cliente
                    clientes.recargar();

                    // recargamos los remitos sin imprimir
                    remitos.recargar();

                    // recargamos el inventario
                    inventario.recargar();

                    // recargamos los egresos

                    // limpiamos el formulario
                    ventas.limpiaFormulario();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onblur de la cantidad a vender
     * calcula el importe total de la venta y lo presenta
     */
    calculaImporte(){

        // definimos las variables
        var mensaje = "";
        var cantidad = document.getElementById("cantidadegreso").value;

        // si no seleccionó un item
        if (document.getElementById("iditem-egreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar un item primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.IdItem = document.getElementById("iditem-egreso").value;

        }

        // si la cantidad es inválida
        if (cantidad == "0" || cantidad == "0.00" || cantidad == ""){

            // presenta el mensaje y retorna
            mensaje = "La cantidad debe ser distinta de 0";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos el importe minorista unitario del item
        $.get('../modelos/getmodelo.php','modelo='+this.IdItem,
            function(data){

                // definimos las variables
                var importe;
                var total;

                // calculamos el importe minorista y asignamos en la variable
                importe = data.Minorista * cantidad;

                // asignamos los valores
                document.getElementById("importeegreso").value = importe.toFixed(2);

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario luego de grabar una venta
     */
    limpiaFormulario(){

        // dejamos en blanco los campos
        document.getElementById("iditem-egreso").value = "";
        document.getElementById("item_egreso").value = "";
        document.getElementById("cantidadegreso").value = 0;
        document.getElementById("importeegreso").value = 0;

        // fijamos el foco
        document.getElementById("item_egreso").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recarga la grilla de ventas
     */
    recargar(){

        // limpiamos la tabla y luego la recargamos
        var table = $('#tabla-ventas').DataTable();
        table.clear();
        this.listaVentas();

    }
    
}