<?php

/**
 *
 * Class Inventario | inventario/inventario.class.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que controla el abm de la tabla de inventario
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Inventario{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                  // clave del registro
    protected $Item;                // clave del item
    protected $Cantidad;            // cantidad en existencia
    protected $Critico;             // Nivel crítico que dispara la alarma
    protected $Fecha;               // fecha de alta del registro
    protected $Usuario;             // clave del usuario que editó

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // obtenemos la id del usuario
        $this->Usuario = $_COOKIE["Id"];

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setId($id){

        // verifica que sea un número
        if (is_numeric($id)){

            // lo asigna
            $this->Id = $id;

        // si no es numérico
        } else {

            // abandona por error
            echo "La clave de inventario debe ser un número";
            exit;

        }

    }
    public function setItem($item){

        // verifica que sea un número
        if (is_numeric($item)){

            // asigna
            $this->Item = $item;

        // si no es numérico
        } else {

            // abandona por error
            echo "La clave del item de inventario debe ser un número";
            exit;

        }

    }
    public function setCantidad($cantidad){

        // verifica que sea un número
        if (is_numeric($cantidad)){

            // asigna
            $this->Cantidad = $cantidad;

        // si no es numérico
        } else {

            // abandona por error
            echo "La cantidad del inventario debe ser un número";
            exit;

        }

    }
    public function setCritico($critico){

        // verifica que sea un número
        if (is_numeric($critico)){

            // lo asigna
            $this->Critico = $critico;

        // si no es numérico
        } else {

            // abandona por error
            echo "El nivel crítico debe ser un número";
            exit;

        }

    }

    /**
     * Método que graba el registro en la base y retorna
     * la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaInventario(){

        // si es una inserción
        if (empty($this->Id)){
            $estado = $this->nuevoInventario();
        } else {
            $estado = $this->editaInventario();
        }

        // retorna el estado de la operación
        return $estado;

    }

    /**
     * Método que ejecuta la consulta de inserción, retorna
     * el resultado de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function nuevoInventario(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO inventario
                            (item,
                             cantidad,
                             critico,
                             fecha,
                             usuario)
                            VALUES
                            ('$this->Item',
                             '$this->Cantidad',
                             '$this->Critico',
                             '$this->Fecha',
                             '$this->Usuario');";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retornamos el resultado
        return $estado;

    }

    /**
     * Método que ejecuta la consulta de edición, retorna el
     * resultado de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    protected function editaInventario(){

        // actualiza la auditoría
        $this->auditoriaInventario("Edición");

        // actualiza el registro en la base, solo actualiza
        // las cantidades la descripción lo controlamos
        // por la tabla de repuestos
        $consulta = "UPDATE inventario SET
                            cantidad = '$this->Cantidad',
                            fecha = '$this->Fecha',
                            usuario = '$this->Usuario'
                        WHERE id = '$this->Id';";

        // ejecuta la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retornamos el resultado
        return $estado;

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id
     */
    public function borraInventario($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definido por la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No se ha recibido la clave del registro a eliminar";
                exit;

            // si está definida por la clase
            } else {

                // lo asigna
                $id = $this->Id;

            }

        }

        // actualiza la auditoría
        $this->auditoriaInventario("Eliminación");

        // elimina el registro
        $consulta = "DELETE FROM inventario
                     WHERE id = '$id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // retorna el estado de la operación
        return $estado;

    }

    /**
     * Método que retorna la nómina completa de inventario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaInventario(){

        // directamente consultamos la vista
        $consulta = "SELECT vw_inventario.id AS id,
                            vw_inventario.marca AS marca,
                            vw_inventario.modelo AS modelo,
                            vw_inventario.cantidad AS cantidad,
                            vw_inventario.costo AS costo,
                            vw_inventario.mayorista AS mayorista,
                            vw_inventario.minorista AS minorista,
                            vw_inventario.critico AS critico,
                            vw_inventario.fecha AS fecha
                     FROM vw_inventario;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $fila;

    }

    /**
     * Método que recibe como parámetro el evento y ejecuta la
     * consulta de inserción en la auditoría
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento
     */
    protected function auditoriaInventario($evento){

        // primero obtiene los datos del registro actual
        $consulta = "SELECT inventario.id AS id_inventario,
                            inventario.item AS item_inventario,
                            inventario.cantidad AS cantidad_inventario,
                            inventario.critico AS critico_inventario,
                            inventario.fecha AS fecha_inventario,
                            inventario.usuario AS usuario_inventario
                     FROM inventario
                     WHERE inventario.id = '$this->Id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // graba el registro de auditoría
        $consulta = "INSERT INTO auditoria_inventario
                            (id,
                             item,
                             cantidad,
                             critico,
                             fecha,
                             usuario,
                             evento)
                            VALUES
                            ('$id_inventario',
                             '$item_inventario',
                             '$cantidad_inventario',
                             '$critico_inventario',
                             '$fecha_inventario',
                             '$usuario_inventario',
                             '$evento');";
        $resultado = $this->Link->prepare($consulta);
        $resultado->execute();

        // retorna
        return;

    }

    /**
     * Método que verifica si existen elementos en stock crítico
     * y retorna el array de los mismos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function hayCritico(){

        // componemos la consulta
        $consulta = "SELECT vw_inventario.marca AS marca,
                            vw_inventario.modelo AS modelo,
                            CONCAT_WS(' ', vw_inventario.modelo) AS modelo,
                            vw_inventario.cantidad AS cantidad,
                            vw_inventario.critico AS critico
                     FROM vw_inventario
                     WHERE vw_inventario.critico != 0 AND
                           vw_inventario.critico >= vw_inventario.cantidad;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el array
        return $fila;

    }

    /**
     * Método que recibe como parámetro la clave de un item y verifica que no
     * quede por debajo del valor crítico
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del item
     * @return boolean
     */
    public function VerificaCritico($id){

        // verifica que sea un número
        if (!is_numeric($id)){

            // abandona por error
            echo "La clave del repuesto debe ser un número";
            exit;

        }

        // compone la consulta
        $consulta = "SELECT vw_inventario.marca AS marca,
                            vw_inventario.modelo AS modelo,
                            CONCAT_WS(' ', vw_inventario.repuesto, vw_inventario.color) AS repuesto
                     FROM vw_inventario
                     WHERE vw_inventario.critico != 0 AND
                           vw_inventario.critico >= vw_inventario.cantidad AND
                           vw_inventario.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el array
        return $fila;

    }

}
?>
