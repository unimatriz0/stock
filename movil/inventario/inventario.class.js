
/*
 * Nombre: inventario.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/12/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock para el inventario
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control del inventario
 */
class Inventario {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initInventario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initInventario(){

        // inicializamos las variables

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de inventario y lo agrega
     * a la grilla de la interfaz movil
     */
    listaInventario(){

        // lo llamamos asincrónico
        $.ajax({
            url: "inventario/lista_inventario.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                inventario.cargaTabla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} fila - array con los datos del cliente
     * Metodo que recibe como parámetro un array con el registro
     * y lo agrega como una fila al dom de la tabla
     */
    cargaTabla(fila){

        // declaramos las variables
        var tabla = $('#tabla-inventario').DataTable();
        var enlace = "";
        var cantidad = "";

        // recorre el vector de resultados
        for(var i=0; i < fila.length; i++){

            // armamos el enlace
            enlace =  "<input type='button' ";
            enlace += "        name='btnGrabar' ";
            enlace += "        class='botongrabar' ";
            enlace += "        title='Pulse para actualizar el registro' ";
            enlace += "        onClick='inventario.Grabar(" + fila[i].Id + ")';>";

            // arma el texto con la cantidad
            cantidad = "<input type='number' ";
            cantidad += "      name='cantidad_" + fila[i].Id + "'";
            cantidad += "      id='cantidad_" + fila[i].Id + "'";
            cantidad += "      class='numero' ";
            cantidad += "      title='Existencia en depósito' ";
            cantidad += "      value='" + fila[i].Cantidad + "' ";
            cantidad += "      step='0.01'>";

            // agregamos el registro
            tabla.row.add( [
                fila[i].Marca,
                fila[i].Modelo,
                fila[i].Minorista,
                cantidad,
                enlace
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que recibe como parámetro la clave del registro
     * y compone el formulario de datos para enviar al servidor
     */
    Grabar(id){

        // obtenemos la cantidad
        var cantidad = document.getElementById("cantidad_" + id).value;

        // componemos el formulario y fijamos los valores
        var datosInventario = new FormData();
        datosInventario.append("Id", id);
        datosInventario.append("Cantidad", cantidad);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "inventario/graba_inventario.php",
            type: "POST",
            data: datosInventario,
            cahe: false,
            contentType: false,
            dataType: 'json',
            processData: false,
            success: function(data) {

                // si hubo un error
                if (data.Error == false){

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió todo bien
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado", color: 'green'});

                }
            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recarga la grilla del inventario
     */
    recargar(){

        // limpiamos la tabla y luego recargamos
        var table = $('#tabla-inventario').DataTable();
        table.clear();
        this.listaInventario();

    }

}