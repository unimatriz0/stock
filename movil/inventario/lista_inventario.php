<?php

/**
 *
 * inventario/lista_inventario.php
 *
 * @package     Stock
 * @subpackage  Inventario
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que lista la nómina de inventario y la retorna
 * como un array json
 *
*/

// incluimos e instanciamos las clases
require_once("inventario.class.php");
$inventario = new Inventario();

// obtenemos la nómina
$nomina = $inventario->listaInventario();

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Id"        => $id,
                       "Marca"     => $marca,
                       "Modelo"    => $modelo,
                       "Minorista" => $minorista,
                       "Cantidad"  => $cantidad);

}

// retornamos el vector
echo json_encode($listado);

?>