<?php

/**
 *
 * modelos/lista_modelos.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe la clave de una marca y retorna el array de
 * modelos en formato json
 *
*/

// incluye e instancia la clase
require_once ("modelos.class.php");
$modelos = new Modelos();

// obtiene la lista de modelos
if (!empty($_GET["marca"])){
    $lista = $modelos->listaModelos($_GET["marca"]);
} else {
    $lista = $modelos->listaModelos();
}

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach($lista AS $fila){

    // obtiene el registro
    extract($fila);

    // lo agrega a la matriz
    $jsondata[] = array("id"     => $clave_modelo,
                        "modelo" => $descripcion_modelo,
                        "marca"  => $marca_modelo);

}

// devuelve la cadena
echo json_encode($jsondata);
?>
