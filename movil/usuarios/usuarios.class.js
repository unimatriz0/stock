/*
 * Nombre: usuarios.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/12/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones que regulan la acreditación del usuario
 *              en el sistema
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no tiene que haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta los métodos para la acreditación
 * de los usuarios en el sistema
 */
class Usuarios {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos la clase
        this.initUsuarios();
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de la clase
     */
    initUsuarios(){

        // inicializamos las variables
        this.layerUsuarios = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica se hallan completado los datos
     * de ingreso
     */
    Ingresar(){

        // obtenemos las variables
        var mensaje = "";
        var usuario = document.getElementById("usuario").value;
        var password = document.getElementById("password").value;

        // si no ingresó el usuario
        if (usuario == ""){

           // presenta el mensaje y retorna
           mensaje = "Debe ingresar su nombre de usuario";
           new jBox('Notice', {content: mensaje, color: 'red'});
           document.getElementById("usuario").focus();
           return false;

        }

        // si no ingresó la contraseña
        if (password == ""){

           // presenta el mensaje y retorna
           mensaje = "Debe ingresar su contraseña";
           new jBox('Notice', {content: mensaje, color: 'red'});
           document.getElementById("password").focus();
           return false;

        }

        // verificamos las credenciales
        $.ajax({
            type: "GET",
            url: "usuarios/valida_usuario.php?usuario="+usuario+"&password="+password,
            dataType: 'json',
            success: function(data) {

                // si hubo un error
                if (data.Error == false){

                    // presenta el mensaje de error
                    mensaje = "Ha ocurrido un error, verifique el<br>";
                    mensaje += "nombre de usuario y la contraseña<br>";
                    mensaje += "que ha ingresado.";
                    new jBox('Notice', {content: mensaje, color: 'red'});
                    document.getElementById("usuario").focus();

                // si resultó correcto
                } else {

                    // asigna las cookies para que expiren en seis meses
                    Cookies.set('Id', data.Id, { expires: 180 });
                    Cookies.set('Usuario', data.Usuario, { expires: 180 });
                    Cookies.set('Clientes', data.Clientes, { expires: 180 });
                    Cookies.set('Ingresos', data.Ingresos, { expires: 180 });
                    Cookies.set('Egresos', data.Egresos, { expires: 180 });
                    Cookies.set('Administrador', data.Administracor, { expires: 180 });

                    // asigna en el sessionstorage
                    sessionStorage.setItem("Id", data.ID);
                    sessionStorage.setItem("Usuario", data.Usuario);
                    sessionStorage.setItem("Clientes", data.Clientes);
                    sessionStorage.setItem("Ingresos", data.Ingresos);
                    sessionStorage.setItem("Egresos", data.Egresos);
                    sessionStorage.setItem("Administrador", data.Administrador);

                    // cierra el canvas
                    usuarios.layerUsuarios.destroy();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al inicio del sistema que verifica
     * si el usuario se encuentra autenticado, en caso
     * contrario, pide el nombre y contraseña
     */
    verificar(){

        // si el usuario està autenticado
        var usuario = Cookies.get('Usuario');

        // si no está presente el valor
        if (typeof(usuario) == "undefined"){

            // mostramos el ingreso
            usuarios.layerUsuarios = new jBox('Modal', {
                     animation: 'flip',
                     closeOnEsc: false,
                     closeOnClick: false,
                     closeOnMouseleave: false,
                     closeButton: false,
                     repositionOnContent: true,
                     overlay: true,
                     theme: 'TooltipBorder',
                     onCloseComplete: function(){
                        this.destroy();
                    },
                    ajax: {
                        url: 'usuarios/form_ingreso.html',
                        reload: 'strict'
                    }
                });
           usuarios.layerUsuarios.open();

        }

    }

}
