<?php

/**
 *
 * Class Usuarios | usuarios/usuarios.class.php
 *
 * @package     Stock
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que provee los métodos de acceso a la tabla de usuarios del sistema
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Usuarios{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                  // clave del registro
    protected $Usuario;             // nombre del usuario
    protected $Nombre;              // nombre completo del usuario
    protected $Password;            // contraseña de acceso encriptada
    protected $Clientes;            // autorizado para la tabla de clientes
    protected $Egresos;             // autorizado para la tabla de egresos
    protected $Ingresos;            // autorizado para la tabla de ingresos
    protected $Administrador;       // puede administrar el sistema (si/no)
    protected $Activo;              // si el usuario está activo
    protected $Link;                // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

    }

    /**
     * destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // asigna el nombre de usuario
    public function setUsuario($usuario){

        // verifica la longitud
        if (strlen($usuario) < 4 || strlen($usuario) > 15){

            // abandona con mensaje de error
            echo "La longitud del nombre de usuario es de " . strlen($usuario);
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Usuario = $usuario;

        }

    }

    // asigna el password verificando la longitud
    public function setPassword($password){

        // verifica la longitud
        if (strlen($password) < 4 || strlen($password) > 15){

            // abandona con mensaje de error
            echo "La longitud de la contraseña es de " . strlen($password);
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Password = $password;

        }

    }

    // funciones de recuperación de datos
    public function getIdUsuario(){
        return $this->Id;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getClientes(){
        return $this->Clientes;
    }
    public function getIngresos(){
        return $this->Ingresos;
    }
    public function getEgresos(){
        return $this->Egresos;
    }
    public function getAdministrador(){
        return $this->Administrador;
    }
    public function getActivo(){
        return $this->Activo;
    }

    /**
      * método público que recibe como parámetro una id de un registro
      * y asigna los valores a la clase con los datos de ese usuario
      * si no recibe una id, verifica si está seteada la propiedad de
      * la clase
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * @param int $id - clave del usuario
      */
    public function obtenerUsuario($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está asignada
            if (empty($this->Id)){

                // abandona por error
                echo "No se ha asignado la clave del usuario";
                exit;

            // si está asignada la id
            } else {

                // la asigna a la variable del método
                $id = $this->Id;

            }

        }

        // declaramos las variables
        $id_usuario = 0;
        $nombre_usuario = "";
        $password_usuario = "";
        $usuarios_usuario = "";
        $clientes_usuario = "";
        $ingresos_usuario = "";
        $administrador_usuario = "";
        $activo_usuario = "";

        // ahora componemos la consulta, traemos el password
        // encriptado también porque lo vamos a usar en el
        // cambio de password
        $consulta = "SELECT usuarios.id AS id_usuario,
                            usuarios.nombre AS nombre_usuario,
                            usuarios.password AS password_usuario,
                            usuarios.usuario AS usuarios_usuario,
                            usuarios.clientes AS clientes_usuario,
                            usuarios.ingresos AS ingresos_usuario,
                            usuarios.egresos AS egresos_usuario,
                            usuarios.administrador AS administrador_usuario,
                            usuarios.activo AS activo_usuario
                     FROM usuarios
                     WHERE usuarios.id = '$id';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retorna falso
            return false;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

            //asignamos a las variables de clase
            $this->Id = $id_usuario;
            $this->Nombre = $nombre_usuario;
            $this->Usuario = $usuarios_usuario;
            $this->Password = $password_usuario;
            $this->Clientes = $clientes_usuario;
            $this->Ingresos = $ingresos_usuario;
            $this->Egresos = $egresos_usuario;
            $this->Administrador = $administrador_usuario;
            $this->Activo = $activo_usuario;

            // retorna verdadero
            return true;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     * procedimiento usado al ingreso, verifica el nombre de usuario
     * y la contraseña y setea las variables de sesión del sistema
     */
    public function Validar(){

        // obtiene el password de la base usamos una rutina independiente
        // para realizar la verificación porque si llamamos a obtenerUsuario
        // va a pisar las variables de la clase

        // componemos y ejecutamos la consulta
        $consulta = "SELECT usuarios.id AS id,
                            usuarios.usuario AS usuario,
                            usuarios.password AS password,
                            usuarios.clientes AS clientes,
                            usuarios.ingresos AS ingresos,
                            usuarios.egresos AS egresos,
                            usuarios.administrador AS administrador,
                            usuarios.activo AS activo
                     FROM usuarios
                     WHERE usuarios.usuario = '$this->Usuario' AND
                           usuarios.password = MD5('$this->Password') AND
                           usuarios.activo = 'Si';";
        $resultado = $this->Link->query($consulta);

        // si encontró registros
        if ($resultado->rowCount() != 0){

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

                ini_set("session.cookie_lifetime","36000");
                ini_set("session.gc_maxlifetime","36000");

                // asignamos en las variables de clase
                $this->Id = $id;
                $this->Usuario = $usuario;
                $this->Clientes = $clientes;
                $this->Ingresos = $ingresos;
                $this->Egresos = $egresos;
                $this->Administrador = $administrador;

                // retornamos correcto
                $correcto = true;

        // si no encontró el usuario
        } else {

            // retornamos el error
            $correcto = false;

        }

        // retorna el resultado o el estado de la operación
        return $correcto;

    }

}
?>
