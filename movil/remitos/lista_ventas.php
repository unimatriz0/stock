<?php

/**
 *
 * remitos/lista_ventas.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (30/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que lista la nómina de ventas correspondiente a 
 * un remito y lo retorna en un array json
 *
*/

// incluimos e instanciamos las clases
require_once("remitos.class.php");
$remitos = new Remitos();

// obtenemos la nómina
$nomina = $remitos->getVentasRemito($_GET["idremito"]);

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Id"       => $id,
                       "Marca"    => $marca,
                       "Modelo"   => $modelo,
                       "Cantidad" => $cantidad,
                       "Importe"  => $importe);

}

// retornamos el vector
echo json_encode($listado);

?>