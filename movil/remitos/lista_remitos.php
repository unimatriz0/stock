<?php

/**
 *
 * inventario/lista_remitos.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que lista la nómina de remitos sin imprimir
 * y la retorna como un array json
 *
*/

// incluimos e instanciamos las clases
require_once("remitos.class.php");
$remitos = new Remitos();

// obtenemos la nómina
$nomina = $remitos->sinImprimir();

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Id"      => $idremito,
                       "Fecha"   => $fecha,
                       "Cliente" => $cliente,
                       "Tipo"    => $pago,
                       "Importe" => $total);

}

// retornamos el vector
echo json_encode($listado);

?>