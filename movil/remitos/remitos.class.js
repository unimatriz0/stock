
/*
 * Nombre: remitos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/12/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock para los remitos sin imprimir
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control del inventario
 */
class Remitos {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initRemitos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initRemitos(){

        // inicializamos las variables

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de inventario y lo agrega
     * a la grilla de la interfaz movil
     */
    listaRemitos(){

        // lo llamamos asincrónico
        $.ajax({
            url: "remitos/lista_remitos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                remitos.cargaTabla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} fila - array con los datos del cliente
     * Metodo que recibe como parámetro un array con el registro
     * y lo agrega como una fila al dom de la tabla
     */
    cargaTabla(fila){

        // declaramos las variables
        var tabla = $('#tabla-remitos').DataTable();
        var texto = "";

        // recorre el vector de resultados
        for(var i=0; i < fila.length; i++){

            // componemos el botón
            texto =  "<input type='button' ";
            texto += "        name='btnImprimir' ";
            texto += "        class='botonimprimir' ";
            texto += "        title='Pulse para imprimir el remito' ";
            texto += "        onClick='remitos.Imprimir(" + fila[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                fila[i].Id,
                fila[i].Fecha,
                fila[i].Cliente,
                fila[i].Tipo,
                fila[i].Importe,
                texto
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito, clave del remito
     * Método que recibe como parámetro la clave de un remtio y 
     * obtiene la nómina de ventas correspondiente al remito, luego 
     * carga la grilla de ventas
     */
    ventasRemitos(idremito){

        // obtenemos la nómina de ventas
        // lo llamamos asincrónico
        $.ajax({
            url: "remitos/lista_ventas.php?remito="+idremito,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                remitos.cargaVentas(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos
     * Método que recibe como parámetro el vector de resultados 
     * con las ventas de un remito, recorre el vector agregando 
     * los elementos a la tabla 
     */
    cargaVentas(datos){

        // declaramos las variables
        var tabla = $('#remitos-ventas').DataTable();
        var texto = "";

        // recorre el vector de resultados
        for(var i=0; i < fila.length; i++){

            // componemos el botón
            texto =  "<input type='button' ";
            texto += "        name='btnBorrar' ";
            texto += "        class='botonborrar' ";
            texto += "        title='Pulse para borrar la venta' ";
            texto += "        onClick='ventas.Borrar(" + fila[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                fila[i].Marca,
                fila[i].Modelo,
                fila[i].Cantidad,
                fila[i].Importe,
                texto
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método que recibe como parámetro la clave del remito a
     * imprimir y genera el mismo
     */
    Imprimir(idremito){

        // generamos el certificado y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento, en el evento onclose pedimos confirmación
        // para marcar el remito como impreso
        this.layerRemito = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        onCloseComplete: function(){
                            remitos.confirmaImpresion(idremito);
                        },
                        title: 'Remito de Entrega',
                        draggable: 'title',
                        repositionOnContent: true,
                        theme: 'TooltipBorder',
                        zIndex: 15000,
                        ajax: {
                            url: 'remitos/generar_remito.php?remito='+idremito,
                            reload: 'strict'
                        }
                    });
        this.layerRemito.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idremito - clave del remito
     * Método llamado al cerrar la presentación del pdf que pide
     * confirmación para marcar el remito como impreso
     */
    confirmaImpresion(idremito){

        // declaración de variables
        var mensaje;

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Impresión de Remitos',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: 'Desea marcar el remito como impreso?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // llama la rutina php
                                $.get('remitos/confirmaimp.php?id='+idremito,
                                    function(data){

                                        // si retornó correcto
                                        if (data.Error != 0){

                                            // recargamos la grilla para reflejar los cambios
                                            remitos.recargar();

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Impresión registrada", color: 'green'});

                                            // eliminamos el layer del remito
                                            remitos.layerRemito.destroy();

                                        // si no pudo eliminar
                                        } else {

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                        }

                                    }, "json");

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia la tabla de remitos sin imprimir y
     * luego la recarga
     */
    recargar(){

        // limpiamos la tabla y la redibujamos
        var table = $('#tabla-remitos').DataTable();
        table.clear();
        this.listaRemitos();

    }

}