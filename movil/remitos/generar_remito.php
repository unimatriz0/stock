<?php

/**
 *
 * remitos/generar_remito.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (05/10/2018)
 * @copyright   Copyright (c) 2018, KiwiDigital
 *
 * Método que recibe por get la clave de un remito, llama la
 * clase que retorna el contenido del documento y lo presenta
*/

// subimos de directorio para poder incluir
// la clase de impresión sin tener que armar
// el path de las fuentes (el directorio actual
// debe estar en /remitos/)
chdir ("../../remitos");

// incluimos e instanciamos las clases
require_once ("../egresos/remitos.class.php");
$remito = new Remitos($_GET["remito"]);

// enviamos los headers
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// ahora lo presentamos
?>
<object data="../../stock/remitos/remito.pdf"
        type="application/pdf"
        width="850" height="500">
</object>