<?php

/**
 *
 * remitos/remito_cliente.php
 *
 * @package     Stock
 * @subpackage  Remitos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetro la id de un cliente y 
 * retorna en un array json la nómina de remitos
 *
*/

// incluimos e instanciamos las clases
require_once("remitos.class.php");
$remitos = new Remitos();

// obtenemos la nómina
$nomina = $remitos->nominaRemitos($_GET["cliente"]);

// inicializamos el array
$listado = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $listado[] = array("Fecha"   => $fecha, 
                       "Id"      => $idremito,
                       "Importe" => $importe);

}

// retornamos el vector
echo json_encode($listado);

?>