<?php

/**
 *
 * modelos/borra_modelo.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave e un registro y
 * ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once ("modelos.class.php");
$modelo = new Modelos();

// asignamos el valor y ejecutamos
$resultado = $modelo->borraModelo($_GET["id"]);

// retornamos el estado de la operación
$jsondata["Error"] = $resultado;
echo json_encode($jsondata);

?>
