<?php

/**
 *
 * Class Modelos | modelos/modelos.class.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla el abm de modelos de la base
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Modelos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                      // clave del modelo
    protected $Marca;                   // clave de la marca
    protected $Descripcion;             // descripción del modelo
    protected $Critico;                 // valor crítico de stock
    protected $Costo;                   // valor de compra
    protected $IncMayorista;            // porcentaje de ganancia mayorista
    protected $IncMinorista;            // porcentaje de ganancia minorista
    protected $Mayorista;               // precio de venta mayorista
    protected $Minorista;               // precio de venta minorista
    protected $Fecha;                   // fecha de alta modificación
    protected $Usuario;                 // nombre del usuario
    protected $IdUsuario;               // clave del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos la fecha en formato mysql
        $this->Fecha = date('Y-m-d');

        // inicializamos las variables
        $this->Id = 0;
        $this->Marca = "";
        $this->Descripcion = "";
        $this->Critico = "";
        $this->Costo = "";
        $this->IncMayorista = 0;
        $this->IncMinorista = 0;
        $this->Mayorista = 0;
        $this->Minorista = 0;
        $this->Usuario = "";

        // obtenemos la id del usuario
        $this->IdUsuario = $_COOKIE["ID"];

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setId($id){

        // verifica que sea un número
        if (is_numeric($id)){

            // lo asigna
            $this->Id = $id;

        // si no es numérico
        } else {

            // abandona por error
            echo "La clave del modelo debe ser un número";
            exit;

        }

    }
    public function setMarca($marca){

        // verifica que sea un número
        if (is_numeric($marca)){

            // lo asigna
            $this->Marca = $marca;

        // si no es un número
        } else {

            // abandona por error
            echo "La clave de la marca debe ser un número";
            exit;

        }

    }
    public function setDescripcion($descripcion){
        $this->Descripcion = $descripcion;
    }
    public function setCritico($critico){
        $this->Critico = $critico;
    }
    public function setCosto($costo){
        $this->Costo = $costo;
    }
    public function setIncMayorista($incremento){
        $this->IncMayorista = $incremento;
    }
    public function setIncMinorista($incremento){
        $this->IncMinorista = $incremento;
    }
    public function setMayorista($mayorista){
        $this->Mayorista = $mayorista;
    }
    public function setMinorista($minorista){
        $this->Minorista = $minorista;
    }

    // Métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getMarca(){
        return $this->Marca;
    }
    public function getDescripcion(){
        return $this->Descripcion;
    }
    public function getCritico(){
        return $this->Critico;
    }
    public function getCosto(){
        return $this->Costo;
    }
    public function getIncMayorista(){
        return $this->IncMayorista;
    }
    public function getIncMinorista(){
        return $this->IncMinorista;
    }
    public function getMayorista(){
        return $this->Mayorista;
    }
    public function getMinorista(){
        return $this->Minorista;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * Método que recibe como parámetro la clave de una marca y
     * retorna un array con los modelos de esa marca, si no recibe
     * la marca retorna el array completo (en el caso de ser llamado
     * desde el autocomplete de egresos)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $marca clave de la marca
     * @return array
     */
    public function listaModelos($marca = ""){

        // si no recibió la marca
        if (empty($marca)){

            // compone retornando todas las marcas
            $consulta = "SELECT vw_modelos.id AS clave_modelo,
                                vw_modelos.descripcion AS descripcion_modelo,
                                vw_modelos.marca AS marca_modelo
                        FROM vw_modelos;";

        // si recibió la marca
        } else {

            // compone la consulta buscando en la vista
            // por la clave y por el texto
            $consulta = "SELECT vw_modelos.id AS clave_modelo,
                                vw_modelos.descripcion AS descripcion_modelo
                        FROM vw_modelos
                        WHERE vw_modelos.marca LIKE '%$marca%' OR
                              vw_modelos.id_marca = '$marca';";

        }

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $fila;

    }

    /**
     * Método que retorna la nómina completa de modelos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function todosModelos(){

        // compone la consulta buscando en la vista
        $consulta = "SELECT *
                     FROM vw_modelos;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $fila;

    }

    /**
     * Método que llama la consulta de inserción o edición según
     * corresponda y retorna la id del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaModelo(){

        // si está insertando
        if (empty($this->Id)){
            $this->nuevoModelo();
        } else {
            $this->editaModelo();
        }

        // retorna la id del registro
        return $this->Id;

    }

    /**
     * Método protegido que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoModelo(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO modelos
                            (marca,
                             descripcion,
                             critico,
                             costo,
                             incmayorista,
                             incminorista,
                             mayorista,
                             minorista,
                             fecha,
                             usuario)
                            VALUES
                            ('$this->Marca',
                             '$this->Descripcion',
                             '$this->Critico',
                             '$this->Costo',
                             '$this->IncMayorista',
                             '$this->IncMinorista',
                             '$this->Mayorista',
                             '$this->Minorista',
                             '$this->Fecha',
                             '$this->IdUsuario');";

        // ejecutamos la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // si pudo insertar
        if ($estado){

            // obtenemos la id del registro
            $this->Id = $this->Link->lastInsertId();

            // inserta el item en el inventario
            $this->nuevoInventario();

        }

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaModelo(){

        // compone la consulta de edición
        $consulta = "UPDATE modelos SET
                            marca = '$this->Marca',
                            descripcion = '$this->Descripcion',
                            critico = '$this->Critico',
                            costo = '$this->Costo',
                            incmayorista = '$this->IncMayorista',
                            incminorista = '$this->IncMinorista',
                            mayorista = '$this->Mayorista',
                            minorista = '$this->Minorista',
                            fecha = '$this->Fecha',
                            usuario = '$this->IdUsuario'
                     WHERE id = '$this->Id';";

        // ejecutamos la consulta
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // si pudo editar
        if ($estado){

            // actualizamos el inventario
            $this->editaInventario();

        // si hubo un error
        } else {

            // reinicia la clave
            $this->Id = 0;

        }

    }

    /**
     * Método llamado luego de insertar un modelo que actualiza
     * los items del inventario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function nuevoInventario(){

        // componemos la consulta
        $consulta = "INSERT INTO inventario
                            (item,
                             critico,
                             usuario)
                            VALUES
                            ('$this->Id',
                             '$this->Critico',
                             '$this->IdUsuario');";
        $this->Link->exec($consulta);

    }

    /**
     * Métod llamado luego de editar un modelo que actualiza
     * los items del inventario
     */
    public function editaInventario(){

        // componemos la consulta
        $consulta = "UPDATE inventario SET
                            critico = '$this->Critico',
                            usuario = '$this->IdUsuario'
                     WHERE inventario.item = '$this->Id';";
        $this->Link->exec($consulta);

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y determina si se puede borrar (es decir, que el modelo
     * no tenga hijos)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del modelo
     * @return boolean
     */
    // método público que determina si se puede borrar un modelo
    public function puedeBorrar($id = false){

        // si no recibió la id
        if (!$id){

            // verifica si está definida por la clase
            if (empty($this->Id)){

                // abandona por error
                echo "No he recibido la clave del modelo a verificar";
                exit;

            // si está definida
            } else {

                // lo asigna
                $id = $this->Id;

            }

        }

        // compone la consulta buscando si los repuestos
        // tienen ese modelo
        $consulta = "SELECT COUNT(egresos.id) AS cantidad
                     FROM egresos
                     WHERE egresos.item = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si hubo registros
        if ($cantidad != 0){
            return false;
        } else {
            return true;
        }

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los datos del mismo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del modelo
     */
    public function getDatosModelo($idmodelo){

        // inicializamos las variables
        $idmarca = 0;
        $descripcion = "";
        $critico = 0;
        $costo = 0;
        $incmayorista = 0;
        $incminorista = 0;
        $mayorista = 0;
        $minorista = 0;
        $fecha = "";
        $usuario = "";

        // componemos la consulta
        $consulta = "SELECT vw_modelos.id AS idmodelo,
                            vw_modelos.id_marca AS idmarca,
                            vw_modelos.descripcion AS descripcion,
                            vw_modelos.critico AS critico,
                            vw_modelos.costo AS costo,
                            vw_modelos.incmayorista AS incmayorista,
                            vw_modelos.incminorista AS incminorista,
                            vw_modelos.mayorista AS mayorista,
                            vw_modelos.minorista AS minorista,
                            vw_modelos.fecha AS fecha,
                            vw_modelos.usuario AS usuario
                     FROM vw_modelos
                     WHERE vw_modelos.id = '$idmodelo';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables y asignamos
        extract($fila);
        $this->Id = $idmodelo;
        $this->Marca = $idmarca;
        $this->Descripcion = $descripcion;
        $this->Critico = $critico;
        $this->Costo = $costo;
        $this->IncMayorista = $incmayorista;
        $this->IncMinorista = $incminorista;
        $this->Mayorista = $mayorista;
        $this->Minorista = $minorista;
        $this->Fecha = $fecha;
        $this->Usuario = $usuario;

    }

    /**
     * Método que recibe como parámetro la clave de un registro y
     * ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     * @return boolean
     */
    public function borraModelo($id){

        // compone la consulta
        $consulta = "DELETE FROM modelos
                     WHERE id = '$id';";
        $resultado = $this->Link->prepare($consulta);
        $estado = $resultado->execute();

        // si pudo eliminar
        if ($estado){

            // eliminamos de la tabla de inventario
            $consulta = "DELETE FROM inventario
                         WHERE inventario.item = '$id';";
            $this->Link->exec($consulta);

        }

        // retorna el resultado
        return $estado;

    }

    /**
     * Método que recibe como paràmetro la clave de la marca
     * y el nombre del modelo y verifica que no esté declarado
     * en la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idmarca - clave de la marca
     * @param string $modelo - nombre del modelo
     * @return int
     */
    public function validaModelo($idmarca, $modelo){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(modelos.id) AS registros
                     FROM modelos
                     WHERE modelos.marca = '$idmarca' AND
                           modelos.descripcion = '$modelo';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables y retornamos
        extract($fila);
        return $registros;

    }

}
