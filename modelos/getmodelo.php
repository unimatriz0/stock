<?php

/**
 *
 * modelos/getmodelo.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de un modelo y
 * retorna un array json con los datos del registro
 *
*/

// incluimos e instanciamos la clase
require_once ("modelos.class.php");
$modelo = new Modelos();

// obtenemos los datos del registro
$modelo->getDatosModelo($_GET["modelo"]);

// retornamos el array json
echo json_encode(array("Id" =>           $modelo->getId(),
                       "Marca" =>        $modelo->getMarca(),
                       "Descripcion" =>  $modelo->getDescripcion(),
                       "Critico" =>      $modelo->getCritico(),
                       "Costo" =>        $modelo->getCosto(),
                       "IncMayorista" => $modelo->getIncMayorista(),
                       "IncMinorista" => $modelo->getIncMinorista(),
                       "Mayorista" =>    $modelo->getMayorista(),
                       "Minorista" =>    $modelo->getMinorista(),
                       "Usuario" =>      $modelo->getUsuario(),
                       "Fecha" =>        $modelo->getFecha()));

?>
