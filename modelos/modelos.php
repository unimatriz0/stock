<?php

/**
 *
 * modelos/modelos.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que arma la grilla para el abm de modelos
*/

// incluimos la clases y las instanciamos
require_once ("modelos.class.php");
$modelos = new Modelos();

// obtiene el nivel de acceso
$esadmin = $_COOKIE["Administrador"];

// obtenemos el array completo
$lista = $modelos->todosModelos();

// definimos la tabla
echo "<table width='90%' align='center' border='0' id='modelos'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>Marca</th>";
echo "<th>Modelo</th>";
echo "<th>Fecha</th>";
echo "<th>Usuario</th>";
echo "<th>";
echo "<input type='button'
             name='btnNuevo'
             id='btnNuevo'
             class='botonagregar'
             title='Agrega un nuevo modelo'
             onClick='modelos.nuevoModelo()'>";
echo "</th>";
echo "<th></th>";              // para el botón borrar
echo "<th></th>";              // para el botòn agregar stock
echo "</tr>";
echo "</thead>";

// abrimos el cuerpo de la tabla
echo "<tbody>";

// recorremos el array
foreach($lista AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos el formulario
    echo "<form name='modelos'>";

    // abrimos la fila
    echo "<tr>";

    // presentamos la marca
    echo "<td>$marca</td>";

    // presentamos el texto con el modelo
    echo "<td>$descripcion</td>";

    // presentamos la fecha (solo lectura)
    echo "<td>$fecha</td>";

    // presentamos el usuario (solo lectura)
    echo "<td>$usuario</td>";

    // si es administrador
    if ($esadmin == "Si"){

        // el botòn agregar stock
        echo "<td>";
        echo "<input type='button'
                     name='BtnIngresar'
                     title='Ingresar mercaderìa al depósito'
                     class='botonagregar'
                     onClick='ingresos.nuevoIngreso($id, " . chr(34) . $descripcion . chr(34) . ")'>";
        echo "</td>";

        // el botón editar
        echo "<td>";
        echo "<input type='button'
                     name='BtnEditar'
                     title='Modificar el modelo'
                     class='botoneditar'
                     onClick='modelos.getDatosModelo($id)'>";
        echo "</td>";

        // si puede borrar, presenta el botón eliminar
        echo "<td>";
        if($modelos->puedeBorrar($id)){
            echo "<input type='button'
                   name='BtnBorrar'
                   title='Eliminar el modelo'
                   class='botonborrar'
                   onClick='modelos.BorraModelo($id)'>";
        }
        echo "</td>";

    // si no es admin
    } else {

        // presenta las columnas en blanco
        echo "<td></td><td></td><td></td>";

    }

    // cerramos la fila
    echo "<tr>";

    // cerramos el formulario
    echo "</form>";

}

// cerramos la tabla
echo "</tbody></table>";

// define el div para el paginador de la tabla
echo "<div class='paging'></div>";

?>
<SCRIPT>

    // seteamos el título de la página
    $("#encabezado").html("<h1 class='title'>Nómina de Modelos</h1>");

    // aquí fijamos las propiedades del objeto tabla
    // definimos las propiedades de la tabla
    $('#modelos').datatable({
        pageSize: 15,
        sort:    [true, true, true, true,     false, false, false],
        filters: [true, true, true, 'select', false, false, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
