<?php

/**
 *
 * modelos/graba_modelo.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del formulario
 * y actualiza la base de datos, retorna el resultado de la
 * operación
 *
*/

// incluimos e instanciamos la clase
require_once ("modelos.class.php");
$modelo = new Modelos();

// asigna los valores recibidos
if (!empty($_POST["id"])){
    $modelo->setId($_POST["id"]);
}
$modelo->setMarca($_POST["marca"]);
$modelo->setDescripcion($_POST["descripcion"]);
$modelo->setCritico($_POST["critico"]);
$modelo->setCosto($_POST["costo"]);
$modelo->setIncMayorista($_POST["incmayorista"]);
$modelo->setIncMinorista($_POST["incminorista"]);
$modelo->setMayorista($_POST["mayorista"]);
$modelo->setMinorista($_POST["minorista"]);

// actualiza la base
$resultado = $modelo->grabaModelo();

// retorna la clave del registro
echo json_encode(array("Id" => $resultado));

?>
