/*
 * Nombre: modelos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 22/09/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de stock en los modelos
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no debería haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta métodos para el control de los modelos
 */
class Modelos {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initModelos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initModelos(){

        // inicializamos las variables
        this.IdModelo = 0;                  // clave del registro
        this.Marca = 0;                     // clave de la marca
        this.Descripcion = "";              // nombre del modelo
        this.Critico = 0;                   // valor crítico de stock
        this.Costo = 0;                     // valor de compra
        this.IncMayorista = 0;              // porcentaje de ganancia mayorista
        this.IncMinorista = 0;              // porcentaje de ganancia minorista
        this.Mayorista = 0;                 // precio de venta mayorista
        this.Minorista = 0;                 // precio de venta minorista
        this.Fecha = "";                    // fecha de alta
        this.Usuario = "";                  // nombre del usuario
        this.Correcto = true;               // switch de modelo repetido

    }

    // métodos de asignación de valores
    setIdModelo(idmodelo){
        this.IdModelo = idmodelo;
    }
    setMarca(marca){
        this.Marca = marca;
    }
    setDescripcion(descripcion){
        this.Descripcion = descripcion;
    }
    setCritico(critico){
        this.Critico = critico;
    }
    setCosto(costo){
        this.Costo = costo;
    }
    setIncMayorista(incremento){
        this.IncMayorista = incremento;
    }
    setVentaMayorista(venta){
        this.VentaMayorista = venta;
    }
    setIncMinorista(incremento){
        this.IncMinorista = incremento;
    }
    setVentaMinorista(venta){
        this.VentaMinorista = venta;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que carga en el div la grilla de modelos
     */
    Modelos(){

        // carga la nómina en el layer
        $('#contenido').load('modelos/modelos.html');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que carga en el div la grilla de modelos
     */
    grillaModelos(){

        // carga la nómina en el layer
        $('#nominamodelos').load('modelos/modelos.php');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica que no se
     * encuentre repetida el modelo
     */
    validaModelo(modeloCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'modelos/valida_modelo.php?marca='+this.Marca+'&modelo='+this.Descripcion,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    modelos.Correcto = true;
                } else {
                    modelos.Correcto = false;
                }

            }});

        // retornamos
        modeloCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} tipo - el tipo de incremento (mayorista o minorista)
     * Método llamado al pulsar el botón calcular, toma el valor
     * declarado de costo y calcula el precio de venta
     */
    calculaIncremento(tipo){

        // declaración de variables
        var precio;
        var mensaje;
        var costo = parseFloat(document.getElementById("compra_modelo").value);
        var incremento;

        // segùn el tipo de incremento
        if (tipo == "mayorista"){

            // obtenemos el incremento
            incremento = parseFloat(document.getElementById("incremento_mayorista").value);

        // si es minorista
        } else {

            // obtenemos el incremento
            incremento = parseFloat(document.getElementById("incremento_minorista").value);

        }

        // si no ingresó el costo
        if (isNaN(costo)){

            // presenta el mensaje
            mensaje = "Debe indicar el valor de compra";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no ingresó el incremento
        if (isNaN(incremento)){

            // presenta el mensaje
            mensaje = "Debe indicar el porcentaje de incremento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // calculamos el valor final y lo asignamos
        precio = (costo * incremento /100) + costo;

        // segùn el preciò que estamos calculando
        if (tipo == "mayorista"){
            document.getElementById("venta_mayorista").value = precio.toFixed(2);
        } else {
            document.getElementById("venta_minorista").value = precio.toFixed(2);
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmai.com>
     * Método que verifica el formulario de datos antes de
     * enviarlo al servidor
     */
    GrabaModelo(){

        // inicializamos las variables para estar seguros
        // de no arrastrar basura
        this.initModelos();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("idmodelo").value != ""){

            // lo asigna a la variable de clase
            this.IdModelo = document.getElementById("idmodelo").value;

        }

        // verifica se halla seleccionado una marca
        this.Marca = document.getElementById("marca_modelo").value;
        if (this.Marca == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar una marca de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("marca_modelo").focus();
            return false;

        }

        // asignamos el valor crítico
        this.Critico = document.getElementById("critico_modelo").value;

        // verifica se halla seleccionado un modelo
        this.Descripcion = document.getElementById("descripcion_modelo").value;
        if (this.Descripcion === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el nombre del modelo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("descripcion_modelo").focus();
            return false;

        }

        // si no indicó el valor de compra
        this.Costo = document.getElementById("compra_modelo").value;
        if (this.Costo == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el valor de compra";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("compra_modelo").focus();
            return false;

        }

        // si no indicó el incremento minorista
        this.IncMinorista = document.getElementById("incremento_minorista").value;
        if (this.IncMinorista == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el porcentaje de incremento minorista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("incremento_minorista").focus();
            return false;

        }

        // si existe el valor de venta minorista
        this.VentaMinorista = document.getElementById("venta_minorista").value;
        if (this.VentaMinorista == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el valor de venta minorista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("venta_minorista").focus();
            return false;

        }

        // si no indicó el incremento mayorista
        this.IncMayorista = document.getElementById("incremento_mayorista").value;
        if (this.IncMayorista == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el porcentaje de incremento mayorista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("incremento_mayorista").focus();
            return false;

        }

        // si existe el valor de venta mayorista
        this.VentaMayorista = document.getElementById("venta_mayorista").value;
        if (this.VentaMayorista == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el valor de venta mayorista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("venta_mayorista").focus();
            return false;

        }

        // si está dando un alta
        if (this.IdModelo == 0){

            // verificamos por callback que no este repetido
            this.validaModelo(function(modeloCorrecto){

                // si está repetido
                if (!modelos.Correcto){

                    // presenta el mensaje
                    mensaje = "Ese modelo ya se encuentra declarado !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.actualizaRegistro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía el formulario al servidor
     */
    actualizaRegistro(){

        // si el registro está duplicado
        if (!this.Correcto){
            return false;
        }

        // crea el objeto formulario de html5 y le asigna las propiedades
        var formulario = new FormData();
        formulario.append("marca", this.Marca);
        formulario.append("descripcion", this.Descripcion);
        formulario.append("critico", this.Critico);
        formulario.append("costo", this.Costo);
        formulario.append("incmayorista", this.IncMayorista);
        formulario.append("mayorista", this.VentaMayorista);
        formulario.append("incminorista", this.IncMinorista);
        formulario.append("minorista", this.VentaMinorista);

        // si está editando
        if (this.IdModelo != 0){

            // agrega al formulario
            formulario.append("id", this.IdModelo);

        }

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "modelos/graba_modelo.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si hubo un error
                if (data.Id == 0){

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si salió todo bien
                } else {

                    // si está insertando
                    if (document.getElementById("idmodelo").value == ""){

                        // llamamos el alta de artículos
                        ingresos.nuevoIngreso(data.Id, modelos.Descripcion);

                    }

                    // reiniciamos el formulario
                    modelos.nuevoModelo();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // recarga el div de datos que es menos
                    modelos.grillaModelos();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idmodelo - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores del mismo
     */
    getDatosModelo(idmodelo){

        // llama la rutina php
        $.get('modelos/getmodelo.php','modelo='+idmodelo,
            function(data){

                // asignamos en las variables de clase
                modelos.setIdModelo(data.Id);
                modelos.setDescripcion(data.Descripcion);
                modelos.setCritico(data.Critico);
                modelos.setIncMayorista(data.IncMayorista);
                modelos.setIncMinorista(data.IncMinorista);
                modelos.setMarca(data.Marca);
                modelos.setUsuario(data.Usuario);
                modelos.setFecha(data.Fecha);
                modelos.setCosto(data.Costo);
                modelos.setVentaMayorista(data.Mayorista);
                modelos.setVentaMinorista(data.Minorista);

                // cargamos los datos
                modelos.mostrarModelo();

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga
     * el formulario
     */
    mostrarModelo(){

        // cargamos los datos en el formulario
        document.getElementById("idmodelo").value = this.IdModelo;
        document.getElementById("marca_modelo").value = this.Marca;
        document.getElementById("descripcion_modelo").value = this.Descripcion;
        document.getElementById("critico_modelo").value = this.Critico;
        document.getElementById("compra_modelo").value = this.Costo;
        document.getElementById("incremento_minorista").value = this.IncMinorista;
        document.getElementById("venta_minorista").value = this.VentaMinorista;
        document.getElementById("incremento_mayorista").value = this.IncMayorista;
        document.getElementById("venta_mayorista").value = this.VentaMayorista;
        document.getElementById("alta_modelo").value = this.Fecha;
        document.getElementById("usuario_modelo").value = this.Usuario;

        // seteamos el foco
        document.getElementById("marca_modelo").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario y las variables de clase
     */
    nuevoModelo(){

        // inicializamos las variables de clase
        this.initModelos();

        // reiniciamos el formulario
        document.getElementById("idmodelo").value = "";
        document.getElementById("marca_modelo").value = "";
        document.getElementById("descripcion_modelo").value = "";
        document.getElementById("critico_modelo").value = 0;
        document.getElementById("compra_modelo").value = "";
        document.getElementById("incremento_minorista").value = 0;
        document.getElementById("venta_minorista").value = 0;
        document.getElementById("incremento_mayorista").value = 0;
        document.getElementById("venta_mayorista").value = 0;
        document.getElementById("alta_modelo").value = fechaActual();
        document.getElementById("usuario_modelo").value = sessionStorage.getItem("Usuario");

        // fijamos el foco
        document.getElementById("marca_modelo").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente resetea el formulario de modelos
     */
    CancelaModelo(){

        // reinicia el formulario y setea el foco
        document.getElementById("form_modelos").reset();
        document.getElementById("marca_modelo").focus();

    }
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del modelo
     * Método que pide confirmación antes de eliminar el modelo de
     * la base
     */
    BorraModelo(id){

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Eliminar Modelo',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: 'Está seguro que desea eliminar el registro?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // llama la rutina php
                                $.get('modelos/borra_modelo.php?id='+id,
                                    function(data){

                                        // si retornó correcto
                                        if (data.Error != 0){

                                            // recargamos la grilla para reflejar los cambios
                                            modelos.grillaModelos();

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                                        // si no pudo eliminar
                                        } else {

                                            // presenta el mensaje
                                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                                        }

                                    }, "json");

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                        });

            // mostramos el diálogo
            Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que genera la exportación de modelos a una
     * tabla excel
     */
    Exportar(){

        // probamos de redirigir el navegador
        window.location = "modelos/xls_modelos.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento . id de un elemento html
     * @param {int} idmarca - clave de la marca
     * @param {int} idmodelo - valor preseleccionado
     * Método que recibe como parámetro la clave de un elemento
     * html, la clave de la marca y carga el select con los
     * valores de la tabla de modelos, si además recibe un
     * tercer parámetro preselecciona este valor
     */
    listaModelos(idelemento, idmarca, idmodelo){

        // limpia el combo
        $("#" + idelemento).html('');

        // llama la rutina php
        $.get('modelos/lista_modelos.php?marca='+idmarca,
            function(data){

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si coincide con el valor recibido y no indefinido
                    if (typeof(idmodelo) != "undefined"){

                        // si coincide
                        if (idmodelo == data[i].id){

                            // lo predetermina
                            $("#" + idelemento).append("<option selected value=" + data[i].id + ">" + data[i].modelo + "</option>");

                        // si no coincide
                        } else {

                            // lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].modelo + "</option>");

                        }

                    // si no recibió la clave
                    } else {

                        // lo agrega
                        $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].modelo + "</option>");

                    }

                }

            }, "json");

    }

}