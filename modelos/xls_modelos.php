<?php

/**
 *
 * modelos/xls_modelos.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (03/10/2018)
 * @copyright   Copyright (c) 2018, KiwiDigital
 *
 * Procedimiento que genera un xls con la nómina de modelos
 *
*/

// incluimos e instanciamos las clases
require_once ("modelos.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
$modelo = new Modelos();
$hoja = new PHPExcel();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Nómina de Modelos")
					  ->setSubject("Modelos en inventario")
					  ->setDescription("Resumen de la nómina de modelos disponibles")
					  ->setKeywords("Stock")
					  ->setCategory("Reportes");

// obtenemos la nómina de modelos
$nomina = $modelo->todosModelos();

// si hubo registros
if (count($nomina) != 0){

    // leemos la plantilla
    $hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

    // establecemos el ancho de las columnas
    $hoja->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $hoja->getActiveSheet()->getColumnDimension('B')->setWidth(60);
    $hoja->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('E')->setWidth(12);
    $hoja->getActiveSheet()->getColumnDimension('F')->setWidth(12);
    $hoja->getActiveSheet()->getColumnDimension('G')->setWidth(12);
    $hoja->getActiveSheet()->getColumnDimension('H')->setWidth(12);

    // contador de filas
    $fila = 13;

    // fijamos el estilo del título
    $estilo = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 12,
            'name'  => 'Verdana'
        ));

    // fijamos el estilo de los encabezados
    $encabezado = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 10,
            'name'  => 'Verdana'
        ));

    // centramos la columna de la cantidad crítica
    $hoja->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // presenta el título
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B3', 'Sistema de Control de Stock');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B5', 'Nómina de Modelos en Inventario');

    // centramos las celdas de los títulos
    $hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);

    // establecemos la fuente
    $hoja->getDefaultStyle()->getFont()->setName('Arial')
         ->setSize(10);

    // presenta los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Marca')
          ->setCellValue('B' . $fila, 'Modelo')
          ->setCellValue('C' . $fila, 'Crítico')
          ->setCellValue('D' . $fila, 'Costo')
          ->setCellValue('E' . $fila, 'Inc.May.')
          ->setCellValue('F' . $fila, 'Mayorista')
          ->setCellValue('G' . $fila, 'Inc.Min.')
          ->setCellValue('H' . $fila, 'Minorista');

    // establecemos la fuente de los encabezados
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('H' . $fila)->applyFromArray($encabezado);

    // volvemos a incrementar la fila
    $fila++;

    // iniciamos un bucle recorriendo el vector
    foreach ($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // presenta el registro
        $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila , $marca)
            ->setCellValue('B' . $fila , $descripcion)
            ->setCellValue('C' . $fila , $critico)
            ->setCellValue('D' . $fila , $costo)
            ->setCellValue('E' . $fila , $incmayorista)
            ->setCellValue('F' . $fila , round($mayorista, 2))
            ->setCellValue('G' . $fila , $incminorista)
            ->setCellValue('H' . $fila , round($minorista, 2));

        // fijamos el formato de las celdas numéricas
        $hoja->getActiveSheet()->getStyle('D' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('E' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('F' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('G' . $fila)->getNumberFormat()->setFormatCode("#,###.##");
        $hoja->getActiveSheet()->getStyle('H' . $fila)->getNumberFormat()->setFormatCode("#,###.##");

        // incrementamos el contador
        $fila++;

    }

    // renombramos la hoja
    $hoja->getActiveSheet()->setTitle('Modelos');

    // fijamos la primer hoja como activa para abrirla predeterminada
    $hoja->setActiveSheetIndex(0);

    // creamos el writer y lo dirigimos al navegador en formato 2007
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="modelos.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
    $Writer->save('php://output');

}

?>