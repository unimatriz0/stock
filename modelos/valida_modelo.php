<?php

/**
 *
 * modelos/valida_modelo.php
 *
 * @package     Stock
 * @subpackage  Modelos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (24/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de una marca y el
 * nombre de un modelo y retorna la cantidad de registros
 * hallados, utilizado para evitar el ingreso de duplicados
 *
*/

// incluimos e instanciamos la clase
require_once ("modelos.class.php");
$modelo = new Modelos();

// obtenemos el número de registros
$registros = $modelo->validaModelo($_GET["marca"], $_GET["modelo"]);

// retornamos el estado de la operación
echo json_encode(array("Registros" => $registros));

?>
